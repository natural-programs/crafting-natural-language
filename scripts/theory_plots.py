"""Draw the plots from the theory analysis."""
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

def np_prob_of_enough_data(
        current_generation_i: int,
        top_level_puzzle_solution_n: int,
        max_puzzle_height: int,
        ) -> float:
    l = top_level_puzzle_solution_n
    g = current_generation_i
    i = max_puzzle_height
    return (1 - (1 - 1/l)**(g-1))**(i)

def ds_prob_of_enough_data(
        current_generation_i: int,
        top_level_puzzle_solution_n: int,
        max_puzzle_height: int,
        ) -> float:
    l = top_level_puzzle_solution_n
    g = current_generation_i
    i = max_puzzle_height
    return 1 - (1 - (1/l)**i)**(g-1)


# Create figure
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

# Define generation number
generation_n = 20

top_level_puzzle_solution_n = 2
max_puzzle_height = 5

parameters = [
    dict(top_level_puzzle_solution_n=2, max_puzzle_height=4),
    dict(top_level_puzzle_solution_n=3, max_puzzle_height=5),
    dict(top_level_puzzle_solution_n=4, max_puzzle_height=6),
]

for i, params in enumerate(parameters):
    gens = list(range(1, generation_n))
    N = params['top_level_puzzle_solution_n']
    l = params['max_puzzle_height']
    y_np = [
        np_prob_of_enough_data(
            current_generation_i=g,
            top_level_puzzle_solution_n=N,
            max_puzzle_height=l,
        )
        for g in gens
    ]
    y_ds = [
        ds_prob_of_enough_data(
            current_generation_i=g,
            top_level_puzzle_solution_n=N,
            max_puzzle_height=l,
        )
        for g in gens
    ]
    alpha = (i+1)/len(parameters)
    ax.plot(gens, y_np, '-', label=f"NP N={N} l={l}", alpha=alpha, color='C0')
    ax.plot(gens, y_ds, '--', label=f"DS N={N} l={l}", alpha=alpha, color='C1')


# Add legend
plt.legend()
# sort both labels and handles by labels
# https://stackoverflow.com/a/27512450
handles, labels = ax.get_legend_handles_labels()
labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
ax.legend(handles, labels)

# Make axis use integer ticks
ax.xaxis.set_major_locator(MaxNLocator(integer=True))

# Set axis labels
ax.set_xlabel('generation')
ax.set_ylabel('P(adaptation)')

# Save
output_svg = 'theory_plot.svg'
plt.savefig(output_svg)
print(f'Wrote {output_svg}')
