"""Draw the plots from the theory analysis."""
import matplotlib.pyplot as plt

def np_prob_of_enough_data(
        current_generation_i: int,
        top_level_puzzle_solution_n: int,
        max_puzzle_height: int,
        ) -> float:
    l = top_level_puzzle_solution_n
    g = current_generation_i
    i = max_puzzle_height
    return (1 - (1 - 1/l)**(g-1))**(i)

def ds_prob_of_enough_data(
        current_generation_i: int,
        top_level_puzzle_solution_n: int,
        max_puzzle_height: int,
        ) -> float:
    l = top_level_puzzle_solution_n
    g = current_generation_i
    i = max_puzzle_height
    return 1 - (1 - (1/l)**i)**(g-1)


top_level_puzzle_solution_n = 2
generation_n = 20
item_complexities = [3,4,5,8,9,10]

gens = list(range(1, generation_n))
y_np = list()
y_ds = list()
for g in gens:
    # NP:
    y_np_probs = list()
    for complexity in item_complexities:
        p = np_prob_of_enough_data(
                current_generation_i=g,
                top_level_puzzle_solution_n=top_level_puzzle_solution_n,
                max_puzzle_height=complexity,
            )
        y_np_probs.append(p)
    expected_np_items = sum(y_np_probs)
    y_np.append(expected_np_items)

    y_ds_probs = list()
    for complexity in [3,4,5,8,9,10]:
        p = ds_prob_of_enough_data(
                current_generation_i=g,
                top_level_puzzle_solution_n=top_level_puzzle_solution_n,
                max_puzzle_height=complexity,
            )
        y_ds_probs.append(p)
    expect_ds_items = sum(y_ds_probs)
    y_ds.append(expect_ds_items)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(gens, y_np, label="Expected NP items built")
ax.plot(gens, y_ds, label="Expected DS items built")
plt.legend()
output_svg = 'theory_plot2.svg'
plt.savefig(output_svg)
print(f'Wrote {output_svg}')
