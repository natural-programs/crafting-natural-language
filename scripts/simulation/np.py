"""This script implements a barebones version of Natural Programming.
"""
from typing import NewType
from typing import Union
from dataclasses import dataclass
from dataclasses import field
from functools import cached_property
import heapq
from puzzle import PuzzleID
from puzzle import get_random_recipe_book
from puzzle import Solution
from puzzle import Primitive
from puzzle import RecipeBook
from puzzle import MetaRecipeBook
from puzzle import is_solution
from puzzle import SolverFailedError
from puzzle import Candidate
import random


Signature = PuzzleID
Steps = tuple[Union["NPSketch",Signature,Primitive], ...]
Decomposition = tuple[Union[Signature, Primitive], ...]
NPLibrary = NewType('Library', dict[PuzzleID, set[Decomposition]])
Cost = int


class ExecutedPartialError(Exception):
    """Raised when a constraint is not satisfied or a partial program is
    unrolled."""


class ConstraintError(Exception):
    """Raised when a constraint is not satisfied or a partial program is
    unrolled."""


class NoLeftMostSignatureError(Exception):
    """Raised when there is no leftmost signature."""

@dataclass
class NPSketch:
    signature: Signature
    steps: Steps
    recipe_book: RecipeBook=field(repr=False)

    @cached_property
    #@property
    def is_partial(self) -> bool:
        """Whether there is a subprogram that is a signature."""
        for step in self.steps:
            if isinstance(step, Signature):
                return True
            elif isinstance(step, NPSketch):
                if step.is_partial:
                    return True
        return False

    @cached_property
    #@property
    def unrolled_steps(self) -> Solution:
        """Raises ExecutionError if this, or an internal program, is not
        correct."""
        solution = list[Primitive]()
        for step in self.steps:
            if isinstance(step, Primitive):
                solution.append(step)
            elif isinstance(step, Signature):
                raise ExecutedPartialError()
            else:
                solution.extend(step.unrolled_steps)
        solution = Solution(tuple(solution))
        if not is_solution(Candidate(solution), self.signature, self.recipe_book):
            raise ConstraintError()
        return solution

    @cached_property
    #@property
    def is_correct(self) -> bool:
        """Full correctness. I.e., is fully concrete and every subprogram
        satisfies constraints."""
        try:
            self.unrolled_steps
            return True
        except ExecutedPartialError:
            return False
        except ConstraintError:
            return False

    @cached_property
    #@property
    def concrete_unsat(self) -> bool:
        """Whether there is a concrete program that currently does not
        satisfy its constraints."""
        try:
            self.unrolled_steps
            return False
        except ExecutedPartialError:
            return False
        except ConstraintError:
            return True

    @cached_property
    #@property
    def non_top_level_concrete_unsat(self) -> bool:
        """Whether there is a concrete subprogram that currently does not
        satisfy its constraints."""
        for step in self.steps:
            if isinstance(step, Signature):
                return False
            elif isinstance(step, Primitive):
                return False
            elif isinstance(step, NPSketch):
                if step.is_partial:
                    return False
                elif step.concrete_unsat:
                    return True
        return False

    @cached_property
    #@property
    def leftmost_signature(self) -> Signature:
        """The leftmost signature. Raises ValueError if there is none (i.e., if
        the sketch is not partial."""
        for step in self.steps:
            if isinstance(step, Signature):
                return step
            elif isinstance(step, NPSketch):
                try:
                    signature = step.leftmost_signature
                    return signature
                except NoLeftMostSignatureError:
                    pass
        raise NoLeftMostSignatureError()


def get_leftmost_signature_expanded(
        np: NPSketch,
        replacement: Steps,
        ) -> tuple[NPSketch, bool]:
    """Replace the leftmost signature of the given sketch with the given
    replacement. Returns a boolean flat that indicates whether a replacement
    was performed."""
    if not np.is_partial:
        return np, False

    new_steps = Steps()
    replaced = False
    for step in np.steps:
        if replaced:
            new_step = step
        elif isinstance(step, Signature):
            new_step = NPSketch(
                signature=step,
                steps=replacement,
                recipe_book=np.recipe_book,
            )
            replaced = True
        elif isinstance(step, NPSketch):
            new_step, replaced = get_leftmost_signature_expanded(
                step,
                replacement
            )
        else:
            new_step = step
        new_steps = new_steps + (new_step,)
    return NPSketch(
        signature=np.signature,
        steps=new_steps,
        recipe_book=np.recipe_book,
    ), replaced


QueueItem = NPSketch


@dataclass(order=True)
class PrioritizedItem:
    priority: float
    item: QueueItem=field(compare=False)


def get_solution(
        puzzle_id: PuzzleID,
        recipe_book: RecipeBook,
        library: NPLibrary,
        primitives: set[Primitive],
        max_iter_n: int,
        ) -> tuple[NPSketch, Cost]:
    """Solve the puzzle using Natural Programming and return the solution
    and number of `pops` to the search space."""
    # Beam is signatures and primitives
    beam = list[Signature|Primitive]()
    signatures = library.keys()
    beam.extend([s for s in signatures if s != puzzle_id])
    beam.extend(primitives)
    random.shuffle(beam)

    # The queue is prioritized with user data. In this simulation
    # we use top-level length to sort the queue, since we don't really have
    # user data.
    queue = list[PrioritizedItem]()
    def enqueue(item: QueueItem):
        priority = len(item.steps)
        qitem = PrioritizedItem(priority=priority, item=item)
        heapq.heappush(queue, qitem)
    def dequeue():
        item = heapq.heappop(queue)
        return item.item

    # Starting queue is given by library decompositions
    if puzzle_id in library.keys():
        for solution in library[puzzle_id]:
            new_candidate = NPSketch(
                signature=puzzle_id,
                steps=solution,
                recipe_book=recipe_book,
            )
            enqueue(new_candidate)

    explore_cross_product = False
    if len(queue) == 0:
        # Start cross product exploration
        explore_cross_product = True
        enqueue(NPSketch(puzzle_id, tuple(), recipe_book))

    # Try to expand natural program. Otherwise, explore entire cross product as
    # a last resort
    iter_i = 0
    for _ in range(max_iter_n):
        if len(queue) == 0:
            break

        # Pop from queue
        candidate = dequeue()
        iter_i += 1

        if candidate.is_correct:
            return candidate, iter_i
        elif candidate.non_top_level_concrete_unsat:
            # There is a concrete internal program that does not satisfy
            # its constraints
            # Discard it.
            continue
        elif candidate.is_partial:
            # There is an unexpanded signature. Expand it with every
            # known decomposition
            signature = candidate.leftmost_signature
            if signature in library.keys():
                decompositions = library[signature]
                for decomposition in decompositions:
                    new_candidate, _ = get_leftmost_signature_expanded(
                        np=candidate,
                        replacement=decomposition,
                    )
                    enqueue(new_candidate)
        elif explore_cross_product:
            # Explore cross product with the beam
            for elem in beam:
                new_candidate = NPSketch(
                    signature=candidate.signature,
                    steps=candidate.steps + (elem,),
                    recipe_book=recipe_book,
                )
                enqueue(new_candidate)

        if len(queue) == 0 and not explore_cross_product:
            # Start cross product exploration
            explore_cross_product = True
            enqueue(NPSketch(puzzle_id, tuple(), recipe_book))

    raise SolverFailedError()


def get_decompositions(sketch: NPSketch) -> list[tuple[Signature, Decomposition]]:
    """Return every decomposition in the sketch."""
    decompositions = list()

    # Get the decomposition for this sketch
    decomposition = list()
    for step in sketch.steps:
        if isinstance(step, NPSketch):
            item = step.signature
        else:
            item = step
        decomposition.append(item)
    decompositions.append((sketch.signature, tuple(decomposition)))

    # Recursively get other decompositions
    for step in sketch.steps:
        if isinstance(step, NPSketch):
            decompositions.extend(get_decompositions(step))
    return decompositions


def get_updated_library(library: NPLibrary, sketch: NPSketch) -> NPLibrary:
    """Return a library that incorporates the given sketch."""
    # Shallow copy library
    new_library = NPLibrary(dict(library))

    # Add every decomposition to the library
    for signature, decomposition in get_decompositions(sketch):
        if signature not in new_library.keys():
            new_library[signature] = set()
        new_library[signature].add(decomposition)
    return new_library


def get_np_solution(puzzle_id: PuzzleID, recipe_book: RecipeBook) -> NPSketch:
    """Return the solution to the given puzzle."""
    steps = list()
    for component in recipe_book[puzzle_id]:
        if isinstance(component, Primitive):
            steps.append(component)
        else:
            steps.append(get_np_solution(component, recipe_book))
    return NPSketch(puzzle_id, tuple(steps), recipe_book)



def sample_generation_costs(
        generation_n: int,
        meta_recipe_book: MetaRecipeBook,
        primitives: set[Primitive],
        max_iter_n: int,
        ) -> tuple[tuple[Cost, ...], ...]:
    """Return a sample of the cost of each item across generations,
    from easy to complex, incrementally building a Natural Programming
    library."""
    library = NPLibrary({})

    generation_costs = list()
    for _ in range(generation_n):
        # Sample recipe book
        recipe_book = get_random_recipe_book(meta_recipe_book)

        # Track generation costs
        costs = list()

        # Solve each puzzle
        for puzzle_id in recipe_book.keys():
            try:
                solution, cost = get_solution(
                    puzzle_id,
                    recipe_book,
                    library,
                    primitives,
                    max_iter_n,
                )
                costs.append(cost)

                # Update library
                library = get_updated_library(library, solution)
            except SolverFailedError:
                # Simulate perfect user
                solution = get_np_solution(puzzle_id, recipe_book)
                library = get_updated_library(library, solution)
                costs.append(max_iter_n)

        # Update costs
        generation_costs.append(costs)
    return tuple(generation_costs)
