"""This script performs a simulation of the search algorithm to quantify
the expected number of iterations of a search algorithm in the Natural
Programming and Direct Synthesis settings.

We compare Natural Programming and Direct Synthesis by using the same
high-level search algorithm on both --enumerative search--, but instead we vary
the search spaces accordingly.

In NP, every sub program is annotated recursively with a goal. Whereas in
DS, we only have a top-level goal. By using a library of Natural Programs, this
representation allows Natural Programming systems to construct a recursive
search space that is very directed.
"""
from puzzle import get_synthetic_meta_recipe_book
from np import sample_generation_costs as sample_np_generation_costs
from ds import sample_generation_costs as sample_ds_generation_costs
import csv
import concurrent.futures


if __name__ == "__main__":
    # Simulation parameters
    max_iter_n = 5000
    generation_n = 20
    simulation_n = 1000
    meta_book_parameters = [
        dict(N=2, l=4, height=1),
        dict(N=3, l=5, height=1),
        dict(N=4, l=6, height=1),
        dict(N=2, l=2, height=2),
        dict(N=3, l=2, height=2),
        dict(N=4, l=2, height=2),
    ]

    quick = False  # set to true to make script finish quickly
    if quick:
        simulation_n = 3
        max_iter_n = 10

    # Simulate rollouts
    methods = [
        ('NP', sample_np_generation_costs),
        ('DS', sample_ds_generation_costs),
    ]
    futures = list()
    rows = list()
    with concurrent.futures.ProcessPoolExecutor() as pool:
        for meta_params in meta_book_parameters:
            meta_book, top_level_item, primitives = get_synthetic_meta_recipe_book(
                N=meta_params['N'],
                l=meta_params['l'],
                height=meta_params['height'],
                name_start=0,
            )
            top_level_item_name = str(top_level_item)
            for _ in range(simulation_n):
                for method, sampler in methods:
                    costs_future = pool.submit(
                        sampler,
                        generation_n=generation_n,
                        primitives=primitives,
                        max_iter_n=max_iter_n,
                        meta_recipe_book=meta_book,
                    )
                    futures.append((
                        costs_future, method, meta_book, meta_params, top_level_item_name,
                    ))

        for i, (costs_future, method, meta_book, meta_params, top_level_item_name) in enumerate(futures):
            print(f"Simulation {i+1}/{len(futures)}...")
            costs = costs_future.result()
            for generation_i, generation_costs in enumerate(costs):
                for puzzle_i, cost in enumerate(generation_costs):
                    puzzle_name = list(meta_book.keys())[puzzle_i]
                    row = dict(
                        method=method,
                        generation=generation_i,
                        iter_n=cost,
                        puzzle_i=puzzle_i,
                        puzzle_name=puzzle_name,
                        simulation_i=i,
                        max_iter_n=max_iter_n,
                        is_success=0 if cost==max_iter_n else 1,
                        meta_params=str(meta_params),
                        top_level_item_name=top_level_item_name,
                        height=meta_params['height'],
                    )
                    rows.append(row)

    # Write CSV
    csv_path = 'puzzle_costs.csv'
    with open(csv_path, 'w', newline='') as csvfile:
        fieldnames = [
            'method',
            'generation',
            'iter_n',
            'simulation_i',
            'puzzle_i',
            'max_iter_n',
            'is_success',
            'puzzle_name',
            'meta_params',
            'top_level_item_name',
            'height',
        ]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for row in rows:
            writer.writerow(row)
    print(f"Wrote {csv_path}")

    # Stable palette
    import seaborn as sns
    method_names = [name for name, _ in methods]
    palette = dict(zip(
        method_names, sns.color_palette(n_colors=len(method_names))
    ))

    # Plot raw CSV
    import pandas as pd
    import matplotlib.pyplot as plt
    df = pd.read_csv(csv_path)
    df = df[df['top_level_item_name'] == df['puzzle_name']]
    plot = sns.lmplot(
        data=df,
        x="generation",
        y="iter_n",
        hue="method",
        scatter_kws=dict(rasterized=True),  # too many points for an SVG
        palette=palette,
        row='meta_params',
    )
    plot_path = "plot_raw.svg"
    plot.savefig(plot_path)
    plt.clf()
    print(f"Wrote {plot_path}")

    # Plot generation means
    df = pd.read_csv(csv_path)
    df = df[df['top_level_item_name'] == df['puzzle_name']]
    plt.figure(figsize=(8,6))
    plot = sns.lineplot(
        data=df,
        x="generation",
        y="iter_n",
        hue="method",
        palette=palette,
        #style='height',
        size='meta_params',
        legend='full',
        errorbar=None,
        #scatter_kws=dict(rasterized=True),  # too many points for an SVG
    )
    plot_path = 'plot_means.svg'
    plt.tight_layout()
    plot.get_figure().savefig(plot_path)
    plt.clf()
    print(f"Wrote {plot_path}")
