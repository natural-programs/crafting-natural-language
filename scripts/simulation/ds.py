from typing import NewType
from dataclasses import dataclass
from dataclasses import field
import heapq
from puzzle import PuzzleID
from puzzle import get_random_recipe_book
from puzzle import get_solution as get_primitive_solution
from puzzle import MetaRecipeBook
from puzzle import Candidate
from puzzle import Solution
from puzzle import Primitive
from puzzle import RecipeBook
from puzzle import is_solution
from puzzle import SolverFailedError
import random


DSLibrary = NewType('Library', dict[PuzzleID, set[Solution]])
QueueItem = Candidate
Cost = int


@dataclass(order=True)
class PrioritizedItem:
    priority: float
    item: QueueItem=field(compare=False)


def get_solution(
        puzzle_id: PuzzleID,
        recipe_book: RecipeBook,
        library: DSLibrary,
        primitives: set[Primitive],
        max_iter_n: int,
        ) -> tuple[Solution, Cost]:
    """Solve the puzzle using Natural Programming and return the solution
    and number of `pops` to the search space."""
    # Beam is signatures and primitives
    beam = list[Primitive]()
    beam.extend(primitives)
    random.shuffle(beam)

    # The queue is prioritized with user data. In this simulation
    # we length to sort the queue, since we are not doing any user data.
    # TODO: we really should use top-level length instead...
    queue = list[PrioritizedItem]()
    def enqueue(item: QueueItem):
        qitem = PrioritizedItem(priority=len(item), item=item)
        heapq.heappush(queue, qitem)
    def dequeue():
        item = heapq.heappop(queue)
        return item.item

    # Also know solutions
    if puzzle_id in library.keys():
        for candidate in library[puzzle_id]:
            enqueue(Candidate(candidate))

    explore_cross_product = False
    if len(queue) == 0:
        # Start cross product exploration
        explore_cross_product = True
        enqueue(Candidate(tuple()))

    # Try to expand natural program. Otherwise, explore entire cross product as
    # a last resort
    iter_i = 0
    for _ in range(max_iter_n):
        # Pop from queue
        candidate = dequeue()
        iter_i += 1

        # NOTE: NP is even better than this, because we can check if any
        # internal constraints are not met. Right now, we try expanding
        # every possible decomposition. TODO: keep track of the
        # internal recursive structure to discard any candidates that
        # fail constraints

        # Three cases
        if is_solution(candidate, puzzle_id, recipe_book):
            return Solution(candidate), Cost(iter_i)
        else:
            # Explore cross product with the beam
            for elem in beam:
                new_candidate = Candidate(candidate + (elem,))

                # Add new candidate to queue
                enqueue(new_candidate)


        if len(queue) == 0 and not explore_cross_product:
            explore_cross_product = True
            enqueue(Candidate(tuple()))
    raise SolverFailedError()


def sample_generation_costs(
        generation_n: int,
        meta_recipe_book: MetaRecipeBook,
        primitives: set[Primitive],
        max_iter_n: int,
        ) -> tuple[tuple[Cost, ...], ...]:
    """Return a sample of the cost of each item across generations,
    from easy to complex, incrementally building a Direct Synthesis
    library."""
    library = DSLibrary({})

    generation_costs = list()
    for _ in range(generation_n):
        # Sample recipe book
        recipe_book = get_random_recipe_book(meta_recipe_book)

        # Track generation costs
        costs = list()

        # Solve each puzzle
        for puzzle_id in recipe_book.keys():
            try:
                solution, cost = get_solution(
                    puzzle_id,
                    recipe_book,
                    library,
                    primitives,
                    max_iter_n,
                )
                costs.append(cost)

                # Update library
                if puzzle_id not in library.keys():
                    library[puzzle_id] = set()
                library[puzzle_id].add(solution)
            except SolverFailedError:
                # Simulate perfect user
                solution = get_primitive_solution(puzzle_id, recipe_book)
                if puzzle_id not in library.keys():
                    library[puzzle_id] = set()
                library[puzzle_id].add(solution)
                costs.append(max_iter_n)

        # Update costs
        generation_costs.append(costs)
    return tuple(generation_costs)
