/**
 * Save a complete library to firebase snapshot.
 */
import { get_api } from "../src/lib/minecraft_app";
import { parse_csvs_all_recipes } from "../src/lib/world_csv";
import { new_library } from "natural-programs/library/library";
import { add_recursive } from "natural-programs/library/library";
import type { Library } from "natural-programs/library/library";
import { parse_csvs } from "../src/lib/world_csv";
import type { MinecraftAPI } from "../src/lib/minecraft_app";
import { get_signature } from "../src/lib/minecraft_app";
import { get_default_snapshot, get_new_snapshot_id, write_snapshot } from "../src/lib/firebase";
import type { CraftableItem } from "minecraft/items";
import type { Item } from "minecraft/items";
import type { NaturalProcedure } from "natural-programs/types";
import { naturalize_pick } from "minecraft/solutions";
import { naturalize_simple_primitive } from "minecraft/solutions";
import { MinecraftSimplePrimitive } from "minecraft/grammar";
import { add_recursive_batch } from "natural-programs/library/library";


const string_sim_timeout = 2000;
const starting_default_snapshot = "items_v5_natural";


/**
 * Return a hierarchical program that crafts the given item by first building
 * all pre-requisite items.
 */
function get_program(item: CraftableItem, items: Item[]): NaturalProcedure {
	// Craft all craftable items
	const craftable_prerequisites = [];
	const placed_items = [];
	for (const prerequisite_name of item.recipe) {
		const prerequisite = items.filter(i=>i.name===prerequisite_name).at(0);
		if (prerequisite === undefined)
			throw "Item in recipe '" + prerequisite_name + "' not found in world!";
		placed_items.push(prerequisite);
		if ("recipe" in prerequisite)
			craftable_prerequisites.push(prerequisite);
	}

	const steps = [
		...craftable_prerequisites.map(prereq=>get_program(prereq, items)),
		...placed_items.map(i=>naturalize_pick(i)),
		naturalize_simple_primitive(MinecraftSimplePrimitive.Craft),
	];
	return {
		signature: get_signature(item),
		steps: steps,
	};
}


/**
 * Generate all choice indices of each array (e.g., [0,0,0], [0,0,1], ...).
 */
function* all_choices(sizes: number[]): Generator<number[], void, void> {
	// Base case: no array to choose from
	if (sizes.length === 0) {
		yield [];
		return;
	}

// Inductive case
	for (let i = 0; i < sizes[0]; i++)
	for (const choices of all_choices(sizes.slice(1)))
		yield [i, ...choices];
}


async function init_new_library(api: MinecraftAPI): Promise<Library> {
	const library = new_library();
	for (const primitive of api.primitives_map.keys())
		await add_recursive(library, primitive, string_sim_timeout);
	return library;
}


/**
 * Add to the library a concrete hierarchical program for each possible
 * CraftableItem. There is a concrete program for each CraftableItem recipe (so
 * items with multiple recipes result in multiple concrete programs).
 */
async function simulate_complete_solution_library(
		items: Item[],
		library: Library,
		string_sim_timeout: number,
	): Promise<Library> {
	// Implementation note: this is not the asymptotically-fastest implementation
	// of this process, but this is a one-off experiment

	// Construct a map of item names to item recipes
	const item_versions: Item[][] = [];
	const item_names = new Set<string>(items.map(item=>item.name));
	for (const item_name of item_names) {
		const current_item_versions = items.filter(item=>item.name===item_name);
		item_versions.push(current_item_versions);
	}

	const recipe_numbers = item_versions.map(v=>v.length);
	let world_n = 1;
	for (const number of recipe_numbers)
		world_n = world_n * number;
	console.log([recipe_numbers, world_n]);

	let i = 0;
	console.log("Obtaining programs in all crafting worlds...");
	const program_jsons = new Set<string>();
	// For every possible crafting world configuration, construct all
	// corresponding concrete hierarchical programs
	for (const choices of all_choices(recipe_numbers)) {
		const current_items = choices
			.map((choice_i, i)=>item_versions[i][choice_i]);
		for (const craftable_item of current_items) {
			if (!("recipe" in craftable_item))
				continue;
			const program = get_program(craftable_item, current_items);
			program_jsons.add(JSON.stringify(program));
		}

		i++;
		if (i % 1000 === 0) {
			console.log({percent: i/world_n*100, program_n: program_jsons.size, world_i: i, world_n: world_n});
		}
	}
	console.log("Done obtaining programs :D");
	console.log("Adding programs to library...");

	// Build the library
	const programs = [...program_jsons].map(j=>JSON.parse(j));
	await add_recursive_batch(library, programs, string_sim_timeout);

	console.log("Done adding to library :D");
	return library;
}


async function get_complete_solution_library(
		item_csv: string,
		api: MinecraftAPI,
		): Promise<Library> {
	const items = parse_csvs_all_recipes(item_csv, "");
	const new_library = await simulate_complete_solution_library(
		items,
		await init_new_library(api),
		string_sim_timeout,
	);
	return new_library;
}


async function main() {
	// Fetch default snapshot
	const icon_dir = "";
	const snapshot = await get_default_snapshot(starting_default_snapshot, icon_dir);
	const world = parse_csvs(snapshot.item_csv, snapshot.mine_csv, snapshot.seed, icon_dir);
	const api = get_api(world);

	// Construct complete library
	const library = await get_complete_solution_library(snapshot.item_csv, api);

	// Upload new snapshot with complete library
	const new_snapshot = {...snapshot, library: library};
	const new_snapshot_id = await get_new_snapshot_id();

	console.log("Uploading snapshot...");
	await write_snapshot(new_snapshot_id, new_snapshot);

	console.log({event: "SUCCESS!", snapshot_id: new_snapshot_id});
}

(async function() {
	await main();
	process.exit(0);
})();
