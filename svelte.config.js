import adapter from '@sveltejs/adapter-static';
import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const dev = process.env.NODE_ENV === 'development';
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess({
		scss: { includePaths: ['src', 'node_modules'] },
	}),

	kit: {
		adapter: adapter({
			fallback: 'index.html',
		}),
		paths: {
			base: dev ? '' : '/crafting-natural-language',
		},
		appDir: 'internal',
	}
};

export default config;
