/**
 * Implements semantics for the Minecraft crafting grammar.
 */
import type { CraftingWorld, Item } from "./items";
import { can_mine } from "./items";
import { get_yield } from "./items";
import type { MinecraftHierarchicalPrimitive } from "./grammar";
import { MinecraftSimplePrimitive } from "./grammar";
import type { MinecraftPrimitive } from "./grammar";
import type { MinecraftPrimitivePick } from "./grammar";
import type { MinecraftPrimitiveMine } from "./grammar";


/**
 * Executions return a new inventory and a new crafting table.
 */
type ProgramState = {
	inventory: Item[],
	crafting_table: Item[],
	held_item: Item | null,
}


/**
 * Returns true if and only if the given crafting table matches
 * the given recipe.
 */
function is_match_recipe(
	crafting_table: string[],
	recipe: string[],
	): boolean {
	// Treat recipe and crafting table as multi-sets
	// to check for equality compare sorted arrays.
	if (crafting_table.length !== recipe.length) return false;
	crafting_table = [...crafting_table];
	recipe = [...recipe];
	crafting_table.sort();
	recipe.sort();
	for (let i = 0; i < recipe.length; i++)
	if (recipe[i] !== crafting_table[i])
		return false;
	return true;
}


/**
 * Return the item that can be crafted with the given crafting table, or
 * null if there is none.
 */
function get_craftable_item<T extends Item>(
		crafting_table: Item[],
		world: CraftingWorld<T>
		): Item|null {
	for(const item of world.name_to_item.values())
		if("recipe" in item &&
		   is_match_recipe(crafting_table.map(i=>i.name), item.recipe)
		  )
			return item;
	return null;
}


/**
 * Execute the given Minecraft primitive craft operation.
 *
 * If the crafting table matches a craftable item recipe, then a
 * new inventory with the added item and the used items removed is
 * returned. Else, an exception is thrown.
 */
function execute_primitive_craft<T extends Item>(
		state: ProgramState, world: CraftingWorld<T>
		): ProgramState {
	const item = get_craftable_item(state.crafting_table, world);
	if (item !== null)
		return {
			inventory: [...state.inventory, item],
			crafting_table: [],
			held_item: state.held_item,
		};
	throw "Attempted craft but no recipe matches crafting table!";
}

/**
 * Execute the given Minecraft primitive craft operation.
 *
 * If the crafting table matches a craftable item recipe, then a
 * new inventory with the added item and the used items removed is
 * returned. Else, an exception is thrown.
 */
function execute_primitive_clear(
		state: ProgramState,
		): ProgramState {
	let inventory = [...state.inventory, ...state.crafting_table];
	if (state.held_item !== null)
		inventory = [...state.inventory, state.held_item];
	return {
		inventory: inventory,
		crafting_table: [],
		held_item: null,
	};
}


/**
 * Execute the given Minecraft primitive pick operation.
 *
 * If the picked item is in the inventory, remove the
 * item from the inventory and set it as the held item. Else, throw an
 * exception.
 *
 * If an item was held, return it to the inventory.
 */
function execute_primitive_pick(
		program: MinecraftPrimitivePick,
		state: ProgramState,
		): ProgramState {
	let inventory = state.inventory;
	if(state.held_item !== null) {
		inventory = [...state.inventory, state.held_item];
	}
	let inventory_names = inventory.map((item)=>item.name);
	if(!(inventory_names.includes(program.item.name)))
		throw "Cannot pick " + program.item.name + ": item is not in inventory!";

	// Create a new inventory by removing one copy of the placed item from  the
	// old inventory
	let new_inventory = [];
	let removed_one = false;
	for(const item of inventory) {
		if(!(program.item.is_infinite)
		   && !removed_one
		   && item.name === program.item.name
		   ) {
			removed_one = true;
		} else {
			new_inventory.push(item);
		}
	}

	// Build a new crafting table with the item
	const new_crafting_table = [
		...state.crafting_table,
		program.item,
	];

	return {
		inventory: new_inventory,
		crafting_table: new_crafting_table,
		held_item: state.held_item,
	};
}


/**
 * Execute the given Minecraft primitive mine operation.
 */
function execute_primitive_mine<T extends Item>(
		program: MinecraftPrimitiveMine,
		state: ProgramState,
		world: CraftingWorld<T>
		): ProgramState {
	const tool = state.held_item;
	const item = program.mined_item;
	if (!(can_mine(tool, item, world))) {
		const name = (tool === null) ? "hand" : tool.name;
		throw "Cannot mine " + item.name + " with " + name;
	}
	// Build a new inventory
	const yield_n = get_yield(tool, item, world);
	const new_inventory = [...state.inventory];
	for (let i = 0; i < yield_n; i++)
		new_inventory.push(item);

	// Reduce the durability of the held item if appropriate
	if (tool === null || !("durability" in tool))
		return {
			...state,
			inventory: new_inventory
		};
	const new_durability = tool.durability-1;
	const new_held_item = (new_durability > 0) ? {
		...tool,
		durability: new_durability,
	} : null;
	return {
		...state,
		held_item: new_held_item,
		inventory: new_inventory
	};
}


/**
 * Execute the given Minecraft primitive.
 */
function execute_primitive<T extends Item>(
		program: MinecraftPrimitive,
		state: ProgramState,
		world: CraftingWorld<T>
	): ProgramState {
	if(program === MinecraftSimplePrimitive.Clear)
		return execute_primitive_clear(
			state,
		);
	if(program === MinecraftSimplePrimitive.Craft)
		return execute_primitive_craft(
			state,
			world,
		);
	if("item" in program)
		return execute_primitive_pick(
			program,
			state,
		);
	if("mined_item" in program)
		return execute_primitive_mine(
			program,
			state,
			world,
		);
	throw 'Cannot execute ' + JSON.stringify(program);
}


/**
 * Execute the given MinecraftHierarchicalPrimitive.
 */
function execute<T extends Item>(
		program: MinecraftHierarchicalPrimitive,
		state: ProgramState,
		world: CraftingWorld<T>
		): ProgramState {

	if(
			program === MinecraftSimplePrimitive.Craft
			|| program === MinecraftSimplePrimitive.Clear
			|| !("statements" in program)
			) {
		return execute_primitive(
			program,
			state,
			world,
		);
	}

	for(const statement of program.statements) {
		state = execute(
			statement,
			state,
			world,
		);
	}

	return state;
}

export {
	execute,
	is_match_recipe,
	get_craftable_item,
}

export type {
	ProgramState,
}
