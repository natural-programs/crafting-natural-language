import type { ProgramState } from "./semantics";
import type { CraftingWorld, Item } from "./items";

/**
 * Constraints encode statements of the form "these items should be created
 * during execution of this program".
 */
type Constraint = {
	inventory: Item[],
}

/**
 * Return a mapping from elements in the list to the frequency of that element
 * in the list.
 */
function get_counter<T>(a: T[]): Map<T, number> {
	const counter = new Map<T, number>();
	for(const ai of a) {
		if(counter.has(ai))
			counter.set(ai, counter.get(ai) as number + 1);
		else
			counter.set(ai, 1);
	}
	return counter;
}


/**
 * Returns true if and only if the state satisfies the given post_condition.
 */
function post_condition_interpreter(
		post_condition: Constraint,
		old_state: ProgramState,
		new_state: ProgramState
		): boolean {
	// Check inventory (held item is considered part of the inventory
	// for post_condition purposes).
	const old_user_items = [...old_state.inventory]
	const new_user_items = [...new_state.inventory]
	if (old_state.held_item !== null)
		old_user_items.push(old_state.held_item);
	if (new_state.held_item !== null)
		new_user_items.push(new_state.held_item);
	const old_counter = get_counter(old_user_items.map(item=>item.name));
	const new_counter = get_counter(new_user_items.map(item=>item.name));
	const diff_counter = get_counter(post_condition.inventory.map(item=>item.name));
	for(const [id_mc, diff_count] of diff_counter) {
		let old_count = old_counter.get(id_mc);
		if(old_count === undefined)
			old_count = 0;
		let new_count = new_counter.get(id_mc);
		if(new_count === undefined)
			new_count = 0;
		if(new_count < old_count + diff_count) {
			return false;
		}
	}
	return true;
}


/**
 * Returns a string representation of the constraint.
 */
function get_constraint_as_string(constraint: Constraint): string {
	const names = constraint.inventory.map(item=>item.name);
	return JSON.stringify(names);
}


/**
 * De-serializes the given constraint.
 */
function get_constraint_from_string(
		constraint: string,
		world: CraftingWorld<Item>
		): Constraint {
	const names = JSON.parse(constraint) as string[];
	const items = [];
	for (const name of names) {
		const item = world.name_to_item.get(name);
		if (item === undefined)
			throw "Item " + name + " not in crafting world!";
		items.push(item);
	}
	return { inventory: items };
}

/**
 * Interprets a post_condition in the Minecraft post_condition DSL.
 */
function post_condition_string_interpreter<T extends Item>(
		post_condition: string,
		old_state: ProgramState,
		new_state: ProgramState,
		world: CraftingWorld<T>
		): boolean {
	const constraint = get_constraint_from_string(post_condition, world);
	return post_condition_interpreter(constraint, old_state, new_state);
}

export type {
	Constraint,
};

export {
	post_condition_interpreter,
	post_condition_string_interpreter,
	get_constraint_as_string,
	get_constraint_from_string,
	get_counter,
};
