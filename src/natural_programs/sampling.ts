/**
 * Weighted samples over finite distributions.
 */
import type { FiniteDistribution } from "./types";

/**
 * Linearly normalize the scores so that numbers are between 0 and 1 and sum to 1.
 */
function linearly_normalized<T>(distribution: FiniteDistribution<T>): FiniteDistribution<T> {
	if (distribution.length === 0)
		return distribution;
	const og_scores = distribution.map(([_, s])=>s);
	const max = Math.max(...og_scores);
	const min = Math.min(...og_scores);
	if (max === min) return distribution.map(([t, _])=>[t, 1.0]);
	const interpolated: FiniteDistribution<T> = distribution.map(([i, s])=>[i, (s-min)/(max-min)]);
	const interpolated_scores = interpolated.map(([_, s])=>s);
	const sum = interpolated_scores.reduce(function(a, b) { return a+b; });
	if (sum <= 0.0) return distribution.map(([t, _])=>[t, 1.0]);
	return interpolated.map(([i, p])=>[i, p/sum]);
}

/**
 * Normalize the scores so that item numbers are between 0 and 1 and sum
 * to 1 using the softmax function.
 */
function softmaxed<T>(
		distribution: FiniteDistribution<T>,
		): FiniteDistribution<T> {
	if(distribution.length === 0)
		return distribution;
	const scores = distribution.map(([_, score]) => score);
	const max = Math.max(...scores);
	const e_x = scores.map(x=>Math.exp(x - max));
	const sum = e_x.reduce(function(a, b) { return a+b; });
	const new_distribution: FiniteDistribution<T> = [];
	for(let i = 0; i < distribution.length; i++) {
		new_distribution.push([distribution[i][0], e_x[i]/sum]);
	}
	return new_distribution;
}


/**
 * Evaluate the scores as a probability distribution and return the log probabilities.
 */
function logsumexp<T>(
		distribution: FiniteDistribution<T>,
		): FiniteDistribution<T> {
	if(distribution.length == 0)
		return distribution;
	const scores = distribution.map(([_, score]) => score);
	const max = Math.max(...scores);
	const logsumexp_ = max + Math.log(scores.map(xj=>Math.exp(xj-max)).reduce((a, b)=>a+b));
	const new_distribution: FiniteDistribution<T> = [];
	for(let i = 0; i < distribution.length; i++) {
		const logpi = distribution[i][1]-logsumexp_;
		new_distribution.push([distribution[i][0], logpi]);
	}
	return new_distribution;
}


/**
 * Normalize the distribution so that items are sorted from high to low score.
 */
function sorted_distribution<T>(
		distribution: FiniteDistribution<T>
		): FiniteDistribution<T> {
	distribution = [...distribution];
	distribution.sort(function compare(a, b) {
		if(a[1] > b[1]) return -1;
		if(a[1] < b[1]) return 1;
		return 0;
	});
	return distribution;
}


/**
 * Sample from a distribution in log-space.
 */
function get_log_sample<T>(log_distribution: FiniteDistribution<T>): T {
	// https://en.wikipedia.org/wiki/Categorical_distribution#Sampling_via_the_Gumbel_distribution
	// c = argmax_i (dist[i].log_prob + gumbel[i])
	// return dist[c].item
	const gumbel_samples = log_distribution.map(_=>-Math.log(-Math.log(Math.random())));
	const scores: [T, number][] = log_distribution.map(([t, gamma], i)=>[t, gamma+gumbel_samples[i]]);
	return scores
		.reduce(([t, s], [max_t, max_s])=>(max_s < s) ? [t, s] : [max_t, max_s]
	)[0];
}


export {
	sorted_distribution,
	softmaxed,
	logsumexp,
	linearly_normalized,
	get_log_sample,
}
