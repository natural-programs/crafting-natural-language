/**
 * Module to partially execute a natural program sketch.
 */
import type { NaturalProgramSketch } from "natural-programs/types";
import type { CallstackContext } from "natural-programs/types";
import type { NaturalProgram } from "natural-programs/types";
import type { Callstack } from "natural-programs/types";
import type { ConstraintInterpreter } from "natural-programs/types";
import type { ConcreteInterpreter } from "natural-programs/types";
import type { PartialExecutionState } from "natural-programs/types";
import type { AnnotatedNaturalProcedureSketch } from "natural-programs/types";

/**
 * Executes a program sketch, returning the program state execution at, and
 * location of, the first signature. If during execution an error was thrown,
 * it is thrown without change.
 */
function get_partial_execution_state<ProgramState>(
		np: NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>,
		context: CallstackContext<ProgramState>,
		concrete_interpreter: ConcreteInterpreter<ProgramState>,
		post_condition_interpreter: ConstraintInterpreter<ProgramState>,
		): [PartialExecutionState<ProgramState>, NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>] {
	// Primitive case
	if (typeof np === "string") {
		try {
			const new_state = concrete_interpreter(np, context.state);
			return [
				{
					program: np,
					last_state: new_state,
				},
				np
			];
		} catch(e) {
			return [
				{
					np: np,
					error: {execution_exception: JSON.stringify(e)},
					state: context.state,
					context: context,
				},
				np
			];
		}
	}

	// Signature case
	if (!("steps" in np))
		return [
			{ context: context, signature: np },
			np,
		];

	// Natural procedure case

	// Step through all steps in the program
	const annotated_steps: (NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>)[] = [
		...np.steps
	];
	const start_i = ("last_fully_executed_step" in np) ?
		np.last_fully_executed_step.index+1 : 0;
	let state = ("last_fully_executed_step" in np) ? 
		np.last_fully_executed_step.state : context.state;
	const steps: NaturalProgram[] = ("last_fully_executed_step" in np) ?
		[...np.executed_steps] : [];
	for (let i = start_i; i < np.steps.length; i++) {
		const step = np.steps[i];
		const callstack: Callstack = [...context.callstack, [np.signature, i]];
		const step_context = { state: state, callstack: callstack };
		const [result, annotated_step] = get_partial_execution_state(
			step,
			step_context,
			concrete_interpreter,
			post_condition_interpreter,
		);
		annotated_steps[i] = annotated_step;
		if (!("program" in result)) {
			// Execution was not successful. Propagate the error.
			const annotated_np = {
				...np,
				last_fully_executed_step: {index: i, state: state},
				executed_steps: annotated_steps
			}
			return [result, annotated_np];
		}
		// Execution was successful. Update steps and execution trace.
		steps.push(result.program);
		state = result.last_state;
	};

	// Verify the post-condition is satisfied
	if (np.signature.post_condition !== undefined
			&& !(post_condition_interpreter(np.signature.post_condition, context.state, state)))
		return [
			{
				np: np,
				error: {failed_post_condition: np.signature.post_condition},
				state: state,
				context: context,
			},
			{
				...np,
				last_fully_executed_step: {index: steps.length-1, state: state},
				executed_steps: steps,
				steps: annotated_steps,
			}
		];

	// Return the program
	const program = {
		signature: np.signature,
		steps: steps
	};
	const result = {
		program: program,
		last_state: state,
	};
	const annotated_np = {
		...np,
		last_fully_executed_step: {index: steps.length-1, state: state},
		executed_steps: steps,
		steps: annotated_steps
	}
	return [result, annotated_np];
}

export {
	get_partial_execution_state,
};
