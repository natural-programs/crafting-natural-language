/**
 * Fringe-based enumeration of signatures in the library in a depth-first
 * order, so each candidate program is maximally expanded before moving
 * on to the next one.
 * The solver first applies this search strategy to programs in the fringe
 * seed and then starts enumerating the cross-product of the beam in
 * string order.
 */
import type { SolverResult } from "natural-programs/types";
import type { SolverError } from "natural-programs/types";
import type { PartialExecutionState } from "natural-programs/types";
import type { FiniteDistribution } from "natural-programs/types";
import type { NaturalProgramSketch } from "natural-programs/types";
import type { ConstraintInterpreter } from "natural-programs/types";
import type { ConcreteInterpreter } from "natural-programs/types";
import type { NaturalProposer } from "natural-programs/types";
import type { AnnotatedNaturalProcedureSketch } from "natural-programs/types";
import { get_partial_execution_state } from "natural-programs/partial_execution";
import { get_replaced } from "natural-programs/replace";

type BeamSearchParameters = {
	library_beam_width: number,
	propose_beam_width: number,
	search_budget: number,
	sorting_noise_scale: number,
	temperature: number,
	max_top_level_sequence_length: number,
};

/**
 * Search for a concretization of the given sketch that satisfies all
 * constraints.
 *
 * If given a signature, a suffix is searched over L* of the given
 * beam. This only applies to the top-level signature.
 *
 * Returns the first solver error if no solution was found, or the
 * first solution found.
 *
 * Scores in the beam are assumed to be the natural logarithm of a probability
 * distribution.
 *
 * `fringe_seed` are sketches with which to initialize the fringe. Providing
 * an empty array here is fine.
 */
function solver<ProgramState>(
		np: NaturalProgramSketch,
		state: ProgramState,
		beam: ([NaturalProgramSketch, number])[],
		concrete_interpreter: ConcreteInterpreter<ProgramState>,
		post_condition_interpreter: ConstraintInterpreter<ProgramState>,
		proposer: NaturalProposer<ProgramState>,
		parameters: BeamSearchParameters,
		timeout_ms: number,
		max_queue_size: number,
		fringe_seed: NaturalProgramSketch[],
		): SolverResult<ProgramState>|SolverError<ProgramState> {
	const start = Date.now();

	const budget = parameters.search_budget;
	let dequeue_n = 0;

	// Helper function to execute a partial program
	function get_partial(
			np: NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>
			): [
				PartialExecutionState<ProgramState>,
				NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>
			] {
		return get_partial_execution_state(
			np,
			{callstack: [], state: state},
			concrete_interpreter,
			post_condition_interpreter
		);
	}

	// Add things in the seed
	const fringe_seed_copy = [...fringe_seed];
	fringe_seed_copy.sort(function(np1, np2) {
		// These checks are mostly for the type checker.
		if (typeof np1 === "string" || typeof np2 === "string") {
			if (typeof np1 === "string" && typeof np2 === "string")
				return 0;
			else if (typeof np1 === "string" && typeof np2 !== "string")
				return -1;
			else if (typeof np1 !== "string" && typeof np2 === "string")
				return 1;
		} else if ("steps" in np1 && "steps" in np2)
			return np1.steps.length - np2.steps.length;
		else if ("steps" in np1) return 1;
		return -1;
	})
	fringe_seed_copy.reverse();

	// Build depth-first search stacks
	// Each top-level sketch length gets its own stack, so that we try
	// programs of top-level length 1 first, then top-level length 2, etc.
	type Item = NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>;
	// Invariant for the map: only non-empty stacks are in map
	const stacks = new Map<number, Item[]>();
	function enqueue(np: Item) {
		// Compute top-level length for the given item
		const top_level_length = (typeof np === "string" || !("steps" in np)) ? 0 : np.steps.length;

		// Get stack for top-level length
		const maybe_stack = stacks.get(top_level_length);
		const stack = (maybe_stack === undefined) ? [] : maybe_stack;
		if (maybe_stack === undefined)
			stacks.set(top_level_length, stack);

		// Add item to stack
		stack.push(np);
	}
	function dequeue(): Item {
		// First try the seed
		const seed = fringe_seed_copy.pop();
		if (seed !== undefined) {
			return seed;
		}
		const lengths = [...stacks.keys()];
		if (lengths.length === 0)
			throw "Cannot pop from empty stack queue!";
		const min_length = Math.min(...lengths);
		const stack = stacks.get(min_length);
		if (stack === undefined)
			throw "Inconsistent stack!";
		const top = stack.pop();
		if (top === undefined)
			throw "Cannot pop from empty stack!";
		// Preserve map invariant
		if (stack.length === 0)
			stacks.delete(min_length);
		return top;
	}
	function queue_size(): number {
		let size = fringe_seed_copy.length;
		for (const stack of stacks.values())
			size = size + stack.length;
		return size;
	}

	// If given signature, explore cross-product of beam.
	let explore_cross_product = false;
	enqueue(np);

	function termination_criteria(): boolean {
		if (queue_size() === 0) return true;
		if (dequeue_n >= budget) return true;
		if (Date.now()-start > timeout_ms) return true;
		return false;
	}

	// Run best-first search by popping from the queue
	let first_execution_error = null;
	while (!(termination_criteria())) {
		dequeue_n++;
		const top_candidate = dequeue();

		const [result, annotated_top_candidate] = get_partial(top_candidate);
		if ("last_state" in result) {
			// This program finished executing, which means all constraints were
			// successful. Return it.
			return {
				program: result.program,
				final_state: result.last_state,
				dequeue_n: dequeue_n,
				milliseconds: Date.now()-start,
			};
		} else if (!("error" in result)) {
			// The interpreter found a signature, find decompositions.
			const raw_replacements = proposer(result.context, result.signature);
			const perturbed_replacements: FiniteDistribution<NaturalProgramSketch[]> = raw_replacements.map(function([v, score]) {
				const n1 = Math.random()*parameters.sorting_noise_scale;
				return [v, (score+n1)/parameters.temperature];
			});

			// Sort replacements from low-score to high score
			raw_replacements.sort(function([_, s1], [__, s2]) {
				return s1-s2;
			})
			
			// Filter the top replacements
			const top_candidates = perturbed_replacements.slice(
				-parameters.propose_beam_width
			);

			// Get new candidates by performing the replacements on the annotated
			// program. The highest scoring replacement is placed at the top of the stack.
			const unknown_index = result.context.callstack.map(i=>i[1]);
			for (const [replacement_sequence, _] of top_candidates) {
				const replacement_np = {
					signature: result.signature,
					steps: replacement_sequence,
				};
				const new_np = get_replaced(
					annotated_top_candidate,
					unknown_index,
					replacement_np,
				);
				enqueue(new_np);
			}
		} else if ("error" in result) {
			// The program crashed (maybe because a post-condition failed)
			if (first_execution_error === null)
				first_execution_error = result;

			// If the program crashed because the top-level post-condition failed,
			// expand the sequence.
			if (
			    explore_cross_product
			    && (typeof annotated_top_candidate !== "string")
			    && ("steps" in annotated_top_candidate)
			    && ("failed_post_condition" in result.error)
			    && (result.context.callstack.length === 0)
			    && (annotated_top_candidate.steps.length < parameters.max_top_level_sequence_length)
			    ) {
				// Sort beam from low to high
				const sorted_beam = [...beam];
				sorted_beam.sort(function([_, s1], [__, s2]) {
					return s1-s2;
				})
				// Add new programs to the stack. The best program is placed at the top
				// of the stack of its top-level length.
				for (const [np, _] of sorted_beam) {
					const expanded_np = {
						...annotated_top_candidate,
						steps: [...annotated_top_candidate.steps, np]
					};
					enqueue(expanded_np);
				}
			}
		}

		// Once the original queue is empty, explore cross-product if query is
		// signature
		if (queue_size() === 0
		    && !explore_cross_product
		    && (typeof np !== "string")
		    && !("steps" in np)
			 ) {
			console.log("Starting cross-product exploration at dequeue_n: " + dequeue_n);
			explore_cross_product = true;
			const new_np = {signature: np, steps: []};
			enqueue(new_np);
		}
	}
	const termination_reason = (dequeue_n >= budget) ?
		"Exhausted budget!" : (queue_size() === 0) ?
		"Exhausted search space!" : "Aborted search!";
	return {
		error: termination_reason,
		dequeue_n: dequeue_n,
		first_execution_error: first_execution_error,
		milliseconds: Date.now()-start,
	};
};

export {
	solver,
};

export type {
	BeamSearchParameters,
};
