/**
 * LLM-aided solver for Natural Program Signatures.
 *
 * This module can actually be thought of as a front-end to another solver, as
 * the LLM is used to build a set of initial candidate expansions.
 **/
import type { NaturalSignature } from "natural-programs/types";
import type { NaturalProcedureSketch } from "natural-programs/types";
import type { NaturalProgramSketch } from "natural-programs/types";
import type { Primitive } from "natural-programs/types";


/**
 * Return a pretty-printed representation of the given signature.
 **/
function get_pretty_signature(signature: NaturalSignature): string {
	return JSON.stringify(signature);
}


/**
 * Return a compressed version of the given step for building the prompt.
 */
function get_pretty_step(step: NaturalProgramSketch): string {
	if (typeof step === 'string')
		return step;
	if ('steps' in step)
		return get_pretty_step(step.signature);
	// Remove name from signature to use fewer tokens
	const compressed_signature = {...step};
	delete compressed_signature.name;
	return get_pretty_signature(compressed_signature);
}


/**
 * Pseudo-inverse function to the `get_pretty_step` function used to turn
 * LLM completions into a sequence of steps consisting of primitives and
 * signatures.
 */
function get_unpretty_step(
		step: string,
		primitives: string[],
		): NaturalSignature|Primitive {
	const is_primitive = primitives.includes(step);
	if (is_primitive)
		return step;
	return JSON.parse(step);
}

/**
 * Return a string to build a prompt whose completion is supposed to be
 * a top-level decomposition for the given signature.
 *
 * Only the top-level decomposition of each given program will be used.
 **/
function get_prompt(
		signature: NaturalSignature,
		relevant_nps: NaturalProcedureSketch[],
		primitives: string[],
		post_conditions: string[],
		): string {
	const lines = [
		"The task is to convert a sentence of how to craft an item into a small program that crafts the item.",
		"",
		"Here are all the primitive functions that you can use:",
		JSON.stringify(primitives),
		"",
		"When you are placing something, you can only use the functions in this list.",
		"",
		"Here are all the post conditions that you can use:",
		JSON.stringify(post_conditions),
		"",
		"Here are some examples:",
	];

	// Get a pretty-printed representation of the top-level decomposition of each
	// relevant program
	for (const np of relevant_nps) {
		lines.push("Input:");
		lines.push(get_pretty_signature(np.signature));

		// Construct a pretty-printed representation of `np`
		lines.push("Output:");
		lines.push("[");
		const np_lines = [
			...np.steps.map(step=>'    '+get_pretty_step(step)),
		];
		lines.push(...np_lines);
		lines.push("]");

		// Add an empty line between programs
		lines.push("");
	}

	// Add the query signature
	lines.push("Input:");
	lines.push(get_pretty_signature(signature));
	lines.push("What is the output? Give five examples with post conditions and five without.");

	// Concatenate all lines with new lines
	const prompt = lines.join('\n');
	return prompt;
}


/**
 * Parse the given string and signature into a sketch. If the sketch cannot be
 * parsed, an exception is raised.
 *
 * This is an inverse function 
 */
function get_parsed_completion(
		completion: string,
		np: NaturalSignature,
		primitives: string[],
		): NaturalProcedureSketch[] {
	// There is a regex for this, but writing the parsing explicitly
	// makes for more legible code
	const sketches = [];
	let parsing_sketch = false;
	let steps = [];
	for (const line of completion.split('\n')) {
		if (line === "[") {
			parsing_sketch = true;
		} else if (line === "]") {
			const sketch = {
				signature: np,
				steps: steps,
			}
			sketches.push(sketch);
			steps = [];
			parsing_sketch = false;
		} else if (parsing_sketch) {
			try {
				steps.push(get_unpretty_step(line.trim(), primitives));
			} catch {
				// Just try to continue parsing
			}
		} else {
			// Garbage line
		}
	}
	return sketches;
}


/**
 * Return a list of sketches suggested by a LLM. Every completion returned
 * by the completion function will be parsed into a sketch.
 *
 * The completion function is assumed to return the completion suffix, not
 * including the prompt.
 *
 * If the list of prompt procedures is empty, the prompt will not be
 * informative enough to produce suggestions, and thus the list of suggestions
 * will be empty.
 */
async function* get_llm_suggestion_sketches(
		np: NaturalSignature,
		prompt_procedures: NaturalProcedureSketch[],
		primitives: string[],
		post_conditions: string[],
		get_llm_completions: (prompt: string, n: number) => Promise<string[]>,
		suggestion_n: number,
		): AsyncGenerator<NaturalProcedureSketch, void, void> {
	// No point querying the LLM with an empty prompt
	if (prompt_procedures.length === 0) {
		console.error("Cannot ask for suggestion sketches without prompt procedures!");
		return;
	}

	// Form prompt
	const prompt = get_prompt(
		np,
		prompt_procedures,
		primitives,
		post_conditions,
	);

	// Sample LLM for sketches
	let i = 0;
	while (i < suggestion_n) {
		// Ask for one completion at a time to reduce latency
		const completion = (await get_llm_completions(prompt, 1))[0];

		try {
			const np_sketches = get_parsed_completion(completion, np, primitives);
			for (const np_sketch of np_sketches) {
				console.log({event: "parsed sketch", np_sketch: np_sketch, completion: completion});
				yield np_sketch;
			}
		} catch (e) {
			// Completion could not be parsed.
			console.log({
				error: "LLM completion could not be parsed",
				completion: completion,
				parse_error: e,
			});
		}
		i++;
	}
}


export {
	get_llm_suggestion_sketches,
};
