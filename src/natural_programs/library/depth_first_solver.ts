/**
 * Fringe-based enumeration of signatures in the library in a depth-first
 * order, so each candidate program is maximally expanded before moving
 * on to the next one.

 * The solver first applies this search strategy to programs in the fringe
 * seed and then starts enumerating the cross-product of the beam in
 * string order.

 * This solver differs from `depth_first_solver_complete` because it only finds
 * one solution for every step in a decomposition. That is, if a decomposition is
 * `[ A(); B(); c; d]`, this solver will move on to solving `B()` as soon as `A()`
 * is solved. The full solver explores all `A() B()` combinations, but it necessarily
 * moves much slower through the cross-product.
 */
import type { NaturalSignature, SolverResult } from "natural-programs/types";
import type { SolverError } from "natural-programs/types";
import type { PartialExecutionState } from "natural-programs/types";
import type { FiniteDistribution } from "natural-programs/types";
import type { NaturalProgramSketch } from "natural-programs/types";
import type { ConstraintInterpreter } from "natural-programs/types";
import type { ConcreteInterpreter } from "natural-programs/types";
import type { NaturalProposer } from "natural-programs/types";
import type { AnnotatedNaturalProcedureSketch } from "natural-programs/types";
import { get_partial_execution_state } from "natural-programs/partial_execution";
import { get_replaced } from "natural-programs/replace";

type BeamSearchParameters = {
	library_beam_width: number,
	propose_beam_width: number,
	search_budget: number,
	sorting_noise_scale: number,
	temperature: number,
	max_top_level_sequence_length: number,
};

type Item<ProgramState> = NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>;

/**
 * Search for a concretization of the given sketch that satisfies all
 * constraints.
 *
 * If given a signature, a suffix is searched over L* of the given
 * beam. This only applies to the top-level signature.
 *
 * Returns the first solver error if no solution was found, or the
 * first solution found.
 *
 * Scores in the beam are assumed to be the natural logarithm of a probability
 * distribution.
 *
 * `fringe_seed` are sketches with which to initialize the fringe. Providing
 * an empty array here is fine.
 */
function depth_first_solver<ProgramState>(
		np: NaturalProgramSketch,
		state: ProgramState,
		concrete_interpreter: ConcreteInterpreter<ProgramState>,
		post_condition_interpreter: ConstraintInterpreter<ProgramState>,
		proposer: NaturalProposer<ProgramState>,
		parameters: BeamSearchParameters,
		timeout_ms: number,
		max_queue_size: number,
		): SolverResult<ProgramState>&{annotated_sketch: Item<ProgramState>}|SolverError<ProgramState> {
	const start = Date.now();

	const budget = parameters.search_budget;
	let dequeue_n = 0;

	// Helper function to execute a partial program
	function get_partial(
			np: NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>
			): [
				PartialExecutionState<ProgramState>,
				NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>
			] {
		return get_partial_execution_state(
			np,
			{callstack: [], state: state},
			concrete_interpreter,
			post_condition_interpreter
		);
	}

	// Build depth-first search stack
	const stack: Item<ProgramState>[] = [];
	function push(np: Item<ProgramState>) {
		// Avoid memory leak due to unbounded queue
		// There are many ways to implement this.
		if (queue_size() > max_queue_size)
			pop();

		// Add item to stack
		stack.push(np);
	}
	function pop(): Item<ProgramState> {
		const top = stack.pop();
		if (top === undefined)
			throw "Cannot pop from empty stack!";
		return top;
	}
	function queue_size(): number {
		return stack.length;
	}

	// If given signature, explore cross-product of beam.
	push(np);

	function termination_criteria(): boolean {
		if (queue_size() === 0) return true;
		if (dequeue_n >= budget) return true;
		if (Date.now()-start > timeout_ms) return true;
		return false;
	}

	// Run best-first search by popping from the queue
	let first_execution_error = null;
	while (!(termination_criteria())) {
		dequeue_n++;
		const top_candidate = pop();

		const [result, annotated_top_candidate] = get_partial(top_candidate);
		if ("last_state" in result) {
			// This program finished executing, which means all constraints were
			// successful. Return it.
			return {
				program: result.program,
				final_state: result.last_state,
				dequeue_n: dequeue_n,
				milliseconds: Date.now()-start,
				annotated_sketch: annotated_top_candidate,
			};
		} else if (!("error" in result)) {
			// The interpreter found a signature, find decompositions.
			const raw_replacements = proposer(result.context, result.signature);
			const perturbed_replacements: FiniteDistribution<NaturalProgramSketch[]> = raw_replacements.map(function([v, score]) {
				const n1 = Math.random()*parameters.sorting_noise_scale;
				return [v, (score+n1)/parameters.temperature];
			});

			// Sort replacements from low-score to high score,
			// long to short
			perturbed_replacements.sort(function([seq1, s1], [seq2, s2]) {
				if (seq1.length !== seq2.length)
					return seq2.length-seq1.length;
				return s1-s2;
			});
			
			// Filter the top replacements
			const top_candidates = perturbed_replacements.slice(
				-parameters.propose_beam_width
			);

			// Get new candidates by performing the replacements on the annotated
			// program. The highest scoring replacement is placed at the top of the stack.
			const unknown_index = result.context.callstack.map(i=>i[1]);
			for (const [replacement_sequence, _] of top_candidates) {
				const replacement_np = {
					signature: result.signature,
					steps: replacement_sequence,
				};
				const new_np = get_replaced(
					annotated_top_candidate,
					unknown_index,
					replacement_np,
				);
				push(new_np);
			}
		} else if ("error" in result) {
			// The program crashed (maybe because a post-condition failed)
			// There is nothing to do.
		}
	}
	const termination_reason = (dequeue_n >= budget) ?
		"Exhausted budget!" : (queue_size() === 0) ?
		"Exhausted search space!" : "Aborted search!";
	return {
		error: termination_reason,
		dequeue_n: dequeue_n,
		first_execution_error: first_execution_error,
		milliseconds: Date.now()-start,
	};
};


/**
 * Search for a concretization of the given sketch that satisfies all
 * constraints.
 *
 * If given a signature, a suffix is searched over L* of the given
 * beam. This only applies to the top-level signature.
 *
 * Returns the first solver error if no solution was found, or the
 * first solution found.
 *
 * Scores in the beam are assumed to be the natural logarithm of a probability
 * distribution.
 *
 * `fringe_seed` are sketches with which to initialize the fringe. Providing
 * an empty array here is fine.
 */
function enumerative_solver<ProgramState>(
		np: NaturalSignature,
		state: ProgramState,
		beam: ([NaturalProgramSketch, number])[],
		concrete_interpreter: ConcreteInterpreter<ProgramState>,
		post_condition_interpreter: ConstraintInterpreter<ProgramState>,
		proposer: NaturalProposer<ProgramState>,
		parameters: BeamSearchParameters,
		timeout_ms: number,
		max_queue_size: number,
		): SolverResult<ProgramState>|SolverError<ProgramState> {
	const start = Date.now();

	const budget = parameters.search_budget;
	let dequeue_n = 0;
	const first_execution_error = null;
	
	// Construct weak version of signature
	const weak_np = {...np};
	delete weak_np.post_condition;

	function termination_criteria(): boolean {
		if (queue.length === 0) return true;
		if (dequeue_n >= budget) return true;
		if (Date.now()-start > timeout_ms) return true;
		return false;
	}
	
	// Helper function to keep track of dequeue number
	function inner_solver(np: Item<ProgramState>) {
		//const elapsed_ms = Date.now()-start;
		const result = depth_first_solver(
			np,
			state,
			concrete_interpreter,
			post_condition_interpreter,
			proposer,
			parameters,
			//timeout_ms-elapsed_ms,
			10,
			max_queue_size,
		);
		return result;
	}
	

	// Initialize queue. To make it really fast, the queue
	// contains PartialExecutionState
	// of the weak version of each candidate (i.e. without
	// top-level constraint).
	const queue: Item<ProgramState>[] = [];
	function enqueue(np: Item<ProgramState>) {
		queue.push(np);
	}
	function dequeue() {
		const top = queue.shift();
		if (top === undefined)
			throw "Cannot dequeue from empty queue!";
		return top;
	}

	// Start enumerating singleton sequences
	const sorted_beam = [...beam];
	beam.sort(([_, s1], [__, s2]) => s2-s1)
	for (const [beam_np, _] of sorted_beam)
		enqueue({
			signature: weak_np,
			steps: [beam_np],
		});

	while (!termination_criteria()) {
		// Dequeue sequence with no top-level constraint
		const current_np = dequeue();

		// Solve easy synthesis problem
		const result = inner_solver(current_np);
		dequeue_n = dequeue_n + result.dequeue_n;

		if (!("error" in result)) {
			// Check if concrete candidate also satisfies constraint
			// (use annotated sketch to avoid redundant interpreter calls)
			const annotated_sketch = result.annotated_sketch;
			if (typeof annotated_sketch !== "string"
				  && "executed_steps" in annotated_sketch) {
				const candidate = {
					...annotated_sketch,
					signature: np,
				}
				const candidate_result = inner_solver(candidate);

				// If execution was successful, that means the concrete
				// candidate is a solution
				if (!("error" in candidate_result)) {
					return {
						program: candidate_result.program,
						final_state: result.final_state,
						dequeue_n: dequeue_n,
						milliseconds: Date.now()-start,
					}
				}

				// Otherwise, enqueue cross-product expansions
				if (annotated_sketch.executed_steps.length < parameters.max_top_level_sequence_length) {
					for (const [np, _] of sorted_beam) {
						const expanded_steps = [...annotated_sketch.executed_steps, np];
						const expansion = {
							...annotated_sketch,
							steps: expanded_steps,
						};
						enqueue(expansion);
					}
				}
			}
		}
	}

	const termination_reason = (dequeue_n >= budget) ?
		"Exhausted budget!" : (queue.length === 0) ?
		"Exhausted search space!" : "Aborted search!";
	return {
		error: termination_reason,
		dequeue_n: dequeue_n,
		first_execution_error: first_execution_error,
		milliseconds: Date.now()-start,
	};
};


/**
 * Search for a concretization of the given sketch that satisfies all
 * constraints.
 *
 * If given a signature, a suffix is searched over L* of the given
 * beam. This only applies to the top-level signature.
 *
 * Returns the first solver error if no solution was found, or the
 * first solution found.
 *
 * Scores in the beam are assumed to be the natural logarithm of a probability
 * distribution.
 *
 * `fringe_seed` are sketches with which to initialize the fringe. Providing
 * an empty array here is fine.
 *
 * This solver assumes that decompositions are perfect: any program that
 * satisfies the constraint of a step "fits" into the corresponding step of the decomposition.
 */
function solver<ProgramState>(
		np: NaturalProgramSketch,
		state: ProgramState,
		beam: ([NaturalProgramSketch, number])[],
		concrete_interpreter: ConcreteInterpreter<ProgramState>,
		post_condition_interpreter: ConstraintInterpreter<ProgramState>,
		proposer: NaturalProposer<ProgramState>,
		parameters: BeamSearchParameters,
		timeout_ms: number,
		max_queue_size: number,
		fringe_seed: NaturalProgramSketch[],
		): SolverResult<ProgramState>|SolverError<ProgramState> {
	if (typeof np === "string" || "steps" in np)
		return depth_first_solver(
			np,
			state,
			concrete_interpreter,
			post_condition_interpreter,
			proposer,
			parameters,
			timeout_ms,
			max_queue_size,
		);

	const start = Date.now();

	// Sort seed by top-level length, from short to long
	const sorted_fringe_seed = [...fringe_seed];
	sorted_fringe_seed.sort(function(np1, np2) {
		// These checks are mostly for the type checker.
		if (typeof np1 === "string" || typeof np2 === "string") {
			if (typeof np1 === "string" && typeof np2 === "string")
				return 0;
			else if (typeof np1 === "string" && typeof np2 !== "string")
				return -1;
			else if (typeof np1 !== "string" && typeof np2 === "string")
				return 1;
		} else if ("steps" in np1 && "steps" in np2)
			return np1.steps.length - np2.steps.length;
		else if ("steps" in np1) return 1;
		return -1;
	})
	
	// Try each program in the seed
	for (const quick_np of [...sorted_fringe_seed, np]) {
		const result = depth_first_solver(
			quick_np,
			state,
			concrete_interpreter,
			post_condition_interpreter,
			proposer,
			parameters,
			Math.min(timeout_ms-(Date.now()-start), 3000),
			max_queue_size,
		);
		if (!("error" in result))
			return result;
	}
	
	// Enumerative solver on short sequences of primitives
	const primitive_beam = beam.filter(([np, _])=>typeof np === "string")
	const result = enumerative_solver(
		np,
		state,
		primitive_beam,
		concrete_interpreter,
		post_condition_interpreter,
		proposer,
		{...parameters, max_top_level_sequence_length: 3},
		timeout_ms-(Date.now()-start),
		max_queue_size,
	);
	if (!("error" in result))
		return result;

	// Last resort, call enumerative solver
	return enumerative_solver(
		np,
		state,
		beam,
		concrete_interpreter,
		post_condition_interpreter,
		proposer,
		parameters,
		timeout_ms-(Date.now()-start),
		max_queue_size,
	);
};


export {
	solver,
};

export type {
	BeamSearchParameters,
};
