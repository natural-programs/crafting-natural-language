/**
 * Fringe-based enumeration of signatures in the library.
 */
import { logsumexp } from "natural-programs/sampling";
import { sorted_distribution } from "natural-programs/sampling";
import type { SolverResult } from "natural-programs/types";
import type { SolverError } from "natural-programs/types";
import type { PartialExecutionState } from "natural-programs/types";
import type { FiniteDistribution } from "natural-programs/types";
import type { NaturalProgramSketch } from "natural-programs/types";
import type { ConstraintInterpreter } from "natural-programs/types";
import type { ConcreteInterpreter } from "natural-programs/types";
import type { NaturalProposer } from "natural-programs/types";
import type { AnnotatedNaturalProcedureSketch } from "natural-programs/types";
import { get_partial_execution_state } from "natural-programs/partial_execution";
import { get_replaced } from "natural-programs/replace";
import { PriorityQueue } from '@datastructures-js/priority-queue';

type BeamSearchParameters = {
	library_beam_width: number,
	propose_beam_width: number,
	search_budget: number,
	sorting_noise_scale: number,
	temperature: number,
	max_top_level_sequence_length: number,
};

/**
 * Search for a concretization of the given sketch that satisfies all
 * constraints.
 *
 * If given a signature, a suffix is searched over L* of the given
 * beam. This only applies to the top-level signature.
 *
 * Returns the first solver error if no solution was found, or the
 * first solution found.
 *
 * Scores in the beam are assumed to be the natural logarithm of a probability
 * distribution.
 *
 * `fringe_seed` are sketches with which to initialize the fringe. Providing
 * an empty array here is fine.
 */
function solver<ProgramState>(
		np: NaturalProgramSketch,
		state: ProgramState,
		beam: ([NaturalProgramSketch, number])[],
		concrete_interpreter: ConcreteInterpreter<ProgramState>,
		post_condition_interpreter: ConstraintInterpreter<ProgramState>,
		proposer: NaturalProposer<ProgramState>,
		parameters: BeamSearchParameters,
		timeout_ms: number,
		max_queue_size: number,
		fringe_seed: NaturalProgramSketch[],
		): SolverResult<ProgramState>|SolverError<ProgramState> {
	const start = Date.now();

	const budget = parameters.search_budget;
	let dequeue_n = 0;

	// Helper function to execute a partial program
	function get_partial(
			np: NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>
			): [
				PartialExecutionState<ProgramState>,
				NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>
			] {
		return get_partial_execution_state(
			np,
			{callstack: [], state: state},
			concrete_interpreter,
			post_condition_interpreter
		);
	}

	// Build priority queue
	type Item = {
		np: NaturalProgramSketch|AnnotatedNaturalProcedureSketch<ProgramState>,
		log_probs: number[],
		cost: number
	};
	const pqueue = new PriorityQueue<Item>(function(a, b) {
		if (
		     (typeof a.np !== "string")
		     && (typeof b.np !== "string")
		     && (("steps" in a.np) && ("steps" in b.np))
		   ) {
		// First shorter procedures
			if (a.np.steps.length < b.np.steps.length)
				return -1;
			if (a.np.steps.length > b.np.steps.length)
				return 1;
		}

		// Then sort by cost
		return (a.cost < b.cost) ? -1 : 1;
	});

	// Helper function to enqueue and cap queue size
	function enqueue(item: Item) {
		pqueue.enqueue(item);

		if (pqueue.size() > max_queue_size)
			pqueue.dequeue();
	}

	// If given signature, explore cross-product of beam.
	let explore_cross_product = false;
	pqueue.enqueue({np: np, log_probs: [0.0], cost: 0.0});

	// Add things in the seed
	for (const seed_np of fringe_seed) {
		const cost = (typeof seed_np !== "string" && "steps" in seed_np) ?
			seed_np.steps.length : 0.0;
		pqueue.enqueue({np: seed_np, log_probs: [0.0], cost: cost});
	}

	function termination_criteria(): boolean {
		if (pqueue.size() === 0) return true;
		if (dequeue_n >= budget) return true;
		if (Date.now()-start > timeout_ms) return true;
		return false;
	}

	// Run best-first search by popping from the queue
	let first_execution_error = null;
	while (!(termination_criteria())) {
		dequeue_n++;
		const top_candidate = pqueue.dequeue();

		const [result, annotated_top_candidate] = get_partial(top_candidate.np);
		if ("last_state" in result) {
			// This program finished executing, which means all constraints were
			// successful. Return it.
			return {
				program: result.program,
				final_state: result.last_state,
				dequeue_n: dequeue_n,
				milliseconds: Date.now()-start,
			};
		} else if (!("error" in result)) {
			// The interpreter found a signature, find decompositions.
			const raw_replacements = proposer(result.context, result.signature);
			const perturbed_replacements: FiniteDistribution<NaturalProgramSketch[]> = raw_replacements.map(function([v, score]) {
				const n1 = Math.random()*parameters.sorting_noise_scale;
				return [v, (score+n1)/parameters.temperature];
			});
			const replacements = logsumexp(sorted_distribution(perturbed_replacements));
			const top_candidates = replacements.slice(
				0,
				parameters.propose_beam_width
			);

			// Get new candidates by performing the replacements on the annotated
			// program
			const unknown_index = result.context.callstack.map(i=>i[1]);
			const prev_cost = top_candidate.cost;
			for (const [replacement_sequence, replacement_logprob] of top_candidates) {
				const replacement_np = {
					signature: result.signature,
					steps: replacement_sequence,
				};
				const new_np = get_replaced(
					annotated_top_candidate,
					unknown_index,
					replacement_np,
				);
				const log_probs = [...top_candidate.log_probs, replacement_logprob];
				enqueue({
					np: new_np,
					log_probs: log_probs,
					cost: Math.max(prev_cost, -replacement_logprob),
				});
			}
		} else if ("error" in result) {
			// The program crashed (maybe because a post-condition failed)
			if (first_execution_error === null)
				first_execution_error = result;

			// If the program crashed because the top-level post-condition failed,
			// expand the sequence.
			if (
			    explore_cross_product
			    && (typeof annotated_top_candidate !== "string")
			    && ("steps" in annotated_top_candidate)
			    && ("failed_post_condition" in result.error)
			    && (result.context.callstack.length === 0)
			    && (annotated_top_candidate.steps.length < parameters.max_top_level_sequence_length)
			    ) {
				const prev_cost = top_candidate.cost;
				for (const [np, log_prob] of beam) {
					const expanded_np = {
						...annotated_top_candidate,
						steps: [...annotated_top_candidate.steps, np]
					};
					const log_probs = [...top_candidate.log_probs, log_prob];
					enqueue({
						np: expanded_np,
						log_probs: log_probs,
						cost: Math.max(prev_cost, -log_prob),
					});
				}
			}
		}

		// Once the original queue is empty, explore cross-product if query is
		// signature
		if (pqueue.size() === 0
		    && !explore_cross_product
		    && (typeof np !== "string")
		    && !("steps" in np)
			 ) {
			console.log("Starting cross-product. Dequeue_n: " + dequeue_n);
			explore_cross_product = true;
			const new_np = {signature: np, steps: []};
			pqueue.enqueue({np: new_np, log_probs: [0.0], cost: 0.0});
		}
	}
	const termination_reason = (dequeue_n >= budget) ?
		"Exhausted budget!" : (pqueue.size() === 0) ?
		"Exhausted search space!" : "Aborted search!";
	return {
		error: termination_reason,
		dequeue_n: dequeue_n,
		first_execution_error: first_execution_error,
		milliseconds: Date.now()-start,
	};
}

export {
	solver,
};

export type {
	BeamSearchParameters,
};
