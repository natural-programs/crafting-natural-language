/**
 * Naive enumeration of signatures in the library.
 */
import type { SolverResult } from "natural-programs/types";
import type { Library } from "./library";
import type { SolverError } from "natural-programs/types";
import type { SuccessfulExecution } from "natural-programs/types";
import type { PartialExecutionState } from "natural-programs/types";
import type { NaturalProgram } from "natural-programs/types";
import type { NaturalProgramSketch } from "natural-programs/types";
import type { Callstack } from "natural-programs/types";
import type { ConstraintInterpreter } from "natural-programs/types";
import type { ConcreteInterpreter } from "natural-programs/types";
import type { CallstackContext } from "natural-programs/types";
import { get_concrete_implementations } from "./library";
import { PriorityQueue } from '@datastructures-js/priority-queue';


/**
 * Helper function to replace a node in `np`, indexed by `index`, with
 * `new_tree`.
 */
function get_replaced(np: NaturalProgramSketch, index: number[], replacement: NaturalProgramSketch): NaturalProgramSketch {
	if (index.length === 0)
		return replacement;
	if (typeof np === "string" || !("steps" in np))
		throw 'Node index does not exist in NaturalProgramSketch';
	const new_steps = [];
	for (let i = 0; i < np.steps.length; i++) {
		const original_step = np.steps[i];
		const step = (i === index[0]) ?
			get_replaced(original_step, index.slice(1), replacement)
			:
			original_step;
		new_steps.push(step);
	}
	const new_tree = {
		signature: np.signature,
		steps: new_steps,
	};
	return new_tree;
}


type AnnotatedNaturalProgramSketch<ProgramState> = NaturalProgramSketch & {
	successful_execution?: SuccessfulExecution<ProgramState>
};


/**
 * Executes a program sketch, returning the program state execution at, and
 * location of, the first signature. If during execution an error was thrown,
 * it is thrown without change.
 */
function get_partial_execution_state<ProgramState>(
		np: AnnotatedNaturalProgramSketch<ProgramState>,
		context: CallstackContext<ProgramState>,
		concrete_interpreter: ConcreteInterpreter<ProgramState>,
		post_condition_interpreter: ConstraintInterpreter<ProgramState>,
		): [PartialExecutionState<ProgramState>, AnnotatedNaturalProgramSketch<ProgramState>] {
	// Primitive case
	if (typeof np === "string") {
		try {
			const new_state = concrete_interpreter(np, context.state);
			return [
				{
					program: np,
					last_state: new_state,
				},
				np
			];
		} catch(e) {
			return [
				{
					np: np,
					error: {execution_exception: JSON.stringify(e)},
					state: context.state,
					context: context,
				},
				np
			];
		}
	}

	// Signature case
	if (!("steps" in np))
		return [
			{ context: context, signature: np },
			np,
		];

	// Natural procedure case
	if (np.successful_execution !== undefined)
		return [np.successful_execution, np];
	const steps: NaturalProgram[] = [];
	let state = context.state;

	// Step through all steps in the program
	const annotated_steps: AnnotatedNaturalProgramSketch<ProgramState>[] = [
		...np.steps
	];
	for (let i = 0; i < np.steps.length; i++) {
		const step = np.steps[i];
		const callstack: Callstack = [...context.callstack, [np.signature, i]];
		const step_context = { state: state, callstack: callstack };
		const [result, annotated_step] = get_partial_execution_state(
			step,
			step_context,
			concrete_interpreter,
			post_condition_interpreter,
		);
		annotated_steps[i] = annotated_step;
		if (!("program" in result)) {
			// Execution was not successful. Propagate the error.
			return [result, {...np, steps: annotated_steps}];
		}
		// Execution was successful. Update steps and execution trace.
		steps.push(result.program);
		state = result.last_state;
	};

	// Verify the post-condition is satisfied
	if (np.signature.post_condition !== undefined
			&& !(post_condition_interpreter(np.signature.post_condition, context.state, state)))
		return [
			{
				np: np,
				error: {failed_post_condition: np.signature.post_condition},
				state: state,
				context: context,
			},
			{
				...np,
				steps: annotated_steps,
			}
		];

	// Return the program
	const program = {
		signature: np.signature,
		steps: steps
	};
	const result = {
		program: program,
		last_state: state,
	};
	return [
		result,
		{
			...np,
			successful_execution: result,
			steps: annotated_steps
		}
	];
}


/**
 * Return the mean of the list of numbers.
 */
function get_mean(l: number[]): number {
	return l.reduce((a, b)=>a+b)/l.length;
}

/**
 * Calls the given solver using the cross-product of library functions on each
 * sequence. Returns the first solver error if no solution was found, or the
 * first solution found.
 */
function exact_solver<ProgramState>(
		np: AnnotatedNaturalProgramSketch<ProgramState>,
		state: ProgramState,
		library: Library,
		concrete_interpreter: ConcreteInterpreter<ProgramState>,
		post_condition_interpreter: ConstraintInterpreter<ProgramState>,
		): SolverResult<ProgramState>|SolverError<ProgramState> {
	const start = Date.now();

	let dequeue_n = 0;

	// Helper function to execute a partial program
	function get_partial(
			np: AnnotatedNaturalProgramSketch<ProgramState>
			): [
				PartialExecutionState<ProgramState>,
				AnnotatedNaturalProgramSketch<ProgramState>
			] {
		return get_partial_execution_state(
			np,
			{callstack: [], state: state},
			concrete_interpreter,
			post_condition_interpreter
		);
	}

	// Build priority queue
	const pqueue = new PriorityQueue<{np: AnnotatedNaturalProgramSketch<ProgramState>, log_probs: number[]}>(function(a, b) {
		return get_mean(b.log_probs) - get_mean(a.log_probs);
	})

	pqueue.enqueue({np: np, log_probs: [0.0]});

	function termination_criteria(): boolean {
		if (pqueue.size() === 0) return true;
		return false;
	}

	// Run best-first search by popping from the queue
	let first_execution_error = null;
	while (!(termination_criteria())) {
		dequeue_n++;
		const top_candidate = pqueue.dequeue();

		const [result, annotated_top_candidate] = get_partial(top_candidate.np);
		if ("last_state" in result) {
			// This program finished executing, which means all constraints were
			// successful. Return it.
			return {
				program: result.program,
				final_state: result.last_state,
				dequeue_n: dequeue_n,
				milliseconds: Date.now()-start,
			};
		} else if (!("error" in result)) {
			// The interpreter found a signature. Replace with a concrete version, if
			// any.
			const unknown_index = result.context.callstack.map(i=>i[1]);
			const implementations = get_concrete_implementations(library, result.signature);

			if (implementations.length > 0) {
				const new_np = get_replaced(
					annotated_top_candidate,
					unknown_index,
					implementations[0],
				);
				pqueue.enqueue({
					np: new_np,
					log_probs: [...top_candidate.log_probs, 0],
				});
			}
		} else if ("error" in result) {
			// The program crashed
			if (first_execution_error === null)
				first_execution_error = result;
		}
	}

	return {
		error: "Execution failed",
		dequeue_n: dequeue_n,
		first_execution_error: first_execution_error,
		milliseconds: Date.now()-start,
	};
};

export {
	exact_solver,
};
