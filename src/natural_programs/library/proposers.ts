import type { NaturalProcedure, NaturalProgramSketch, Primitive } from "natural-programs/types";
import type { FiniteDistribution } from "natural-programs/types";
import type { NaturalSignature } from "natural-programs/types";
import { assign_id, get, get_canonical_callstack, get_concrete_implementations, get_decomposition_recency_scores, get_ids_with_post_condition, get_sequences, type ItemID } from "./library";
import type { CallstackContext } from "natural-programs/types";
import type { Library } from "./library";
import { linearly_normalized } from "natural-programs/sampling";

/**
 * An library-based abstract proposer type for the minecraft domain.
 */
type LibraryProposer<ProgramState> = (
	context: CallstackContext<ProgramState>,
	signature: NaturalSignature,
	library: Library,
	) => Promise<FiniteDistribution<(NaturalProgramSketch)[]>>;

type ProposeParameters = {
	recency_weight: number,
	frequency_weight: number,
};

/**
 * Use callstack and post-condition to return expansions relevant to
 * the given signature.
 */
function decomposition_proposer<ProgramState>(
		context: CallstackContext<ProgramState>,
		signature: NaturalSignature,
		library: Library,
		parameters: ProposeParameters,
		): FiniteDistribution<(Primitive|NaturalSignature)[]> {
	// Helper to assign sequences the highest awarded score
	const frequencies = new Map<string, [ItemID[], number]>();
	function update_frequency(sequence: ItemID[], frequency: number) {
		// Do not include paraphrases
		if (sequence.length <= 1)
			return;
		const key = JSON.stringify(sequence);
		const old = frequencies.get(key);
		if(old === undefined) {
			frequencies.set(key, [sequence, frequency]);
		} else {
			frequencies.set(key, [sequence, old[1]+frequency]);
		}
	}

	// Look-up expansions of the given signature 
	//
	const known_decompositions = get_sequences(library, assign_id(library, signature));
	known_decompositions.reverse();
	for(const sequence of known_decompositions)
		update_frequency(sequence.sequence, sequence.frequency);

	// Find signatures with identical post-conditions
	const post_condition = signature.post_condition;
	if (post_condition !== undefined)
	for (const id of get_ids_with_post_condition(library, post_condition))
	for (const decomposition of get_sequences(library, id))
		update_frequency(decomposition.sequence, decomposition.frequency);


	// Get recency scores
	const decompositions = [];
	for (const [sequence, _] of frequencies.values()) {
		const _sequence = [];
		for (const id of sequence)  {
			const item = get(library, id);
			if (item === undefined)
				throw "Cannot find item! Did library change?";
			_sequence.push(item);
		}
		decompositions.push(_sequence);
	}
	const recency_scores = get_decomposition_recency_scores(
		library,
		decompositions,
	);

	const distribution: FiniteDistribution<(Primitive|NaturalSignature)[]> = [];
	const max_frequency = Math.max(...[...frequencies.values()].map(f=>f[1]));
	const canonical_callstack = get_canonical_callstack(library, context.callstack);
	const ids_in_callstack = canonical_callstack.map(function([id, _]) { return id; });
	const freqs = [...frequencies.values()];
	for(let i = 0; i < freqs.length; i++) {
		const [sequence, frequency] = freqs[i];
		// Ignore decompositions that induce recursion to avoid the possibility of
		// infinite recursion when expanding a program
		let doing_recursion = false;
		for (const id of sequence) {
			doing_recursion = doing_recursion || ids_in_callstack.includes(id);
		}
		if(doing_recursion)
			continue;
		const _sequence = [];
		for (const id of sequence)  {
			const item = get(library, id);
			if (item === undefined)
				throw "Cannot find item! Did library change?";
			_sequence.push(item);
		}

		// Compute the score of the decomposition
		const recency_score = recency_scores[i];
		const frequency_score = frequency/max_frequency;
		const score = parameters.recency_weight*recency_score + parameters.frequency_weight*frequency_score;
		distribution.push([_sequence, score]);
	}
	return linearly_normalized(distribution);
}

/**
 * Use post-condition and natural language to return expansions relevant
 * to the given signature.
 */
function concrete_proposer<ProgramState>(
		_: CallstackContext<ProgramState>,
		signature: NaturalSignature,
		library: Library,
		): FiniteDistribution<(Primitive|NaturalProcedure)[]> {
	// Helper to assign sequences the highest awarded score
	const scores = new Map<ItemID, number>();
	function maybe_update_score(key: ItemID, score: number) {
		const old = scores.get(key);
		if(old === undefined) {
			scores.set(key, score);
		} else {
			scores.set(key, Math.max(old, score));
		}
	}

	// Consider the signature itself
	maybe_update_score(assign_id(library, signature), 1.0);

	const post_condition = signature.post_condition;
	if (post_condition !== undefined) {
		// Find signatures with identical post-conditions
		for (const id of get_ids_with_post_condition(library, post_condition))
			maybe_update_score(id, 1.0);
	}

	const distribution: FiniteDistribution<(Primitive|NaturalProcedure)[]> = [];
	const added_items = new Set<string>();
	for(const [id, score] of scores.entries()) {
		const item = get(library, id);
		if (item === undefined)
			throw "Cannot find item! Did library change?";
		const key = JSON.stringify(item);
		// Only add implementations once
		if (added_items.has(key))
			continue;
		added_items.add(key);
		if (typeof item === "string")
			distribution.push([[item], score]);
		else for (const concrete of get_concrete_implementations(library, item))
			distribution.push([[concrete], score]);
	}
	return linearly_normalized(distribution);
}

export type {
	LibraryProposer,
	ProposeParameters,
};

export {
	decomposition_proposer,
	concrete_proposer,
};
