/**
 * Semantic search functionality using the SBERT deployed to our own end-point
 * See: https://gitlab.com/da_doomer/sentence-transformers-server
 */

const EMBEDDINGS_API_URL = "https://nlp.iamleo.space/embedding";
const headers = {
	'Content-Type': 'application/json',
};


const embedding_cache = new Map<string, number[]>();
/*
 * Use a language model to compute a embedding for the given documents. This
 * receives an entire array of documents to encourage batch queries and reduce
 * HTTP requests to the API.
 */
async function get_embeddings(documents: string[], timeout: number): Promise<number[][]> {
	// Query the API for documents whose embedding is not in the cache
	const documents_not_cached = new Set<string>();
	for(const document of documents)
	if(!(embedding_cache.has(document)))
		documents_not_cached.add(document);
	if(documents_not_cached.size > 0) {
		const docs = [...documents_not_cached];
		const data = {
			documents: docs,
		};
		const controller = new AbortController();
		setTimeout(() => controller.abort(), timeout);
		const response = await fetch(EMBEDDINGS_API_URL, {
			headers: headers,
			method: "POST",
			mode: "cors",
			body: JSON.stringify(data),
			signal: controller.signal,
		});
		const res = await response.json();
		for(let i = 0; i < documents_not_cached.size; i++)
			embedding_cache.set(docs[i], res["embeddings"][i]);
	}

	// By now every document is in the cache
	const res = [];
	for(const document of documents)
		res.push(embedding_cache.get(document) as number[]);
	return res;
}


/**
 * Return the cosine similarity between the two vectors.
 */
function cosine_similarity(v1: number[], v2: number[]): number {
	if(v1.length != v2.length)
		throw 'Cannot compute cosine similarity between different size vectors!';
	if(v1.length == 0)
		throw 'Cannot compute cosine similarity between size-0 vectors!';
	let dot = 0.0;
	let norm1 = 0.0;
	let norm2 = 0.0;
	for(let i = 0; i < v1.length; i++){
		dot += v1[i]*v2[i];
		norm1 += v1[i]*v1[i];
		norm2 += v2[i]*v2[i];
	}
	return dot/(Math.sqrt(norm1)*Math.sqrt(norm2));
}


/*
 * Use a language model to compute string similarity between `query` and
 * each string in `documents`.
 * The resulting numbers are between 1 (similar) and 0 (different).
 */
async function get_similarities(
		query: string,
		documents: string[],
		timeout: number
		): Promise<number[]> {
	const embeddings = await get_embeddings([query, ...documents], timeout);
	const v = embeddings[0];
	const vs = embeddings.slice(1);
	const similarities = [];
	for(const vsi of vs)
		similarities.push((1+cosine_similarity(v, vsi))/2);
	return similarities;
}


/**
 * Sort strings by similarity to the given string, from similar to not-similar.
 */
async function get_sorted_by_similarity(
		documents: string[],
		query: string,
		timeout: number,
		): Promise<string[]> {
	const similarities = await get_similarities(query, documents, timeout);
	const labeled_documents: [string, number][] = documents.map((d, i) => [d, similarities[i]]);
	labeled_documents.sort(function(a, b) {
		return a[1]-b[1];
	});
	const sorted_documents = labeled_documents.map((val)=>val[0]);
	return sorted_documents;
}


export {
	get_similarities,
	get_embeddings,
	cosine_similarity,
	get_sorted_by_similarity,
};
