/**
 * This module defines the translation model type.
 *
 * A translation model is an asynchronous mapping from a string to a list of
 * lists of strings. Each list of strings represents a possible translation.
 * The strings are sorted in decreasing order of likelihood or score.
 */

type TranslationModel = (query: string) => Promise<string[][]>;


export type {
	TranslationModel,
};

