/**
 * Implements an automatic session debugger.
 */
import type { NaturalSignature } from "natural-programs/types";
import type { ProgramState } from "minecraft/semantics";
import type { CraftingWorld } from "minecraft/items";
import type { SourcedResult } from "./solver";
import type { Item } from "minecraft/items";
import { get_items } from "minecraft/items";
import { get_constraint_as_string } from "minecraft/constraints";

async function* run_automatic_session(
		solver: (
			np: NaturalSignature,
			state: ProgramState
			) => Promise<SourcedResult>,
		initial_state: ProgramState,
		crafting_world: CraftingWorld<Item>,
		timeout_ms: number|null,
		): AsyncGenerator<string, void, void> {
	const items = get_items(crafting_world);
	let state = initial_state;
	const start_date = Date.now();
	const result_success = []; 
	for (const item of items) {
		if (!("recipe" in item))
			continue;
		let prev_inv_size = state.inventory.length;
		// Try to craft the item until the pre requisites are included
		// i.e., the inventory is not consumed
		while (!(state.inventory.length >= prev_inv_size+1)) {
			prev_inv_size = state.inventory.length;
			const elapsed_time = Date.now() - start_date;
			if (timeout_ms !== null && elapsed_time > timeout_ms)
				break;
			if (!('recipe' in item))
				continue;

			// Construct signature
			const item_name = "'" + item.name + "'";
			const prereqs = item.recipe.map(n=>"'"+n+"'").join(" and ");
			const signature_name = 'please craft ' + item_name + ' with ' + prereqs;
			const signature: NaturalSignature = {
				name: signature_name,
				post_condition: get_constraint_as_string({inventory: [item]})
			};

			// Log current attempt
			yield "USER: attempting to craft " + item_name;
			yield "USER: '" + signature_name + "'";

			// Run solver
			const solver_start_date = Date.now();
			try {
				const result = await solver(signature, state);
				const time = Math.round(Date.now()-solver_start_date)/1000;

				if ("error" in result)
					throw result;

				// Log
				yield "SYSTEM: solver succeeded in " + time + "s";
				yield "SYSTEM: source solver '" + result.source_solver + "'";
				yield JSON.stringify(result.program, undefined, 2);

				// Update state
				state = result.final_state;
				result_success.push(1);
			} catch {
				const time = Math.round(Date.now()-solver_start_date)/1000;
				yield "SYSTEM: solver failed in " + time + "s";
				result_success.push(0);
				break;
			}

			// Log
			yield "";
		}

		// Log
		yield "";
	}

	// Log final stats
	const successes = result_success.reduce((pv, cv) => pv + cv, 0);
	const total_time = Math.round(Date.now() - start_date)/1000;
	yield "SYSTEM: success rate " + successes + "/" + result_success.length;
	yield "SYSTEM: total time: " + total_time + "s";
}

export {
	run_automatic_session,
};
