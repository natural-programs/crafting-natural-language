import { solver } from "natural-programs/library/depth_first_solver";
import type { SynthesisProblem } from "./minecraft_app";
import { concrete_interpreter } from "./minecraft_app";
import { post_condition_string_interpreter } from "minecraft/constraints";
import { decomposition_proposer } from "natural-programs/library/proposers";

onmessage = function(message: MessageEvent<SynthesisProblem>) {
	const problem = message.data;
	const result = solver(
		problem.np,
		problem.state,
		problem.beam,
		(p, s)=>concrete_interpreter(p, s, problem.api, problem.world),
		(p, o, n)=>post_condition_string_interpreter(p, o, n, problem.world),
		(c, s)=>decomposition_proposer(c, s, problem.library, problem.propose_parameters),
		problem.parameters,
		problem.timeout_ms,
		problem.max_queue_size,
		problem.fringe_seed,
	);
	postMessage(result);
}
