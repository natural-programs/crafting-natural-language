import type { TopLevelSpec } from "vega-lite";
import type { SurveyResponse } from "./survey";
import {
	get_chain,
	get_chain_successful_executions,
	get_chain_failed_executions,
	get_session,
	get_snapshot,
	get_snapshot_sat_items,
	get_snapshot_show_library_ms,
	get_snapshot_show_recipes_ms,
	get_snapshot_show_tutorial_ms,
	get_snapshot_solver_ms,
	get_snapshot_total_ms,
	get_survey_responses,
	type Chain,
	type Session,
	type SuccessfulExecution
} from "./firebase";
import type { NaturalProgramSketch } from "natural-programs/types";
import type { FailedExecution } from "./firebase";
import type { ChainNode } from "./firebase";
import { get_chain_nodes } from "./firebase";
import { get_natural_strings } from "natural-programs/library/library";
import type { SessionType } from "./session_snapshot";
import type { Event } from "./events";

// Specific to minecraft domain:
import { parse_csvs } from "$lib/world_csv";
import type { CraftableItem } from "minecraft/items";
import { get_items_in_crafting_subtree } from "minecraft/items";
import { get_goal_seed } from "$lib/goal";

enum ExecutionCategory {
	SuccessfulDiscovery = "SuccessfulDiscovery", // successful post-condition, not successful before
	SuccessfulReuse = "SucessfulReuse", // successful post-condition, successful before
	FailedDiscovery = "FailedDiscovery", // failed post-condition, not successful before
	FailedReuse = "FailedReuse", // failed post-condition, successful before
};

type ClassifiedExecution =
	SuccessfulExecution & { category: ExecutionCategory }
	| FailedExecution & { category: ExecutionCategory };

type ChainNodeData = {
	user_id: number, // user index in the chain
	session_id: string,
	session: Session,
	procedure_sketch_submitted_n: number,
	procedure_sketch_submitted_successful_n: number,
	procedure_sketch_submitted_failed_n: number,
	signature_submitted_n: number,
	signature_submitted_successful_n: number,
	signature_submitted_failed_n: number,
	primitive_submitted_n: number,
	primitive_submitted_successful_n: number,
	primitive_submitted_failed_n: number,
	total_time_in_minutes: number,
	solver_time_in_minutes: number,
	sat_items_n: number,
	submissions: NaturalProgramSketch[],
	successful_executions: SuccessfulExecution[],
	failed_executions: FailedExecution[],
	sorted_classified_executions: ClassifiedExecution[],
	survey_response: SurveyResponse|null,
	mode: SessionType,
	node_id: string,
	original_user_id: string, // user_id in the chain
	show_tutorial_in_minutes: number,
	show_recipes_in_minutes: number,
	show_library_in_minutes: number,
	successful_post_conditions: string[],
	first_try_goal_item_n: number, // number of goal items successfully crafted on the first attempt
	previous_unique_successful_concrete_programs_n: number, // without including executions in this node
	chain_seed: string,
	item_csv: string,
	mine_csv: string,
	node_seed: string,
	goal_post_conditions: string[],
	chain_id: string,
}

type ChainData = {
	chain_id: string,
	nodes: {session: Session, session_id: string, node_id: string}[],
	vega_specifications: TopLevelSpec[],
	nps: NaturalProgramSketch[][],
	histogram: [string, number][],
	survey_responses: (SurveyResponse|null)[],
	data: ChainNodeData[],
	user_id_sat_items_n: [string, number][],
	chain: Chain,
	mine_csv: string,
	item_csv: string,
	seed: string,
	classified_executions: ClassifiedExecution[],
};


// Helper function to sum a non-empty list of numbers
function sum(ns: number[]): number {
	let x = 0;
	for (const n of ns)
		x = x + n;
	return x;
}


/**
 * Sort executions by session id and in increasing sessino time.
 * (So first executions from earlier sessions, and among executions from 
 * the same session, first those that were executed earlier.
 */
function get_sorted_executions(
		executions: (SuccessfulExecution | FailedExecution)[]
		): (SuccessfulExecution | FailedExecution)[] {
	const sorted_executions = [...executions];
	// Sort executions by increasing time
	sorted_executions.sort(function(a, b) {
		// Get sessions (firebase assigns lexicographically increasing IDs)
		const a_session_id = a.session_id;
		const b_session_id = b.session_id;
		if (a_session_id > b_session_id)
			return 1; // sort a after b
		else if (b_session_id > a_session_id)
			return -1; // sort b after a

		// Get event session milliseconds
		const a_time = a.session_milliseconds;
		const b_time = b.session_milliseconds;
		if (a_time > b_time)
			return 1; // sort a after b
		else if (b_time > a_time)
			return -1; // sort b after a
		else
			return 0;
	});
	return sorted_executions;
}


/**
 * Classify the given executions. It is assumed all executions are from the
 * same chain. Only the executions with post-conditions are classified,
 * the rest are ignored.
 * Returned list is sorted according to `get_sorted_executions`.
 **/
function get_classified_executions(
		executions: (SuccessfulExecution | FailedExecution)[]
		): ClassifiedExecution[] {
	// Sort executions by increasing time
	const sorted_executions = get_sorted_executions(executions);

	// Track successful and failed post-conditions
	const successful_post_conditions = new Set<string>();
	const failed_post_conditions = new Set<string>();

	// Label each execution about a program with a post-condition
	const classified_executions: ClassifiedExecution[] = [];
	for (const execution of sorted_executions) {
		// Extract execution program
		const np = JSON.parse(execution.np) as NaturalProgramSketch;

		// If execution has no post-condition, ignore.
		if (typeof np === 'string')
			continue;
		const signature = ('steps' in np) ? np.signature : np;
		if (signature.post_condition === undefined)
			continue;

		// Extract post-condition
		const post_condition = signature.post_condition;

		// Compute execution category
		const successful_before = successful_post_conditions.has(post_condition);
		const successful_now = !("error" in execution);
		const category =
			(successful_now && successful_before) ? ExecutionCategory.SuccessfulReuse :
			(successful_now && !successful_before) ? ExecutionCategory.SuccessfulDiscovery :
			(!successful_now && successful_before) ? ExecutionCategory.FailedReuse :
			/* (!successful_now && !successful_before) */ ExecutionCategory.FailedDiscovery;

		// Classify execution
		classified_executions.push({...execution, category: category });

		// Update successful or failed post-conditions
		if (successful_now)
			successful_post_conditions.add(post_condition);
		else
			failed_post_conditions.add(post_condition);
	}
	return classified_executions;
}


/**
 * Return the number of goal items that were successfully
 * executed on the first attempt in the given time.
 */
function get_first_try_goal_items(
		executions: (SuccessfulExecution | FailedExecution)[],
		goal_post_conditions: Set<string>,
		): Set<string> {
	// Sort executions by increasing time
	const sorted_executions = get_sorted_executions(executions);

	// Track successful, failed and first-try goal post-conditions
	const successful_post_conditions = new Set<string>();
	const failed_post_conditions = new Set<string>();
	const first_try_goal_post_conditions = new Set<string>();

	// Identify first-try success post-conditions
	for (const execution of sorted_executions) {
		// Extract execution program
		const np = JSON.parse(execution.np) as NaturalProgramSketch;

		// If execution has no post-condition, ignore.
		if (typeof np === 'string')
			continue;
		const signature = ('steps' in np) ? np.signature : np;
		if (signature.post_condition === undefined)
			continue;

		// Extract post-condition
		const post_condition = signature.post_condition;

		// Check if first time executing this post-condition
		const first_time = !(successful_post_conditions.has(post_condition) || failed_post_conditions.has(post_condition));

		// Check if execution was successful
		const successful_now = !("error" in execution);

		if (first_time && successful_now && goal_post_conditions.has(post_condition))
			first_try_goal_post_conditions.add(post_condition);

		// Update successful or failed post-conditions
		if (successful_now)
			successful_post_conditions.add(post_condition);
		else
			failed_post_conditions.add(post_condition);
	}
	return first_try_goal_post_conditions;
}


/**
 * Return the number of goal items that were successfully
 * executed in the given time.
 */
function get_crafted_goal_items(
		executions: (SuccessfulExecution | FailedExecution)[],
		goal_post_conditions: Set<string>,
		): Set<string> {
	// Sort executions by increasing time
	const sorted_executions = get_sorted_executions(executions);

	// Track successful, failed and first-try goal post-conditions
	const successful_post_conditions = new Set<string>();
	const failed_post_conditions = new Set<string>();
	const successful_goal_post_conditions = new Set<string>();

	// Identify first-try success post-conditions
	for (const execution of sorted_executions) {
		// Extract execution program
		const np = JSON.parse(execution.np) as NaturalProgramSketch;

		// If execution has no post-condition, ignore.
		if (typeof np === 'string')
			continue;
		const signature = ('steps' in np) ? np.signature : np;
		if (signature.post_condition === undefined)
			continue;

		// Extract post-condition
		const post_condition = signature.post_condition;

		// Check if execution was successful
		const successful_now = !("error" in execution);

		if (successful_now && goal_post_conditions.has(post_condition))
			successful_goal_post_conditions.add(post_condition);

		// Update successful or failed post-conditions
		if (successful_now)
			successful_post_conditions.add(post_condition);
		else
			failed_post_conditions.add(post_condition);
	}
	return successful_goal_post_conditions;
}


/**
 * Get the unique successful programs discovered by the start of each
 * node's session.
 *
 * The input are the successful executions of each node.
 */
function get_unique_concrete_programs_at_session_start(
		successful_executions: SuccessfulExecution[][],
		): (Set<string>)[] {
	// Track the cumulative unique discovered concrete programs
	const discovered_concrete_nps = new Set<string>();

	// Track the executions discovered 
	const starting_concrete_programs = [];

	for (const node_executions of successful_executions) {
		// Record discovering executions at the start of this node
		starting_concrete_programs.push(new Set<string>([...discovered_concrete_nps]));

		for (const execution of node_executions) {
			discovered_concrete_nps.add(execution.concrete_np);
		}
	}
	return starting_concrete_programs;
}

async function process_data(
		chain: Chain,
		chain_id: string,
		chain_nodes: ChainNode[],
		successful_executions: SuccessfulExecution[],
		failed_executions: FailedExecution[],
		survey_responses: SurveyResponse[],
		mode: SessionType,
		item_csv: string,
		mine_csv: string,
		): Promise<ChainNodeData[]> {
	// Sort nodes from old to new (Firebase assigns IDs incrementally, but this
	// can be done using the previous_node_id property)
	const nodes = chain_nodes.filter(n=>n.chain_id===chain_id);
	nodes.sort(function(n1, n2) {
		if (n1.id < n2.id) return -1;
		return 1;
	});
	function filter_procedure_sketches(l: (SuccessfulExecution|FailedExecution)[]) {
		return l
			.map(e=>JSON.parse(e.np))
			.filter(np=>(typeof np !== "string" && "steps" in np));
	}
	function filter_signatures(l: (SuccessfulExecution|FailedExecution)[]) {
		return l
			.map(e=>JSON.parse(e.np))
			.filter(np=>(typeof np !== "string" && !("steps" in np)));
	}
	function filter_primitives(l: (SuccessfulExecution|FailedExecution)[]) {
		return l
			.map(e=>JSON.parse(e.np))
			.filter(np=>(typeof np === "string"));
	}

	// Identify successful executions at each node
	const node_successful_executions = [];
	for (const node of nodes) {
		const local_successful_executions = successful_executions
			.filter(e=>e.session_id===node.session_id);
		node_successful_executions.push(local_successful_executions);
	}

	// Filter successful executions to discovering executions
	const unique_concrete_programs_at_session_start = get_unique_concrete_programs_at_session_start(
		node_successful_executions
	);

	// Process each node
	return Promise.all(nodes.map(async function (n, i) {
		// Statistics for procedure_sketches
		const local_successful_executions = successful_executions
			.filter(e=>e.session_id===n.session_id);
		const local_failed_executions = failed_executions
			.filter(e=>e.session_id===n.session_id);
		const procedure_sketches = filter_procedure_sketches([
			...local_successful_executions,
			...local_failed_executions
		]);

		// Statistics for signatures
		const signatures = filter_signatures([
			...local_successful_executions,
			...local_failed_executions
		]);

		// Statistics for primitives
		const primitives = filter_primitives([
			...local_successful_executions,
			...local_failed_executions
		]);

		// Get session timer
		const session = await get_session(n.session_id);
		const total_time_in_minutes = await get_snapshot_total_ms(
				session.active_snapshot_id
			)/1000/60;
		const solver_time_in_minutes = await get_snapshot_solver_ms(
				session.active_snapshot_id
			)/1000/60;
		const show_tutorial_in_minutes = await get_snapshot_show_tutorial_ms(
				session.active_snapshot_id
			)/1000/60;
		const show_recipes_in_minutes = await get_snapshot_show_recipes_ms(
				session.active_snapshot_id
			)/1000/60;
		const show_library_in_minutes = await get_snapshot_show_library_ms(
				session.active_snapshot_id
			)/1000/60;

		const sat_items_n = (await get_snapshot_sat_items(session.active_snapshot_id)).length;

		// Get all submissions
		const nps = ([...local_successful_executions, ...local_failed_executions])
			.filter(e=>e.session_id===n.session_id)
			.map(e=>JSON.parse(e.np));

		const successful_post_conditions = [
			 ...filter_procedure_sketches(local_successful_executions)
					.filter(np=>np.signature.post_condition !== undefined)
					.map(np=>np.signature.post_condition)
					,
			 ...filter_signatures(local_successful_executions)
					.filter(np=>np.post_condition !== undefined)
					.map(np=>np.post_condition)
			].filter(post_condition=>typeof post_condition === "string");
		const user_responses = survey_responses.filter(r=> {
			// Legacy support: survey responses were indexed by user_id
			if ("user_id" in r)
				return (r as SurveyResponse&{user_id:string}).user_id === session.user_id;
			return r.session_id===n.session_id;
		});
		const survey_response = (user_responses.length >= 1) ? user_responses[0] : null;
		const node_seed = get_goal_seed(chain.chain_seed, i);
		const local_executions = [
			...local_successful_executions,
			...local_failed_executions,
		];
		// This should technically be a call to item_name -> item object -> get_constraint_as_string
		const goal_post_conditions = 
			(n.goal !== undefined) ?
			n.goal
			.map(item_name=>JSON.stringify([item_name]))
			: [];
		const first_try_goal_items = get_first_try_goal_items(
			local_executions,
			new Set<string>(goal_post_conditions)
		);
		const crafted_goal_items = get_crafted_goal_items(
			local_executions,
			new Set<string>(goal_post_conditions)
		);

		const first_try_goal_item_n = first_try_goal_items.size;
		const crafted_goal_item_n = crafted_goal_items.size;

		const starting_concrete_programs = unique_concrete_programs_at_session_start[i];
		const sorted_classified_executions = get_classified_executions(local_executions);
		return {
			user_id: i,
			session_id: n.session_id,
			procedure_sketch_submitted_n: procedure_sketches.length,
			procedure_sketch_submitted_successful_n: filter_procedure_sketches(local_successful_executions).length,
			procedure_sketch_submitted_failed_n: filter_procedure_sketches(local_failed_executions).length,

			signature_submitted_n: signatures.length,
			signature_submitted_successful_n: filter_signatures(local_successful_executions).length,
			signature_submitted_failed_n: filter_signatures(local_failed_executions).length,
			primitive_submitted_n: primitives.length,
			primitive_submitted_successful_n: filter_primitives(local_successful_executions).length,
			primitive_submitted_failed_n: filter_primitives(local_failed_executions).length,
			total_time_in_minutes: total_time_in_minutes,
			solver_time_in_minutes: solver_time_in_minutes,
			successful_executions: local_successful_executions,
			failed_executions: local_failed_executions,
			sat_items_n: sat_items_n,
			submissions: nps,
			session: session,
			survey_response: survey_response,
			mode: mode,
			node_id: n.session_id,
			original_user_id: session.user_id,
			show_tutorial_in_minutes: show_tutorial_in_minutes,
			show_recipes_in_minutes: show_recipes_in_minutes,
			show_library_in_minutes: show_library_in_minutes,
			successful_post_conditions: successful_post_conditions,
			chain_seed: chain.chain_seed,
			item_csv: item_csv,
			mine_csv: mine_csv,
			node_seed: node_seed,
			first_try_goal_item_n: first_try_goal_item_n,
			crafted_goal_item_n: crafted_goal_item_n,
			previous_unique_successful_concrete_programs_n: starting_concrete_programs.size,
			sorted_classified_executions: sorted_classified_executions,
			goal_post_conditions: goal_post_conditions,
			chain_id: chain_id,
		}
	}));
}

async function get_chain_data(chain_ids: string[]): Promise<ChainData[]> {
	const chain_nodes = await get_chain_nodes();
	const survey_responses = await get_survey_responses();
	const chain_data = await Promise.all(chain_ids.map(async function (chain_id) {
		const chain_successful_executions = await get_chain_successful_executions(chain_id);
		const chain_failed_executions = await get_chain_failed_executions(chain_id);
		console.log(chain_successful_executions);
		const chain = await get_chain(chain_id);
		const initial_snapshot = await get_snapshot(chain.initial_snapshot_id, "");
		const mode = initial_snapshot.session_type;
		const data = await process_data(
			chain,
			chain_id,
			chain_nodes,
			chain_successful_executions,
			chain_failed_executions,
			survey_responses,
			mode,
			initial_snapshot.item_csv,
			initial_snapshot.mine_csv,
		);

		// Build metric line charts
		const metrics = [
			"sat_items_n",
			"total_time_in_minutes",
			"solver_time_in_minutes",
			"show_tutorial_in_minutes",
			"show_recipes_in_minutes",
			"show_library_in_minutes",
		];
		const metric_plots: TopLevelSpec[] = metrics.map(function (metric) {
			return {
				data: {values: data},
				mark: "line",
				layer: [
					{
						mark: "line",
						encoding: {
							x: {field: "user_id", type: "nominal"},
							y: {field: metric, type: "quantitative"},
							color: {field: "mode"}
						}
					},
					{
						mark: {type: "point", opacity: 0.3},
						encoding: {
							x: {field: "user_id", type: "nominal"},
							y: {field: metric, type: "quantitative"},
							color: {field: "mode"}
						}
					},
				]
			};
		});

		// Build type distribution chart
		const type_distribution_data = data.flatMap(function (n) {
			return [
				{...n, submissions: n.signature_submitted_n, source: "signature"},
				{...n, submissions: n.primitive_submitted_n, source: "primitive"},
				{...n, submissions: n.procedure_sketch_submitted_n, source: "procedure_sketch"},
			];
		});
		const type_distribution_chart: TopLevelSpec = {
			data: {values: type_distribution_data},
			mark: "bar",
			encoding: {
				x: {field: "user_id", type: "nominal"},
				y: {aggregate: "sum", field: "submissions", type: "quantitative"},
				color: {field: "source"}
			}
		}

		// Build success rate chart
		const success_rate_data = data.flatMap(function (n) {
			const success_n = n.signature_submitted_successful_n+n.primitive_submitted_successful_n+n.procedure_sketch_submitted_successful_n;
			const failed_n = n.signature_submitted_failed_n+n.primitive_submitted_failed_n+n.procedure_sketch_submitted_failed_n;
			return [
				{...n, submissions: success_n, source: "success"},
				{...n, submissions: failed_n, source: "fail"},
			];
		});
		const success_rate_chart: TopLevelSpec = {
			data: {values: success_rate_data},
			mark: "bar",
			encoding: {
				x: {field: "user_id", type: "nominal"},
				y: {aggregate: "sum", field: "submissions", type: "quantitative"},
				color: {field: "source"}
			}
		}

		// Build success distribution chart
		const success_distribution_data = data.flatMap(function (n) {
			return [
				{...n, successful_submissions: n.signature_submitted_successful_n, source: "signature"},
				{...n, successful_submissions: n.primitive_submitted_successful_n, source: "primitive"},
				{...n, successful_submissions: n.procedure_sketch_submitted_successful_n, source: "procedure_sketch"},
			];
		});
		const success_distribution_chart: TopLevelSpec = {
			data: {values: success_distribution_data},
			mark: "bar",
			encoding: {
				x: {field: "user_id", type: "nominal"},
				y: {aggregate: "sum", field: "successful_submissions", type: "quantitative"},
				color: {field: "source"}
			}
		}

		// Build failure distribution chart
		const failed_distribution_data = data.flatMap(function (n) {
			return [
				{...n, failed_submissions: n.signature_submitted_failed_n, source: "signature"},
				{...n, failed_submissions: n.primitive_submitted_failed_n, source: "primitive"},
				{...n, failed_submissions: n.procedure_sketch_submitted_failed_n, source: "procedure_sketch"},
			];
		});
		const failed_distribution_chart: TopLevelSpec = {
			data: {values: failed_distribution_data},
			mark: "bar",
			encoding: {
				x: {field: "user_id", type: "nominal"},
				y: {aggregate: "sum", field: "failed_submissions", type: "quantitative"},
				color: {field: "source"}
			}
		}

		// Build number of successful post-conditions chart
		const initial_crafting_world = parse_csvs(
			initial_snapshot.item_csv,
			initial_snapshot.mine_csv,
			initial_snapshot.seed,
			"",
		);
		const item_names = [...initial_crafting_world.name_to_item.keys()];
		const name_to_items_index = new Map<string, number>();
		for (let i = 0; i < item_names.length; i++)
			name_to_items_index.set(item_names[i], i);
		const successful_post_conditions_heat_map = data.flatMap(function(node) {
			return node.successful_post_conditions.map(function(post_condition) {
				return {
					successful_post_condition: post_condition,
					user_id: node.user_id,
					mode: node.mode,
					i: name_to_items_index.get(JSON.parse(post_condition)[0]),
				}
			})
		});
		const successful_post_conditions_chart: TopLevelSpec = {
			data: {values: successful_post_conditions_heat_map},
			mark: "rect",
			encoding: {
				column: {field: "mode"},
				x: {field: "user_id", type: "ordinal"},
				y: {
					field: "successful_post_condition",
					type: "nominal",
					sort: {field: "i", order: "descending"},
				},
				color: {aggregate: "count"},
			}
		}

		// Get submitted programs
		const nps = data.map(n=>n.submissions);

		// Build word histogram
		const histogram = new Map<string, number>();
		const words = get_natural_strings(nps.flat()).flatMap(s=>s.split(" "));
		for (const word of words) {
			const old_count = histogram.get(word)
			if (old_count === undefined)
				histogram.set(word, 1);
			else
				histogram.set(word, old_count+1);
		}
		const array_histogram = [...histogram.entries()];
		array_histogram.sort(function (a, b) {
			return b[1]-a[1];
		});

		// Add size properties
		const plots = [
			successful_post_conditions_chart,
			type_distribution_chart,
			success_rate_chart,
			success_distribution_chart,
			failed_distribution_chart,
			...metric_plots,
		];

		// Identify the chain executions
		const chain_executions = [...chain_successful_executions, ...chain_failed_executions];

		// Build the list of user IDs and sat items number
		const user_id_sat_items_n: [string, number][] = data
			.map(n=>[n.user_id+"", n.sat_items_n]);
		return {
			chain_id: chain_id,
			vega_specifications: plots,
			nps: nps,
			histogram: array_histogram,
			nodes: data.map(n=>{return {session: n.session, session_id: n.session_id, node_id: n.node_id};}),
			survey_responses: data.map(n=>n.survey_response),
			user_id_sat_items_n: user_id_sat_items_n,
			data: data,
			chain: chain,
			item_csv: initial_snapshot.item_csv,
			mine_csv: initial_snapshot.mine_csv,
			seed: initial_snapshot.seed,
			classified_executions: get_classified_executions(chain_executions),
		};
	}));
	return chain_data;
}

/**
 * Return the list of craftable items crafted by the given node.
 */
function get_discovered_recipes(node: ChainNodeData): CraftableItem[] {
	const world = parse_csvs(
		node.item_csv,
		node.mine_csv,
		node.node_seed,
		"",
	);
	const discovered_recipes = [];
	const post_condition_names = node
		.successful_post_conditions
		.map(post_condition=>JSON.parse(post_condition)[0]);

	// Create a set of created item names
	// Initially, all items that appear in a
	// successful post condition are added.
	const crafted_item_names = new Set<string>(post_condition_names);

	// Recursively add all prerequisite craftable items in the
	// crafting tree.
	for (const name of [...crafted_item_names]) {
		const item = world.name_to_item.get(name);
		if (item === undefined) {
			console.error({
				error: "Cannot get prerequisite items for " + name + "!",
				node: node,
			});
			continue;
		}
		const required_items = get_items_in_crafting_subtree(item, world);
		for (const item of required_items)
			crafted_item_names.add(item.name);
	}

	// Check all items in the world, recording those that
	// were crafted
	for (const item of world.name_to_item.values()) {
		if ("recipe" in item && crafted_item_names.has(item.name))
			discovered_recipes.push(item);
	}
	return discovered_recipes;
}

/**
 * Add the number of cumulative discovered recipes to each node in the
 * given data.
 */
function get_cumulative_discovered_recipes(
		chain_data: ChainData,
		): (ChainNodeData & {cumulative_discovered_recipes_n: number})[] {
	const sorted_chain_data = [...chain_data.data];
	sorted_chain_data.sort((n1, n2)=>n1.user_id-n2.user_id);

	const discovered_recipes = new Set<string>();
	const data = [];

	// For each node in the chain
	for (const node of sorted_chain_data) {
		// Add all discovered recipes to the set
		for (const recipe of get_discovered_recipes(node))
			discovered_recipes.add(JSON.stringify(recipe));

		// Record the size of the set
		const node_data = {
			...node,
			cumulative_discovered_recipes_n: discovered_recipes.size,
		};
		data.push(node_data);
	}

	return data;
}

function get_aggregate_plots(chain_data: ChainData[]): TopLevelSpec[] {
	if (chain_data.length === 0) return [];

	// Build item order
	const initial_crafting_world = parse_csvs(
		chain_data[0].item_csv,
		chain_data[0].mine_csv,
		chain_data[0].seed,
		"",
	);
	const item_names = [...initial_crafting_world.name_to_item.keys()];
	const name_to_items_index = new Map<string, number>();
	for (let i = 0; i < item_names.length; i++)
		name_to_items_index.set(item_names[i], i);

	// Add discovered fraction of recipe book to chain data
	// and flatten to chain nodes
	const nodes = chain_data
		.map(data=>get_cumulative_discovered_recipes(data))
		.flat();

	// Build plots
	const plots = [];

	// Add total submission number to data
	const submissions_data = nodes.flatMap(function (n) {
		return [
			{...n, total_submissions: n.submissions.length}
		];
	});

	// Build numeric charts
	const numeric_properties = [
		"sat_items_n",
		"total_submissions",
		"total_time_in_minutes",
		"solver_time_in_minutes",
		"show_recipes_in_minutes",
		"show_tutorial_in_minutes",
		"show_library_in_minutes",
		"survey_response.age",
		"survey_response.frustration",
		"survey_response.engagement",
		"survey_response.mental_demand",
		"survey_response.ease_of_use",
		"survey_response.hurry",
		"survey_response.self_success",
		"survey_response.effort",
		"cumulative_discovered_recipes_n",
	];
	for (const property of numeric_properties) {
		const field = property;
		const user_id_chart: TopLevelSpec = {
			data: {values: submissions_data},
			mark: "point",
			layer: [
				{
					mark: "line",
					encoding: {
						x: {field: "user_id", type: "nominal"},
						y: {aggregate: "median", field: field, type: "quantitative"},
						color: {field: "mode"}
					}
				},
				{
					mark: {type: "point", opacity: 0.2},
					encoding: {
						x: {field: "user_id", type: "nominal"},
						y: {field: field, type: "quantitative"},
						color: {field: "mode"}
					}
				},
			]
		}
		const rolling_field = `Rolling mean of last 3 of ${property}`;
		const rolling_user_id_chart: TopLevelSpec = {
			data: {values: submissions_data},
			mark: "point",
			transform: [{
				window: [
					{
						field: property,
						op: "mean",
						as: rolling_field,
					},
				],
				groupby: ["mode"],
				sort: [{field: "user_id", order: "ascending"}],
				frame: [-3, 0],
			}],
			layer: [
				{
					mark: {type: "line"},
					encoding: {
						x: {field: "user_id", type: "nominal"},
						y: {field: rolling_field, type: "quantitative"},
						color: {field: "mode"}
					}
				},
				{
					mark: {type: "point", opacity: 0.2},
					encoding: {
						x: {field: "user_id", type: "nominal"},
						y: {field: field, type: "quantitative"},
						color: {field: "mode"}
					}
				},
			]
		}
		const error_bar_chart: TopLevelSpec = {
			data: {values: submissions_data},
			mark: {
				type: "boxplot",
			},
			encoding: {
				x: {field: "mode", type: "nominal"},
				y: {field: field, type: "quantitative"},
				color: {field: "mode"}
			}
		}
		plots.push(user_id_chart, rolling_user_id_chart, error_bar_chart);
	}

	// Build time classification chart
	{
		const tracked_times = nodes.flatMap(function(n) {
			const tracked_times: [string, number][] = [
				["Solver", n.solver_time_in_minutes],
				["Library browsing", n.show_library_in_minutes],
				["Tutorial browsing", n.show_tutorial_in_minutes],
				["Recipes browsing", n.show_recipes_in_minutes],
			];
			const other_time = n.total_time_in_minutes - sum(tracked_times.map(t=>t[1]));
			tracked_times.push(["Other", other_time]);

			const node_data = [];
			for (const tracked_time of tracked_times) {
				const label = tracked_time[0];
				const time = tracked_time[1];
				node_data.push({...n, source: label, minutes: time});
			}
			return node_data;
		});
		const plot: TopLevelSpec = {
			data: { values: tracked_times },
			mark: "bar",
			encoding: {
				column: {field: "mode"},
				x: {field: "user_id", type: "nominal"},
				y: {aggregate: "average", field: "minutes"},
				color: {field: "source"},
			}
		};
		plots.push(plot);
	}

	{
		// Build successful post-conditions heat map
		const successful_post_conditions_heat_map = nodes.flatMap(function(node) {
			return node.successful_post_conditions.map(function(post_condition) {
				return {
					successful_post_condition: post_condition,
					user_id: node.user_id,
					i: name_to_items_index.get(JSON.parse(post_condition)[0]),
					mode: node.mode,
				}
			})
		});
		const successful_post_conditions_heatmap: TopLevelSpec = {
			data: {values: successful_post_conditions_heat_map},
			mark: "rect",
			encoding: {
				column: {field: "mode"},
				x: {field: "user_id", type: "ordinal"},
				y: {
					field: "successful_post_condition",
					type: "nominal",
					sort: {field: "i", order: "descending"},
				},
				color: {aggregate: "count"},
			}
		};
		plots.push(successful_post_conditions_heatmap);
	}

	{
		// Build successful post-conditions heat map
		const successful_post_conditions_n = nodes.map(function(node) {
			const post_condition_set = new Set<string>(
					node.successful_post_conditions
			);
			return {
				unique_successful_post_condition_n: post_condition_set.size,
				user_id: node.user_id,
				mode: node.mode,
			}
		});
		const successful_post_conditions_chart: TopLevelSpec = {
			data: {values: successful_post_conditions_n},
			layer: [
				{
					mark: "line",
					encoding: {
						x: {field: "user_id", type: "nominal"},
						y: {
							aggregate: "mean",
							field: "unique_successful_post_condition_n",
							type: "quantitative"
						},
						color: {field: "mode"}
					}
				},
				{
					mark: {type: "point", opacity: 0.2},
					encoding: {
						x: {field: "user_id", type: "nominal"},
						y: {
							field: "unique_successful_post_condition_n",
							type: "quantitative"
						},
						color: {field: "mode"}
					}
				},
			]
		};
		plots.push(successful_post_conditions_chart);
	}

	{
		// Build execution classification plots
		const data = chain_data.flatMap(function(chain) {
			// Extract classified executions annotated with
			// session type
			if (chain.data.length === 0)
				return [];
			const mode = chain.data[0].mode;
			const classified_executions = chain.classified_executions;
			const annotated_executions = classified_executions
				.map(function(e) { return {...e, chain_id: chain.chain_id, mode: mode}});
			return annotated_executions;
		});
		const plot: TopLevelSpec = {
			data: {values: data},
			mark: {type: "bar", tooltip: true},
			encoding: {
				column: {field: "category"},
				x: {field: "mode", type: "nominal"},
				y: {aggregate: "count", type: "quantitative"},
				color: {field: "mode"}
			}
		};
		plots.push(plot);
	}

	{
		// Build submissions vs sat items
		const plot: TopLevelSpec = {
			data: {values: submissions_data},
			layer: [
				{
					mark: {type: "point"},
					encoding: {
						x: {field: "total_submissions", type: "quantitative"},
						y: {field: "sat_items_n", type: "quantitative"},
						color: {field: "mode"},
					}
				}
			]
		};
		plots.push(plot);
	}

	{ 
		// Build first-try goal item plot
		const plot: TopLevelSpec = {
			data: {values: submissions_data},
			transform: [
				{filter: "datum.sat_items_n > 0"},
				{calculate: "datum.first_try_goal_item_n/datum.crafted_goal_item_n", as: "success_first_try"}
			],
			layer: [
				{
					mark: {type: "point"},
					encoding: {
						x: {field: "mode", type: "nominal"},
						y: {field: "success_first_try", type: "quantitative", aggregate: "mean"},
						color: {field: "mode"}
					}
				}
			],
		};
		plots.push(plot);
	}

	{ 
		// Build previous unique successful items plot
		const plot: TopLevelSpec = {
			data: {values: submissions_data},
			layer: [
				{
					mark: {type: "point"},
					encoding: {
						x: {field: "previous_unique_successful_concrete_programs_n", type: "quantitative"},
						y: {field: "sat_items_n", type: "quantitative"},
						color: {field: "mode"},
					}
				}
			]
		};
		plots.push(plot);
	}

	{
		// Build source solver chart
		const successful_executions = nodes.flatMap(function(n) {
			return n.successful_executions.map(function(se) {
				const source_solver = se.source_solver;
				const user_id = n.user_id;
				return {
					source_solver: source_solver,
					user_id: user_id,
					session_milliseconds: se.session_milliseconds,
					concrete_np: se.concrete_np,
					chain_id: n.chain_id,
				};
			});
		});

		// Identify unique executions across the entire chain
		successful_executions.sort(function(a, b) {
			if (a.user_id !== b.user_id)
				return a.user_id - b.user_id;
			return a.session_milliseconds - b.session_milliseconds;
		});
		const unique_successful_executions = [];
		const keys = new Set<string>();
		for (const e of successful_executions) {
			const key = JSON.stringify([e.concrete_np, e.chain_id]);
			if (!(keys.has(key))) {
				keys.add(key);
				unique_successful_executions.push(e);
			}
		}

		const plot: TopLevelSpec = {
			data: { values: unique_successful_executions },
			mark: "bar",
			encoding: {
				x: {field: "user_id", type: "nominal"},
				y: {aggregate: "count", field: "source_solver"},
				color: {field: "source_solver"},
			}
		};
		plots.push(plot);
	}

	return plots;
}

/**
 * Return a plot summarizing the events in chronological order.
 */
function get_event_plot(events: Event[]): TopLevelSpec {
	// Fix the event sequence
	const new_events = [];
	for (const event of events) {
		if (event.kind === "failed_execution") {
			new_events.push({
				kind: "failed_execution_start",
				session_milliseconds: event.session_milliseconds-event.milliseconds,
			}, {
				kind: "failed_execution_end",
				session_milliseconds: event.session_milliseconds,
			});
		} else if(event.kind === "successful_execution") {
			new_events.push({
				kind: "successful_execution_start",
				session_milliseconds: event.session_milliseconds-event.milliseconds,
			}, {
				kind: "successful_execution_end",
				session_milliseconds: event.session_milliseconds,
			});
		} else if (event.kind === "blockly_event") {
			// Blockly stores icon changing event, which we do not care about
			// for the purposes of this analysis, as it is a user-initiated
			// action on the editor.
			const blockly_event = event.blockly_event_json as any;
			if (!("name" in blockly_event) || blockly_event.name !== "PLAY_BUTTON")
				new_events.push(event);
		} else
			new_events.push(event);
	}

	// Sort events chronologically
	new_events.sort(function (a, b) {
		return a.session_milliseconds-b.session_milliseconds;
	});

	// Build time segments
	let previous_recipes_open = null;
	let previous_tutorial_open = null;
	let previous_library_open = null;
	let previous_blockly = null;
	let previous_execution_start = null;
	let start_blockly = null;
	const time_segments = [];
	for (const event of new_events) {

		// Update previous open events
		if (event.kind === "recipes_open")
			previous_recipes_open = event;
		else if (event.kind === "tutorial_open")
			previous_tutorial_open = event;
		else if (event.kind === "library_open")
			previous_library_open = event;
		else if (event.kind === "blockly_event")
			previous_blockly = event;
		else if (event.kind === "successful_execution_start")
			previous_execution_start = event;
		else if (event.kind === "failed_execution_start")
			previous_execution_start = event;

		const previous_relevant_event =
			(event.kind === "recipes_close") ? previous_recipes_open :
			(event.kind === "tutorial_close") ? previous_tutorial_open :
			(event.kind === "library_close") ? previous_library_open :
			(event.kind === "successful_execution_end") ? previous_execution_start :
			(event.kind === "failed_execution_end") ? previous_execution_start :
			(event.kind !== "blockly_event") ? start_blockly : null;

		if (previous_relevant_event !== null) {
				previous_relevant_event.session_milliseconds;

			const event_kind =
				(event.kind === "recipes_close") ? "Recipe" :
				(event.kind === "tutorial_close") ? "Tutorial" :
				(event.kind === "library_close") ? "Library" :
				(event.kind === "failed_execution_end") ? "Failed execution" :
				(event.kind === "successful_execution_end") ? "Successful execution" :
				"IDE interaction";

			const segment = {
				kind: event_kind,
				start_minutes: previous_relevant_event.session_milliseconds/1000/60,
				end_minutes: event.session_milliseconds/1000/60,
			};
			time_segments.push(segment);
		}

		// If a blockly group just ended
		if (event.kind !== "blockly_event" && previous_blockly !== null && start_blockly !== null) {
			start_blockly = null;
			previous_blockly = null;
		}

		// If we just started a blockly group
		if (event.kind === "blockly_event" && start_blockly === null) {
			start_blockly = event;
		}
	}
	return {
		data: {
			values: time_segments,
		},
		mark: "bar",
		encoding: {
			y: {field: "kind", type: "ordinal"},
			x: {field: "start_minutes", type: "quantitative"},
			x2: {field: "end_minutes"},
			color: {field: "kind"}
		}
	}
}

export {
	get_chain_data,
	get_aggregate_plots,
	get_event_plot
};

export type {
	ChainData,
	ChainNodeData,
};
