/**
 * Block definitions.
 */
import type { SolverResult } from "natural-programs/types";
import type { SketchSolver } from "natural-programs/types";
import type { NaturalProgramSketch } from "natural-programs/types";
import type { NaturalProcedureSketch } from "natural-programs/types";
import type { NaturalSignature } from "natural-programs/types";
import { get_unrolled_implementation } from "natural-programs/unrolling";
import Blockly from 'blockly';

type Outcome<ProgramState> = SolverResult<ProgramState>&{
	sketch: NaturalProgramSketch
};

function get_play_icon_url(base: string): string { return base+"/img/remixicon/play-line.svg"; }
function get_play_disabled_icon_url(base: string): string { return base+"/img/remixicon/time-line.svg"; }
function get_add_icon_url(base: string): string { return base+"/img/remixicon/add-line.svg"; }
const icon_size = 20;
const play_button_tooltip = "Execute block";
const play_button_wait_tooltip = "Wait for execution to complete";
const function_definition_default_text = "<desc>";
const signature_default_text = "<search>";

enum BlockType {
	FunctionDefinition = "function_definition",
	DropdownPrimitive = "dropdown_primitive",
	Primitive ="primitive",
	Signature = "signature",
	SimpleFunctionDefinition = "simple_function_definition",
	SimpleSignature = "simple_signature",
};

const NONE_DROPDOWN_OPTION = "NONE";
type DropdownInfo = {
	src: string,
	width: number,
	height: number,
	alt:string,
}|"none";
type DropdownOption = [DropdownInfo, string|typeof NONE_DROPDOWN_OPTION];

function get_signature_json(dropdown_options: DropdownOption[], base: string) {
	return {
		kind: "block",
		type: BlockType.Signature,
		message0: "description %1 task %2 run %3",
		args0: [
			{
				type: "field_input",
				name: "NAME",
				text: signature_default_text,
			},
			{
				type: "field_dropdown",
				name: "POST_CONDITION",
				options: dropdown_options,
			},
			{
				type: "field_image",
				src: get_play_icon_url(base),
				width: icon_size,
				height: icon_size,
				name: "PLAY_BUTTON",
			},
		],
		inputsInline: true,
		colour: 130,
		previousStatement: true,
		nextStatement: true,
		extensions: [
				"play_button_extension",
			],
	};
}

function get_simple_signature_json(base: string) { return {
	kind: "block",
	type: BlockType.SimpleSignature,
	message0: "name %1 run %2",
	args0: [
		{
			type: "field_input",
			name: "NAME",
			text: signature_default_text,
		},
		{
			type: "field_image",
			src: get_play_icon_url(base),
			width: icon_size,
			height: icon_size,
			name: "PLAY_BUTTON",
		},
	],
	inputsInline: true,
	colour: 130,
	previousStatement: true,
	nextStatement: true,
	extensions: [
			"play_button_extension",
		],
};}

function get_primitive_json(base: string) { return {
	kind: "block",
	type: BlockType.Primitive,
	message0: "%1 %2",
	args0: [
		{
			type: "field_label_serializable",
			name: "NAME",
		},
		{
			type: "field_image",
			src: get_play_icon_url(base),
			width: icon_size,
			height: icon_size,
			name: "PLAY_BUTTON",
		}
	],
	colour: 130,
	previousStatement: true,
	nextStatement: true,
	extensions: ["play_button_extension"],
};}

function get_dropdown_primitive_json(options: DropdownOption[], base: string) {
	return {
		kind: "block",
		type: BlockType.DropdownPrimitive,
		message0: "%1 %2 %3",
		args0: [
			{
				type: "field_label_serializable",
				name: "NAME",
			},
			{
				type: "field_dropdown",
				name: "ILLUSTRATION",
				options: options,
			},
			{
				type: "field_image",
				src: get_play_icon_url(base),
				width: icon_size,
				height: icon_size,
				name: "PLAY_BUTTON",
			}
		],
		colour: 130,
		previousStatement: true,
		nextStatement: true,
		extensions: ["play_button_extension"],
	}
}

function get_function_definition_json(options: DropdownOption[], base: string) {
	return {
		type: BlockType.FunctionDefinition,
		message0: "description %1 task %2 teach %3",
		args0: [
			{
				type: "field_input",
				name: "NAME",
				text: function_definition_default_text,
			},
			{
				type: "field_dropdown",
				name: "POST_CONDITION",
				options: options,
			},
			{
				type: "field_image",
				src: get_add_icon_url(base),
				width: icon_size,
				height: icon_size,
				name: "SAVE_BUTTON",
			},
		],
		message1: "steps %1",
		args1: [
			{
				type: "input_statement",
				name: "BODY",
			}
		],
		inputsInline: true,
		lastDummyAlign2: "RIGHT",
		colour: 130,
		extensions: ["save_button_extension"],
	}
}

function get_simple_function_definition_json(base: string) { return {
	type: BlockType.SimpleFunctionDefinition,
	message0: "name %1 teach %2",
	args0: [
		{
			type: "field_input",
			name: "NAME",
			text: function_definition_default_text,
		},
		{
			type: "field_image",
			src: get_add_icon_url(base),
			width: icon_size,
			height: icon_size,
			name: "SAVE_BUTTON",
		},
	],
	message1: "steps %1",
	args1: [
		{
			type: "input_statement",
			name: "BODY",
		}
	],
	inputsInline: true,
	lastDummyAlign2: "RIGHT",
	colour: 130,
	extensions: ["save_button_extension"],
};}

/**
 * Helper function to get the type of a block.
 */
function get_block_type(block: {type: string}): BlockType {
	for(const type of Object.values(BlockType))
		if(block.type === type)
			return type;
	throw 'Type ' + block.type + ' is unsupported!';
}

/**
 * Helper function to get the statement children of a function definition.
 */
function get_statement_children<T extends Blockly.Block>(block: T): T[] {
	// Gather each block in the statement input
	const children = [];
	const input = block.getInput("BODY");
	if(input === null)
		throw 'Program has no BODY input';
	const connection = input.connection;
	if(connection.isConnected()) {
		let child: T = connection.targetConnection.getSourceBlock() as T;
		let next_child = null;
		do {
			children.push(child);
			next_child = child.getNextBlock();
			if(next_child !== null)
				child = next_child as T;
		} while(next_child !== null);
	}
	return children;
}

/**
 * Parse the block into a signature.
 */
function parse_signature(
		block: Blockly.Block,
		dropdown_option_to_post_condition: (o: string)=>string|null,
		): NaturalSignature {
	let signature: NaturalSignature = {};
	const name = block.getFieldValue("NAME");
	if (name.length > 0) {
		signature = {...signature, name: block.getFieldValue("NAME")};
	}
	const post_condition_value = block.getFieldValue("POST_CONDITION");
	const post_condition = dropdown_option_to_post_condition(post_condition_value);
	if (post_condition !== null)
		signature = {...signature, post_condition: post_condition};
	return signature;
}

/**
 * Parse the block into a natural procedure sketch.
 */
function parse_natural_procedure(
		block: Blockly.Block,
		dropdown_option_to_primitive: (o: string)=>string,
		dropdown_option_to_post_condition: (o: string)=>string|null,
		): NaturalProcedureSketch {
	// Recursively convert each child block into
	// a natural program
	const children = get_statement_children(block);
	const steps = [];
	for(const child of children)
		steps.push(get_natural_sketch(
			child,
			dropdown_option_to_primitive,
			dropdown_option_to_post_condition
		));
	return {
		steps: steps,
		signature: parse_signature(block, dropdown_option_to_post_condition),
	};
}

/**
 * Recursively transform a Blockly block into a natural program.
 */
function get_natural_sketch(
		block: Blockly.Block,
		dropdown_option_to_primitive: (o: string)=>string,
		dropdown_option_to_post_condition: (o: string)=>string|null,
		): NaturalProgramSketch {
	// From the definition of NaturalProgramSketch there are only three cases:
	// primitives, signatures and natural procedures.
	const block_type = get_block_type(block);

	// Primitive case
	if (block_type === BlockType.Primitive) {
		const data = block.data;
		if (data === null)
			throw "Attempted to parse dropdown primitive with no data!";
		return JSON.parse(data).np;
	}
	if (block_type === BlockType.DropdownPrimitive) {
		const option = block.getFieldValue("ILLUSTRATION");
		const primitive = dropdown_option_to_primitive(option);
		return dropdown_option_to_primitive(primitive);
	}

	// Signature case
	if(block_type === BlockType.Signature || block_type === BlockType.SimpleSignature)
		return parse_signature(block, dropdown_option_to_post_condition);

	// Build the sketch structure
	if (block_type === BlockType.FunctionDefinition ||
		  block_type === BlockType.SimpleFunctionDefinition)
		return parse_natural_procedure(
			block,
			dropdown_option_to_primitive,
			dropdown_option_to_post_condition
		);

	throw 'Cannot parse ' + block_type + '!';
}

/**
 * Return a Blockly JSON definition for the given primitive and illustration
 * image URL.
 */
function primitive_as_block_info(
		name: string,
		np: string,
		): Blockly.utils.toolbox.BlockInfo {
	const data = JSON.stringify({np: np});
	return {
		kind: "block",
		type: BlockType.Primitive,
		fields: {
				NAME: name,
		},
		data: data,
	};
}

/**
 * Return a Blockly JSON definition for the given natural program.
 */
function signature_as_block_info(
		np: NaturalSignature,
		options: DropdownOption[],
		option: string,
		base: string,
		): Blockly.utils.toolbox.BlockInfo {
	const signature: Blockly.utils.toolbox.BlockInfo = {
		...get_signature_json(options, base),
		fields: {
			POST_CONDITION: option,
		}
	};
	if (np.name !== undefined)
		signature.fields = {...signature.fields, NAME: np.name};
	return signature;
}

/**
 * Return a Blockly JSON definition for the given natural program.
 */
function simple_signature_as_block_info(
		np: NaturalSignature,
		): Blockly.utils.toolbox.BlockInfo {
	const signature: Blockly.utils.toolbox.BlockInfo = {
		kind: "block",
		type: BlockType.SimpleSignature,
	};
	if (np.name !== undefined)
		signature.fields = {...signature.fields, NAME: np.name};
	return signature;
}


/**
 * Initialize the Blockly API. This function has application-wide side
 * effects and should only be run once.
 */
function init_api<ProgramState>(
		get_initial_state: () => ProgramState,
		execution_pause_ms: number,
		update_ui_state: (state: ProgramState) => unknown,
		success_execution_callback: (block: Blockly.Block, outcome: Outcome<ProgramState>)=>Promise<void>,
		fail_execution_callback: (np: NaturalProgramSketch, error: unknown, milliseconds: number)=>Promise<void>,
		save_successful_execution: (r: Outcome<ProgramState>, block: Blockly.BlockSvg)=>Promise<void>,
		solver: SketchSolver<ProgramState>,
		dropdown_option_to_primitive: (option: string)=>string,
		dropdown_option_to_post_condition: (option: string)=>string|null,
		signature_dropdown_options: DropdownOption[],
		primitive_dropdown_options: DropdownOption[],
		function_definition_dropdown_options: DropdownOption[],
		is_runnable: (np: NaturalProgramSketch, state: ProgramState) => boolean,
		get_automatic_post_condition: (query: string) => Promise<string|null>,
		execute_primitive: (primitive: string, state: ProgramState) => ProgramState,
		base: string, // application base path
		) {
	let execution_semaphore = false;

	/**
	 * Triggered when the user clicks on a play button.
	 */
	async function execute_block(
			button: Blockly.FieldImage,
			save_if_successful: boolean,
			update_ui_state_if_succesful: boolean,
			base: string,
			) {
		// Cast: the type checker doesn't know we're working in an SVG workspace.
		const clicked_block = button.getSourceBlock() as Blockly.BlockSvg;

		// Abort if block is in library
		if (clicked_block.isInFlyout) {
			return;
		}

		// Ask if a post condition should be added
		{
			const post_condition = clicked_block.getFieldValue("POST_CONDITION");
			const name = clicked_block.getFieldValue("NAME");
			if (
			     post_condition === NONE_DROPDOWN_OPTION
			     && name !== signature_default_text
			     && clicked_block.type === BlockType.Signature
			     ) {
				const post_condition_option = await get_automatic_post_condition(
					name
				);
				if (post_condition_option === null) {
					return;
				} else {
					// Assign post-condition
					clicked_block.getField("POST_CONDITION").setValue(post_condition_option);
					clicked_block.setEditable(false);

					// Mark the block as auto-assigned
					clicked_block.data = true;
				}
			}
		}

		// Parse blocks into sketch
		const sketch = get_natural_sketch(
			clicked_block as Blockly.Block,
			dropdown_option_to_primitive,
			dropdown_option_to_post_condition
		);

		// Abort if program is not runnable
		const state = get_initial_state();
		if (!is_runnable(sketch, state))
			return;

	 // Abort if another program is being executed
		if (execution_semaphore)
			return;

		// Semaphore correctness due to single-threaded JS semantics
		// (functions are never partially executed in JS if no await is executed)
		execution_semaphore = true;

		// Disable some ways to modify the program. This is does not guarantee 100%
		// that the program was not modified (e.g. new blocks dragged from the
		// toolbox can still be used to modify it, CTRL-Z, etc), but it's enough to
		// prevent accidental editions.
		Blockly.Events.setRecordUndo(false);
		const affected_blocks: Blockly.Block[] = clicked_block.workspace.getAllBlocks(false);
		affected_blocks.map(block=>block.setMovable(false));
		affected_blocks.map(block=>block.setEditable(false));
		affected_blocks.map(block=>block.setDeletable(false));
		// (set the clicked block as editable, otherwise click events are not
		// fired).
		clicked_block.setEditable(true);

		// Once a program is submitted to the solver, its fields cannot be edited.
		// There is no technical reason behind this. This is just because
		// users got confused in our pilot study.
		// Fields that are images are not disabled because they have to be enabled
		// to register click events (like clicking on play).
		// HACK: function definitions are excempt from this because the "this already exists"
		// check is done after execution, when it should be done before.
		if (get_block_type(clicked_block) !== BlockType.SimpleFunctionDefinition) {
			const fields: Blockly.Field[] = clicked_block.inputList.flatMap(function(input: Blockly.Input) { return input.fieldRow });
			const editable_fields = fields.filter(field=>!(field instanceof Blockly.FieldImage));
			for (const field of editable_fields)
				field.setEnabled(false);
		}

		// Change appearance of blocks
		affected_blocks.map(block=>{
			const play_button = block.getField("PLAY_BUTTON");
			if (play_button !== null) {
				play_button.setValue(get_play_disabled_icon_url(base));
				play_button.setTooltip(play_button_wait_tooltip);
			}
			const save_button = block.getField("SAVE_BUTTON");
			if (save_button !== null) {
				save_button.setValue(get_play_disabled_icon_url(base));
			}
		});

		// Record undo events again
		Blockly.Events.setRecordUndo(true);

		// Run the solver
		update_ui_state(state);
		const solver_start = Date.now();
		solver(
			sketch,
			state,
		).then(async (result) => {
			const outcome = {...result, sketch: sketch}

			// If the program was modified while the solver was running, exit without
			// any side-effects.
			const new_sketch = get_natural_sketch(
				clicked_block as Blockly.Block,
				dropdown_option_to_primitive,
				dropdown_option_to_post_condition
			);
			if (JSON.stringify(sketch) !== JSON.stringify(new_sketch))
				return;

			// Notify of successful execution
			await success_execution_callback(
				clicked_block as Blockly.Block,
				outcome,
			);

			if (save_if_successful)
				await save_successful_execution(outcome, clicked_block);

			// Update the play button icon
			Blockly.Events.setRecordUndo(false);
			const play_button = clicked_block.getField("PLAY_BUTTON");
			if (play_button !== null) {
				play_button.setValue(get_play_disabled_icon_url(base));
				play_button.setTooltip(play_button_wait_tooltip);
			}

			// Update UI if requested
			if (update_ui_state_if_succesful) {
				// Highlight block for visual feedback
				clicked_block.workspace.highlightBlock(clicked_block.id);
				await new Promise(resolve => setTimeout(resolve, execution_pause_ms));

				// Execute each primitive to find the final state
				let final_state = state;
				for (const primitive of get_unrolled_implementation(outcome.program)) {
					final_state = execute_primitive(primitive, final_state);

				}

				Blockly.Events.setRecordUndo(true);

				// Update UI
				update_ui_state(final_state);
			}
		}).catch(err=>
			fail_execution_callback(sketch, err, Date.now()-solver_start)
		).finally(()=>{
			// Reset block appearances
			Blockly.Events.setRecordUndo(false);
			clicked_block.workspace.highlightBlock(null);
			affected_blocks.map(block=>block.setMovable(true));
			affected_blocks.map(block=>block.setEditable(true));
			affected_blocks.map(block=>block.setDeletable(true));
			affected_blocks.map(block=>{
				const play_button = block.getField("PLAY_BUTTON");
				if (play_button !== null) {
					play_button.setValue(get_play_icon_url(base));
					play_button.setTooltip(play_button_tooltip);
					(play_button as Blockly.FieldImage).setOnClickHandler(
						(b: Blockly.FieldImage)=>execute_block(b, false, true, base)
					);
				}
				const save_button = block.getField("SAVE_BUTTON");
				if (save_button !== null) {
					save_button.setValue(get_add_icon_url(base));
					(save_button as Blockly.FieldImage).setOnClickHandler(
						(b: Blockly.FieldImage)=>execute_block(b, true, false, base)
					);
				}
			});
			Blockly.Events.setRecordUndo(true);

			// Release the execution semaphore
			execution_semaphore = false;
		});
	};

	Blockly.Blocks[BlockType.FunctionDefinition] = {
		init: function() {
			this.jsonInit(get_function_definition_json(function_definition_dropdown_options, base));
		}
	};

	Blockly.Blocks[BlockType.SimpleFunctionDefinition] = {
		init: function() {
			this.jsonInit(get_simple_function_definition_json(base));
		}
	};

	Blockly.Blocks[BlockType.Signature] = {
		init: function() {
			this.jsonInit(get_signature_json(signature_dropdown_options, base));
		}
	};

	Blockly.Blocks[BlockType.SimpleSignature] = {
		init: function() {
			this.jsonInit(get_simple_signature_json(base));
		}
	};

	Blockly.Blocks[BlockType.DropdownPrimitive] = {
		init: function() {
			this.jsonInit(get_dropdown_primitive_json(primitive_dropdown_options, base));
		}
	};

	Blockly.Blocks[BlockType.Primitive] = {
		init: function() {
			this.jsonInit(get_primitive_json(base));
		}
	};

	// Disable unsupported operations
	const registry = Blockly.ContextMenuRegistry.registry;
	if (registry !== null)
		for (const id of [
			"blockComment",
			"blockDisable",
			"blockInline",
			"collapseWorkspace",
			"expandWorkspace",
			"blockCollapseExpand",
			"blockHelp"
			])
		try{
			registry.unregister(id);
		} catch {
			// Item was previously unregistered
		}

	// Register extensions
	try {
		Blockly.Extensions.unregister('play_button_extension');
	} catch {
		// Extension was not previously registered
	}
	Blockly.Extensions.register('play_button_extension', function () {
		// @ts-expect-error `this` is always a Blockly Block, but the type system
		// cannot infer that
		const play_button = this.getField("PLAY_BUTTON");
		if (play_button !== null) {
			play_button.setTooltip(play_button_tooltip);
			(play_button as Blockly.FieldImage).setOnClickHandler(
				(b: Blockly.FieldImage)=>execute_block(b, false, true, base)
			);
		}
	}); 

	try {
		Blockly.Extensions.unregister('save_button_extension');
	} catch {
		// Extension was not previously registered
	}
	Blockly.Extensions.register('save_button_extension', function () {
		// @ts-expect-error `this` is always a Blockly Block, but the type system
		// cannot infer that
		const save_button = this.getField("SAVE_BUTTON");
		if (save_button !== null) {
			(save_button as Blockly.FieldImage).setOnClickHandler(
				(b: Blockly.FieldImage)=>execute_block(b, true, false, base)
			);
		}
	}); 
}

export {
	init_api,
	get_natural_sketch,
	get_block_type,
	BlockType,
	signature_as_block_info,
	simple_signature_as_block_info,
	parse_signature,
	primitive_as_block_info,
	icon_size,
	signature_default_text,
	function_definition_default_text,
	NONE_DROPDOWN_OPTION,
};

export type {
	Outcome,
	DropdownOption,
	DropdownInfo,
};
