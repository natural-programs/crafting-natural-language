import { initializeApp } from "firebase/app";
import { getDatabase, ref, set, push, child, get, serverTimestamp, orderByChild } from "firebase/database";
import { query } from "firebase/database";
import { equalTo } from "firebase/database";
import { runTransaction } from "firebase/database";
import type { NaturalProgramSketch } from "natural-programs/types";
import type { SessionSnapshot } from "./session_snapshot";
import { SessionType } from "./session_snapshot";
import type { SurveyResponse } from "./survey";
import { snapshot_as_json, snapshot_from_json } from "./session_snapshot";
import { getStorage, ref as storage_ref } from "firebase/storage";
import { uploadBytes } from "firebase/storage";
import { getBytes } from "firebase/storage";
import type { eventWithTime } from 'rrweb/typings/types';
import type { Event, SuccessfulExecution } from './events';
import type { FailedExecution } from './events';
import type { GoalParameters } from './goal';
import { GoalSource } from './goal';
import pako from "pako";


enum InventorySource {
	CopyPreviousInventory = "COPY_PREVIOUS_INVENTORY",
	BaseInventory = "BASE_INVENTORY",
};


type Chain = {
	initial_snapshot_id: string
	metadata?: string,
	goal_parameters?: GoalParameters,
	chain_seed: string,
	session_timeout_ms: number|null,
	sat_item_bonus_usd: number|null,
	completion_code: string,
	minimum_sat_items_to_store_node: number,
	inventory_source: InventorySource,
};

type ChainNode = {
	id: string,
	chain_id: string,
	previous_node_id?: string,
	session_id: string,
	goal?: string[],
};

type RejectedChainNode = ChainNode & { rejection_cause: string };

type Session = {
	active_snapshot_id: string,
	start_snapshot_id: string,
	user_id: string,
	replay_id: null|string,
};

const firebaseConfig = {
	apiKey: "AIzaSyCH_viepPq5oJl3VkQN_8qEpXUFwMDh0WU",
	authDomain: "crafting-natural-language.firebaseapp.com",
	databaseURL: "https://crafting-natural-language-default-rtdb.firebaseio.com",
	projectId: "crafting-natural-language",
	storageBucket: "crafting-natural-language.appspot.com",
	messagingSenderId: "231930650365",
	appId: "1:231930650365:web:01fa971d16fa173ae9e894"
};

initializeApp(firebaseConfig);

//const auth = getAuth();

async function sign_in_anonymously() {
	//await signInAnonymously(auth);
}

function get_serialized_np(np: NaturalProgramSketch): string {
	return JSON.stringify(np);
}

async function get_new_snapshot_id(): Promise<string> {
	const db = getDatabase();
	const snapshots_ref = ref(db, "snapshots");
	const new_snapshot_id = push(snapshots_ref).key;
	if (new_snapshot_id === null)
		throw "Could not generate new snapshot ID!";
	return new_snapshot_id;
}

async function get_new_chain_node_id(): Promise<string> {
	const db = getDatabase();
	const snapshots_ref = ref(db, "chain_nodes");
	const new_snapshot_id = push(snapshots_ref).key;
	if (new_snapshot_id === null)
		throw "Could not generate new chain node ID!";
	return new_snapshot_id;
}

async function get_new_session_id(): Promise<string> {
	const db = getDatabase();
	const ref_ = ref(db, "sessions");
	const new_id = push(ref_).key;
	if (new_id === null)
		throw "Could not generate new session ID!";
	return new_id;
}

async function get_new_user_id(): Promise<string> {
	const db = getDatabase();
	const ref_ = ref(db, "user_id");
	const new_id = push(ref_).key;
	if (new_id === null)
		throw "Could not generate new user ID!";
	return new_id;
}

async function get_new_replay_id(): Promise<string> {
	const db = getDatabase();
	const ref_ = ref(db, "replay_id");
	const new_id = push(ref_).key;
	if (new_id === null)
		throw "Could not generate new replay ID!";
	return new_id;
}

async function write_chain(chain_id: string, chain: Chain) {
	const db = getDatabase();
	return set(ref(db, 'chains/' + chain_id), chain);
}

async function set_available_chains(chain_ids: string[]) {
	const db = getDatabase();
	await set(ref(db, 'available_chains'), null);
	for (const chain_id of chain_ids) {
		await set(ref(db, 'available_chains/'+chain_id), {last_ping_unix_time: 0});
	}
}

async function write_snapshot(snapshot_id: string, snapshot: SessionSnapshot) {
	const snapshot_json = snapshot_as_json(snapshot);
	return write_snapshot_json(snapshot_id, snapshot_json);
}

// Same as write_snapshot but with a JSON string instead of JSON object
async function write_snapshot_json(snapshot_id: string, snapshot_json: string) {
	const db = getDatabase();
	return set(ref(db, 'snapshots/' + snapshot_id), JSON.parse(snapshot_json));
}

async function write_default_snapshot(snapshot_id: string, snapshot: SessionSnapshot) {
	const db = getDatabase();
	const snapshot_json = snapshot_as_json(snapshot);
	return set(ref(db, 'default_snapshots/' + snapshot_id), JSON.parse(snapshot_json));
}

async function write_chain_node(node: ChainNode) {
	const db = getDatabase();
	return set(ref(db, 'chain_nodes/' + node.id), node);
}

async function write_rejected_chain_node(node: RejectedChainNode) {
	const db = getDatabase();
	return push(ref(db, 'rejected_chain_nodes'), node);
}

async function write_session(session_id: string, session: Session) {
	const db = getDatabase();
	return set(ref(db, 'sessions/' + session_id), session);
}

async function write_event(event: Event) {
	const db = getDatabase();
	const _ref = await push(ref(db, 'events'), event);
	console.log({database_ref: _ref, event: event});
}

async function get_snapshot(snapshot_id: string, icon_directory: string): Promise<SessionSnapshot> {
	const db = ref(getDatabase());
	const data = await get(child(db, "snapshots/"+snapshot_id));
	if (!(data.exists()))
		throw "Snapshot '" + snapshot_id + "' does not exist in database!";
	const raw_val = data.val();
	const snapshot_json = (typeof raw_val === "string") ? raw_val : JSON.stringify(raw_val);
	return snapshot_from_json(snapshot_json, icon_directory);
}

async function get_default_snapshot(snapshot_id: string, icon_directory: string): Promise<SessionSnapshot> {
	const db = ref(getDatabase());
	const data = await get(child(db, "default_snapshots/"+snapshot_id));
	if (!(data.exists()))
		throw "Default snapshot '" + snapshot_id + "' does not exist in database!";
	const raw_val = data.val();
	const snapshot_json = (typeof raw_val === "string") ? raw_val : JSON.stringify(raw_val);
	return snapshot_from_json(snapshot_json, icon_directory);
}

async function get_default_snapshots(icon_directory: string): Promise<{id: string, snapshot: SessionSnapshot}[]> {
	const db = ref(getDatabase());
	const data = await get(child(db, "default_snapshots"));
	if (!(data.exists()))
		throw "Default snapshots could not be retrieved from database!";
	const snapshots = [];
	for (const [id, snap] of Object.entries(data.val())) {
		const snapshot_json = (typeof snap === "string") ? snap : JSON.stringify(snap);
		snapshots.push({id: id, snapshot: await snapshot_from_json(snapshot_json, icon_directory)});
	}
	return snapshots;
}

async function get_chain(chain_id: string): Promise<Chain> {
	const db = ref(getDatabase());
	const data = await get(child(db, "chains/"+chain_id));
	if (!(data.exists()))
		throw "Chain '" + chain_id + "' does not exist in database!";
	let chain = data.val();

	// Legacy support: goal parameters
	if (chain.goal_parameters !== undefined) {
		let goal_parameters = chain.goal_parameters;
		if (goal_parameters.source === undefined) {
			// Legacy support: original goal parameters had no leafs-only support
			if (goal_parameters.leafs_only === undefined ||
					goal_parameters.leafs_only == false)
				goal_parameters = {
					...goal_parameters,
					source: GoalSource.AllCraftableItems,
				}
			else if (goal_parameters.leafs_only === true) {
				goal_parameters = {
					...goal_parameters,
					source: GoalSource.CurrentTreeLeafs,
				}
			}
		}
		chain = {...chain, goal_parameters: goal_parameters};
	}

	// Legacy support: inventory source
	if (chain.inventory_source === undefined)
		chain.inventory_source = InventorySource.BaseInventory;

	// Legacy support: Multisolver mode
	if (chain.session_type === "LLMNaturalProgramming + Natural Programming + Direct Synthesis")
		chain.session_type = SessionType.Multisolver;

	return {
		...chain,
		session_timeout_ms: (chain.session_timeout_ms !== undefined) ? chain.session_timeout_ms : null,
		sat_item_bonus_usd: (chain.sat_item_bonus_usd !== undefined) ? chain.sat_item_bonus_usd : null,
	}
}

async function get_chain_ids(): Promise<string[]> {
	const db = ref(getDatabase());
	const data = await get(child(db, "chains"));
	if (!(data.exists()))
		return [];
	return Object.keys(data.val());
}

async function get_available_chain_ids(): Promise<string[]> {
	const db = ref(getDatabase());
	const data = await get(child(db, "available_chains"));
	if (!(data.exists()))
		return [];
	return Object.keys(data.val());
}

async function get_session(session_id: string): Promise<Session> {
	const db = ref(getDatabase());
	const data = await get(child(db, "sessions/"+session_id));
	if (!(data.exists()))
		throw "Session '" + session_id + "' does not exist in database!";
	let session = data.val();
	// Legacy support
	if (!("replay_id" in session))
		session = {...session, replay_id: null};
	return session;
}

async function get_active_chain_node(chain_id: string): Promise<{node: ChainNode, height: number}|undefined> {
	const db = ref(getDatabase());
	const data = await get(child(db, "chain_nodes"));

	// Seems like all chains are empty
	if (!(data.exists()))
		return undefined;

	// Get nodes in chain
	const nodes = Object.values(data.val() as ChainNode[]).filter(
		node=>node.chain_id===chain_id
	);

	// If there are no nodes associated with the chain, there is no active node
	if (nodes.length === 0)
		return undefined;

	// Get leaf of the longest branch
	const node_map = new Map<string, ChainNode>(nodes.map(n=>[n.id, n]));
	function height(id: string): number {
		const node = node_map.get(id);
		if (node === undefined)
			throw "Inconsistent chain in database!";
		if (node.previous_node_id !== undefined)
			return height(node.previous_node_id)+1;
		return 1;
	}

	// Not the most efficient way to do this, but it is succinct
	const active_node = node_map
		.get(nodes
		.map(n=>n.id)
		.reduce(
			(best_node_id_so_far, other_id)=>height(best_node_id_so_far)>height(other_id) ?
				best_node_id_so_far : other_id
		));
	if (active_node === undefined)
		throw "Unexpected error when computing active node in chain!";
	const node_height = height(active_node.id);
	if (node_height === undefined)
		throw "Unexpected error when computing active node in chain!";
	return {node: active_node, height: node_height};
}


async function get_chain_node(chain_id: string, i: number): Promise<{node: ChainNode, height: number}> {
	const db = ref(getDatabase());
	const data = await get(child(db, "chain_nodes"));

	// Seems like all chains are empty
	if (!(data.exists()))
		throw "Chain not found!";

	// Get nodes in chain
	const nodes = Object.values(data.val() as ChainNode[]).filter(
		node=>node.chain_id===chain_id
	);

	// If there are no nodes associated with the chain, there is no active node
	if (nodes.length === 0)
		throw "Chain is empty!";

	// Get leaf of the longest branch
	const node_map = new Map<string, ChainNode>(nodes.map(n=>[n.id, n]));
	function height(id: string): number {
		const node = node_map.get(id);
		if (node === undefined)
			throw "Inconsistent chain in database!";
		if (node.previous_node_id !== undefined)
			return height(node.previous_node_id)+1;
		return 1;
	}

	// Sort nodes chronologically, from first to last
	nodes.sort(function(a, b) {
		const ah = height(a.id);
		const bh = height(b.id);
		return ah - bh;
	});

	const node = nodes[i];
	return {node: node, height: height(node.id)};
}


/**
 * Returns unix time in seconds.
 */
async function get_unix_time(
		session_id: string,
		): Promise<number> {
	const db = getDatabase();
	const node = {
		session_id: session_id,
		unix_time_ms: serverTimestamp(),
	};
	const reference = await push(ref(db, 'tmp_timestamps'), node);
	const milliseconds = (await get(reference)).val().unix_time_ms;
	return milliseconds/1000;
}

async function get_successful_executions(): Promise<SuccessfulExecution[]> {
	const db = ref(getDatabase());
	const data = await get(child(db, "events"));
	if (!(data.exists()))
		return [];
	// TODO: filter for kind==="..." in the query
	const events = Object.values(data.val()) as Event[];
	return events.filter(e=>e.kind === "successful_execution") as SuccessfulExecution[];
}

async function get_failed_executions(): Promise<FailedExecution[]> {
	const db = ref(getDatabase());
	const data = await get(child(db, "events"));
	if (!(data.exists()))
		return [];
	// TODO: filter for kind==="..." in the query
	const events = Object.values(data.val()) as Event[];
	return events.filter(e=>e.kind === "failed_execution") as FailedExecution[];
}

async function get_chain_nodes(): Promise<ChainNode[]> {
	const db = ref(getDatabase());
	const data = await get(child(db, "chain_nodes"));
	if (!(data.exists()))
		return [];
	return Object.values(data.val());
}

async function get_fallback(path: string): Promise<unknown> {
	const db = ref(getDatabase());
	const data = await get(child(db, "fallbacks/"+path));
	if (!(data.exists()))
		throw "Fallback '" + path + "' does not exist!";
	return data.val();
}

/**
 * Updates the last active time of the given chain to the current
 * milliseconds since Unix epoch.
 */
async function write_chain_activity_ping(chain_id: string): Promise<void> {
	const db_ref = ref(getDatabase(), "available_chains/"+chain_id+"/last_ping_unix_time");
	return set(db_ref, serverTimestamp());
}

/**
 * Atomic operations that returns the ID of a chain in /available_chains that
 * has not been active in at least `ms_for_inactive_chain` and updates its last
 * active time.
 *
 * Throws an error if there are no such chains.
 *
 * This is different from the active node of a chain.
 */
async function assign_available_chain_id(
		ms_for_inactivity: number,
		current_unix_time: number,
		): Promise<string> {
	const db_ref = ref(getDatabase(), "available_chains");
	let assigned_chain_id: (null|string) = null;
	const seconds_for_inactivity = ms_for_inactivity/1000;

	// Get available chain lengths
	// This should technically be done inside `runTransaction`
	// to get up-to-date data in each transaction attempt.
	// Due to Firebase API limitations, this would require a refactor
	// and thus, we operate with slightly out of date data.
	// (The risk is minimal: a participant waiting for a slot
	// could require to reload the page if we update the
	// active chains while the participant is waiting).
	const chain_heights = new Map<string, number>();
	const active_chain_ids = Object.keys((await get(db_ref)).val());
	for (const chain_id of active_chain_ids) {
		const active_node = await get_active_chain_node(chain_id);
		const height = (active_node === undefined) ?
			0 : active_node.height;
		chain_heights.set(chain_id, height);
	}

	const result = await runTransaction(db_ref, function(current_data) {
		// On first call current_data will be null
		// https://github.com/firebase/firebase-android-sdk/issues/1447
		if (current_data === null)
			return "";

		// Find the length of the shortest chain and
		// identify active chains
		let shortest_length = null;
		const active_data: [string, number][] = [];
		for (const [chain_id, chain_state] of Object.entries<{last_ping_unix_time: number}>(current_data)) {
			// Firebase stores Unix time in milliseconds
			const last_ping_time = chain_state.last_ping_unix_time/1000;
			const inactive_time = current_unix_time - last_ping_time;

			// Get chain height
			const height = chain_heights.get(chain_id);
	
			// Identify if active
			const is_active = inactive_time >= seconds_for_inactivity;
			if (is_active && height !== undefined) {

				// Record active chain data
				active_data.push([chain_id, height]);

				// Update shortest length
				if (shortest_length === null)
					shortest_length = height;
				else
					shortest_length = Math.min(shortest_length, height);
			}
		}

		// Find an inactive chain
		for (const [chain_id, height] of active_data) {
			// Identify the first inactive chain of minimum length
			if (height === shortest_length) {
				assigned_chain_id = chain_id;
				break;
			}
		}
		if (assigned_chain_id !== null) {
			current_data[assigned_chain_id] = {
				last_ping_unix_time: serverTimestamp(),
			};
			return current_data;
		}
		return;
	});
	if (!(result.committed) || assigned_chain_id === null)
		throw "There are no inactive chains!";
	return assigned_chain_id;
}

async function write_survey_response(response: SurveyResponse) {
	const db = getDatabase();
	return push(ref(db, 'survey_reponses'), response);
}

async function get_survey_responses(): Promise<SurveyResponse[]> {
	const db = ref(getDatabase());
	const data = await get(child(db, "survey_reponses"));
	if (!(data.exists()))
		return [];
	return Object.values(data.val());
}

async function write_tutorial_completion(user_id: string) {
	const db = getDatabase();
	return push(ref(db, 'tutorial_completions'), {user_id: user_id});
}

/**
 * Returns successful executions in nodes belonging to the
 * given chain.
 */
async function get_chain_successful_executions(
		chain_id: string
		): Promise<((SuccessfulExecution)&{node_i: number})[]>{
	const db = ref(getDatabase());
	const database_nodes = (await get(query(
		child(db, "chain_nodes"),
		orderByChild("chain_id"),
		equalTo(chain_id),
	))).val();
	const chain_nodes = (database_nodes === null) ? [] : Object.values(database_nodes) as ChainNode[];
	const session_ids = chain_nodes.map(n=>n.session_id);
	const executions = [];
	for (let node_i = 0; node_i < session_ids.length; node_i++) {
		const session_id = session_ids[node_i];
		const events = Object.values((await get(query(
			child(db, "events"),
			orderByChild("session_id"),
			equalTo(session_id),
		))).val()) as Event[];
		const db_successful_executions = events.filter(e=>e.kind === "successful_execution") as SuccessfulExecution[];
		const session_successful_executions = (db_successful_executions === null) ?
			[] : Object.values(db_successful_executions) as SuccessfulExecution[];
		const session_executions = [
			...session_successful_executions,
		];
		executions.push(...session_executions.map(e=>{return {...e, node_i: node_i}}));
	}
	return executions;
}

/**
 * Returns both successful and failed executions in nodes belonging to the
 * given chain.
 */
async function get_chain_failed_executions(
		chain_id: string
		): Promise<((FailedExecution)&{node_i: number})[]>{
	const db = ref(getDatabase());
	const database_nodes = (await get(query(
		child(db, "chain_nodes"),
		orderByChild("chain_id"),
		equalTo(chain_id),
	))).val();
	const chain_nodes = (database_nodes === null) ? [] : Object.values(database_nodes) as ChainNode[];
	const session_ids = chain_nodes.map(n=>n.session_id);
	const executions = [];
	for (let node_i = 0; node_i < session_ids.length; node_i++) {
		const session_id = session_ids[node_i];
		const events = Object.values((await get(query(
			child(db, "events"),
			orderByChild("session_id"),
			equalTo(session_id),
		))).val()) as Event[];
		const db_failed_executions = events.filter(e=>e.kind === "failed_execution") as FailedExecution[];
		const session_failed_executions = (db_failed_executions === null) ?
			[] : Object.values(db_failed_executions) as FailedExecution[];
		const session_executions = [
			...session_failed_executions
		];
		executions.push(...session_executions.map(e=>{return {...e, node_i: node_i}}));
	}
	return executions;
}

/**
 * Returns both successful and failed executions in nodes belonging to the
 * given chain.
 */
async function get_chain_executions(
		chain_id: string
		): Promise<((SuccessfulExecution|FailedExecution)&{node_i: number})[]>{
	const db = ref(getDatabase());
	const database_nodes = (await get(query(
		child(db, "chain_nodes"),
		orderByChild("chain_id"),
		equalTo(chain_id),
	))).val();
	const chain_nodes = (database_nodes === null) ? [] : Object.values(database_nodes) as ChainNode[];
	const session_ids = chain_nodes.map(n=>n.session_id);
	const executions = [];
	for (let node_i = 0; node_i < session_ids.length; node_i++) {
		const session_id = session_ids[node_i];
		const db_successful_executions = (await get(query(
			child(db, "events"),
			orderByChild("session_id"),
			equalTo(session_id),
		))).val();
		const session_successful_executions = (db_successful_executions === null) ?
			[] : Object.values(db_successful_executions) as SuccessfulExecution[];
		const db_failed_executions = (await get(query(
			child(db, "events"),
			orderByChild("session_id"),
			equalTo(session_id),
		))).val();
		const session_failed_executions = (db_failed_executions === null) ?
			[] : Object.values(db_failed_executions) as FailedExecution[];
		const session_executions = [
			...session_successful_executions,
			...session_failed_executions
		];
		executions.push(...session_executions.map(e=>{return {...e, node_i: node_i}}));
	}
	return executions;
}

async function write_automatic_post_condition_answer(
		query: string,
		post_condition: string,
		success: boolean,
		session_id: string
		) {
	const db = getDatabase();
	return push(
		ref(db, 'post_condition_answers'),
		{query: query, post_condition: post_condition, success: success, session_id: session_id}
	);
}

async function write_execution_cancellation(
		session_id: string,
		np: NaturalProgramSketch,
		snapshot_id: string,
		error: string,
		milliseconds: number,
		) {
	const db = getDatabase();
	// Legacy:
	// Store stringified np to avoid maximum-depth error
	// in firebase
	return push(
		ref(db, 'post_condition_answers'),
		{
			session_id: session_id,
			np: {serialized_np: get_serialized_np(np)},
			snapshot_id: snapshot_id,
			error: error,
			milliseconds: milliseconds,
		}
	);
}

async function get_snapshot_total_ms(snapshot_id: string): Promise<number> {
	const db = ref(getDatabase());
	const data = await get(child(db, "snapshots/"+snapshot_id+"/total_ms"));
	if (!(data.exists()))
		throw "Default snapshot '" + snapshot_id + "' does not exist in database!";
	return data.val();
}

async function get_snapshot_solver_ms(snapshot_id: string): Promise<number> {
	const db = ref(getDatabase());
	const data = await get(child(db, "snapshots/"+snapshot_id+"/solver_ms"));
	if (!(data.exists()))
		throw "Solver time for snapshot'" + snapshot_id + "' does not exist in database!";
	return data.val();
}

async function get_snapshot_show_recipes_ms(snapshot_id: string): Promise<number> {
	const db = ref(getDatabase());
	const data = await get(child(db, "snapshots/"+snapshot_id+"/show_recipes_ms"));
	// Legacy support: data may not exist in all snapshots
	if (!(data.exists()))
		return 0.0;
	return data.val();
}

async function get_snapshot_show_tutorial_ms(snapshot_id: string): Promise<number> {
	const db = ref(getDatabase());
	const data = await get(child(db, "snapshots/"+snapshot_id+"/show_tutorial_ms"));
	// Legacy support: data may not exist in all snapshots
	if (!(data.exists()))
		return 0.0;
	return data.val();
}

async function get_snapshot_show_library_ms(snapshot_id: string): Promise<number> {
	const db = ref(getDatabase());
	const data = await get(child(db, "snapshots/"+snapshot_id+"/show_library_ms"));
	// Legacy support: data may not exist in all snapshots
	if (!(data.exists()))
		return 0.0;
	return data.val();
}

async function get_snapshot_sat_items(snapshot_id: string): Promise<string[]> {
	const db = ref(getDatabase());
	const data = await get(child(db, "snapshots/"+snapshot_id+"/sat_items_names"));
	if (!(data.exists()))
		return [];
	return data.val();
}

async function write_replay(replay_id: string, replay: unknown) {
	const serialized_replay = JSON.stringify(replay);
	const compressed_replay = pako.deflate(serialized_replay);
	const storage = getStorage();
	const storageRef = storage_ref(storage, 'replays/'+replay_id);
	return uploadBytes(storageRef, compressed_replay);
}

async function get_replay(replay_id: string): Promise<eventWithTime[]> {
	const storage = getStorage();
	const storageRef = storage_ref(storage, 'replays/'+replay_id);
	try {
		const compressed_replay = await getBytes(storageRef);
		const serialized_replay = pako.inflate(compressed_replay, {to: "string"});
		const replay = JSON.parse(serialized_replay);
		return replay;
	} catch {
		throw "Default snapshot '" + replay_id + "' does not exist in database!";
	}
}

async function get_session_events(session_id: string): Promise<Event[]> {
	const db = ref(getDatabase());
	const database_nodes = (await get(query(
		child(db, "events"),
		orderByChild("session_id"),
		equalTo(session_id),
	))).val();
	return Object.values(database_nodes);
}

async function get_openai_api_key(): Promise<string> {
	return get_fallback("openai_api_key") as any;
}

export {
	write_snapshot,
	write_snapshot_json,
	write_chain_node,
	write_chain,
	get_new_snapshot_id,
	get_new_chain_node_id,
	get_new_session_id,
	get_new_user_id,
	get_snapshot,
	get_default_snapshots,
	get_default_snapshot,
	get_chain,
	get_chain_ids,
	get_session,
	get_active_chain_node,
	write_session,
	write_event,
	write_default_snapshot,
	get_successful_executions,
	get_failed_executions,
	get_chain_nodes,
	write_chain_activity_ping,
	set_available_chains,
	assign_available_chain_id,
	write_survey_response,
	get_survey_responses,
	write_tutorial_completion,
	get_fallback,
	get_chain_executions,
	get_available_chain_ids,
	sign_in_anonymously,
	write_automatic_post_condition_answer,
	write_execution_cancellation,
	get_snapshot_total_ms,
	get_unix_time,
	get_snapshot_solver_ms,
	get_snapshot_show_tutorial_ms,
	get_snapshot_show_recipes_ms,
	get_snapshot_show_library_ms,
	get_snapshot_sat_items,
	write_rejected_chain_node,
	write_replay,
	get_new_replay_id,
	get_replay,
	get_session_events,
	get_openai_api_key,
	InventorySource,
	get_chain_node,
	get_chain_successful_executions,
	get_chain_failed_executions,
};

export type {
	ChainNode,
	SuccessfulExecution,
	FailedExecution,
	Session,
	Chain,
};
