/**
 * Implements a user simulation.
 */
import { get_new_chain_snapshot_data } from "./chain_loader";
import { get_new_session_id } from "$lib/firebase";
import { get_new_user_id } from "$lib/firebase";
import { get_fringe_seed } from "./minecraft_app";
import type { MinecraftAPI } from "./minecraft_app";
import { get_concrete_beam } from "./minecraft_app";
import { get_np_beam } from "./minecraft_app";
import { preprocess_np_for_search } from "./minecraft_app";
import { ui_solver_timeout_ms } from "./minecraft_app";
import type { CraftingWorld } from "./minecraft_app";
import type { ProgramState } from "./minecraft_app";
import type { Item } from "minecraft/items";
import type { NaturalProgramSketch } from "natural-programs/types";
import type { NaturalSignature } from "natural-programs/types";
import type { Library } from "natural-programs/library/library";
import { post_condition_string_interpreter } from "minecraft/constraints";
import { concrete_interpreter } from "./minecraft_app";
import type { BeamSearchParameters } from "./session_snapshot";
import { SessionType } from "./session_snapshot";
import { solve_with_multisolver } from "./solver";
import { exact_solver } from "natural-programs/library/exact_solver";
import type { ProposeParameters } from "natural-programs/library/proposers";
import type { BeamParameters } from "./minecraft_app";
import type { SourcedSolverResult } from "./solver";
import type { SynthesisProblem } from "./minecraft_app";
import { SourceSolver } from "./solver";
import { get_api } from "./minecraft_app";
import { parse_csvs } from "./world_csv";
import type { SourcedResult } from "./solver";
import { run_automatic_session } from "./automatic_session";
import { write_chain_node } from "./firebase";
import { write_event } from "./firebase";
import type { SuccessfulExecution } from "./events";
import type { FailedExecution } from "./events";
import { write_snapshot } from "./firebase";
import { add_to_library } from "./minecraft_app";
import { string_sim_timeout } from "./minecraft_app";
import type { SessionSnapshot } from "./session_snapshot";


// Solvers
// Enumerative solver facade
async function enumerative_solver_facade(
		np: NaturalProgramSketch,
		state: ProgramState,
		library: Library,
		mode: SessionType,
		force_cancel_ms: number,
		fringe_seed: NaturalProgramSketch[],
		beam_search_parameters: BeamSearchParameters,
		propose_parameters: ProposeParameters,
		beam_parameters: BeamParameters,
		world: CraftingWorld<Item>,
		api: MinecraftAPI,
		max_queue_size: number,
		): Promise<SourcedSolverResult<ProgramState>> {

	const cancel_promise = new Promise<void>((resolve, _) => {
		// Set automatic cancel if given
		new Promise(r=>setTimeout(r, force_cancel_ms)).then(()=>resolve());
	});

	// Pre-process the program sketch
	const np_for_search = await preprocess_np_for_search(np, library);

	// Construct the beam according to the session type
	const np_beam = await get_np_beam(
			np_for_search,
			library,
			beam_search_parameters.library_beam_width,
			beam_parameters,
		);
	const concrete_beam = await get_concrete_beam(
			np_for_search,
			library,
			beam_search_parameters.library_beam_width,
			beam_parameters,
		);

	// Assemble synthesis problems
	const np_synthesis_problem = {
		np: np,
		state: state,
		beam: np_beam,
		parameters: beam_search_parameters,
		world: world,
		api: api,
		library: library,
		timeout_ms: ui_solver_timeout_ms,
		propose_parameters: propose_parameters,
		max_queue_size: max_queue_size,
		fringe_seed: fringe_seed,
	};
	const ds_synthesis_problem = {
		np: np,
		state: state,
		beam: concrete_beam,
		parameters: beam_search_parameters,
		world: world,
		api: api,
		library: library,
		timeout_ms: ui_solver_timeout_ms,
		propose_parameters: propose_parameters,
		max_queue_size: max_queue_size,
		fringe_seed: fringe_seed,
	};
	const llmnp_synthesis_problem = {
		np: np,
		state: state,
		beam: np_beam,  // it's ignored
		parameters: beam_search_parameters,
		world: world,
		api: api,
		library: library,
		timeout_ms: ui_solver_timeout_ms,
		propose_parameters: propose_parameters,
		max_queue_size: max_queue_size,
		fringe_seed: fringe_seed,
	};
	const llmds_synthesis_problem: SynthesisProblem = {
		np: np,
		state: state,
		beam: concrete_beam,  // it's ignored
		parameters: beam_search_parameters,
		world: world,
		api: api,
		library: library,
		timeout_ms: ui_solver_timeout_ms,
		propose_parameters: propose_parameters,
		max_queue_size: max_queue_size,
		fringe_seed: fringe_seed,
	};

	try {
		// Execute enumerative solver
		const result = (mode === SessionType.NaturalProgramming) ?
			await solve_with_multisolver({
				llmnp_synthesis_problem: undefined,
				llmds_synthesis_problem: undefined,
				np_synthesis_problem: np_synthesis_problem,
				ds_synthesis_problem: undefined,
			}, cancel_promise)
			: (mode === SessionType.DirectSynthesis) ?
			await solve_with_multisolver({
				llmnp_synthesis_problem: undefined,
				llmds_synthesis_problem: undefined,
				np_synthesis_problem: undefined,
				ds_synthesis_problem: ds_synthesis_problem,
			}, cancel_promise)
			: // mode = SessionType.LLMNaturalProgramming
			await solve_with_multisolver({
				llmnp_synthesis_problem: llmnp_synthesis_problem,
				llmds_synthesis_problem: llmds_synthesis_problem,
				np_synthesis_problem: np_synthesis_problem,
				ds_synthesis_problem: ds_synthesis_problem,
			}, cancel_promise);

		if (!("error" in result)) {
			return result;
		}
		throw result;
	} catch (err) {
		// Display an error message. Some forms of error are pretty-printed
		// to ease debugging by the user
	}
	throw "Neither base solver nor beam search worked!";
}

// Exact solver: expand the tree only with exact matches from library
async function exact_solver_facade(
		np: NaturalProgramSketch,
		state: ProgramState,
		library: Library,
		world: CraftingWorld<Item>,
		api: MinecraftAPI,
		): Promise<SourcedSolverResult<ProgramState>> {
	const result = exact_solver(
		np,
		state,
		library,
		(p, s)=>concrete_interpreter(p, s, api, world),
		(p, o, n)=>post_condition_string_interpreter(p, o, n, world),
	);

	if (!("error" in result))
		return {...result, source_solver: SourceSolver.DP};
	if (result.first_execution_error !== null)
		throw result.first_execution_error.error;
	throw "Seems like an unknown function was called!";
}

/**
 * Solver function.
 */
async function solver(
		np: NaturalProgramSketch,
		state: ProgramState,
		library: Library,
		force_cancel_ms: number,
		fringe_seed: NaturalProgramSketch[],
		session_type: SessionType,
		beam_search_parameters: BeamSearchParameters,
		propose_parameters: ProposeParameters,
		beam_parameters: BeamParameters,
		world: CraftingWorld<Item>,
		api: MinecraftAPI,
		max_queue_size: number,
		): Promise<SourcedSolverResult<ProgramState>> {
	// In Direct Programming, interpret the program as-is with
	// the "exact solver"
	if (session_type === SessionType.DirectProgramming) {
		return exact_solver_facade(
			np,
			state,
			library,
			world,
			api,
		);
	}

	// Call enumerative solver with the beam
	return enumerative_solver_facade(
		np,
		state,
		library,
		session_type,
		force_cancel_ms,
		fringe_seed,
		beam_search_parameters,
		propose_parameters,
		beam_parameters,
		world,
		api,
		max_queue_size,
	);
}


/**
 * Simulate a user and a node to the given chain.
 **/
async function* simulate_node(
		chain_id: string,
		): AsyncGenerator<string, void, void> {
	// Get snapshot data
	const icon_directory = ""; // We do not need icons
	const session_id = await get_new_session_id();
	const user_id = await get_new_user_id();
	const snapshot = await get_new_chain_snapshot_data(
		chain_id,
		icon_directory,
		session_id,
		user_id,
	);
	const world = parse_csvs(
		snapshot.item_csv, snapshot.mine_csv, snapshot.seed, icon_directory
	);
	const api = get_api(world);
	const library = snapshot.library;
	const start_date = Date.now();

	// Initialize state
	let state: ProgramState = {
		inventory: [...snapshot.inventory],
		crafting_table: [],
		held_item: null,
	};

	// Keep track of SAT items
	// Same logic as in MinecraftApp.svelte
	const sat_items_names = new Set<string>();
	const goal = snapshot.goal;
	let remaining_goal: (null|Item[]) = (goal === null) ? null : [];
	if (remaining_goal !== null && goal !== null)
		for (const item of goal)
			if (!(sat_items_names.has(item.name)))
				remaining_goal.push(item);
	function update_remaining_goal(state: ProgramState) {
		if (remaining_goal === null)
			return;
		const new_remaining_goal = [];
		for (const item of remaining_goal) {
			const owned_n = state.inventory.filter(i=>i.name===item.name).length;
			const goal_n = remaining_goal.filter(i=>i.name===item.name).length;
			const is_sat = owned_n >= goal_n;
			if (is_sat)
				sat_items_names.add(item.name);
			else
				new_remaining_goal.push(item);
		}
		if (JSON.stringify(new_remaining_goal) !== JSON.stringify(remaining_goal))
			remaining_goal = new_remaining_goal;
	}

	// Construct solver
	const force_cancel_ms = 40000;
	async function local_solver(np: NaturalSignature, state_: ProgramState): Promise<SourcedResult> {
		const solver_start_date = Date.now();
		try {
			const result = await solver(
				np,
				state_,
				library,
				force_cancel_ms,
				get_fringe_seed(np, library.added_sketches),
				snapshot.session_type,
				snapshot.beam_search_parameters,
				snapshot.propose_parameters,
				snapshot.beam_parameters,
				world,
				api,
				snapshot.max_queue_size,
			);
			if ("error" in result)
				throw result;
			const event: SuccessfulExecution = {
				kind: "successful_execution",
				session_milliseconds: Date.now()-start_date,
				session_id: session_id,
				np: JSON.stringify(np),
				concrete_np: JSON.stringify(result.program),
				dequeue_n: result.dequeue_n,
				milliseconds: result.milliseconds,
				execution_snapshot_id: "",
				source_solver: result.source_solver,
			};
			console.log(result);
			state = result.final_state;
			update_remaining_goal(state);
			await write_event(event);
			await add_to_library(result.program, library, string_sim_timeout);
			return result;
		} catch (e) {
			const event: FailedExecution = {
				kind: "failed_execution",
				np: JSON.stringify(np),
				error: JSON.stringify(e),
				milliseconds: Date.now()-solver_start_date,
				execution_snapshot_id: "",
				session_id: session_id,
				session_milliseconds: Date.now()-start_date,
			};
			write_event(event);
			throw e;
		}
	}

	// Simulate user
	const generator = run_automatic_session(
		local_solver,
		state,
		world,
		snapshot.session_timeout_ms,
	);
	for await (const message of generator)
		yield message;

	// Add node to the chain
	const final_snapshot: SessionSnapshot = {
		item_csv: snapshot.item_csv,
		mine_csv: snapshot.mine_csv,
		session_type: snapshot.session_type,
		inventory: state.inventory,
		beam_search_parameters: snapshot.beam_search_parameters,
		propose_parameters: snapshot.propose_parameters,
		beam_parameters: snapshot.beam_parameters,
		library: library,
		seed: snapshot.seed,
		goal: snapshot.goal,
		sat_items_names: [...sat_items_names],
		total_ms: Date.now()-start_date,
		solver_ms: Date.now()-start_date,
		show_recipes_ms: 0,
		show_tutorial_ms: 0,
		show_library_ms: 0,
		show_signature_execution_error_ms: 0,
		max_queue_size: snapshot.max_queue_size,
	};
	await write_snapshot(snapshot.snapshot_id, final_snapshot);
	await write_chain_node(snapshot.chain_node);
	yield "Saved node!";
}

export {
	simulate_node,
};
