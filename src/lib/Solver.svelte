<script lang="ts">
	/**
	 * Component to load sessions with a solver function according to the given
	 * parameters.
	 */
	import LibrarySave from "./LibrarySave.svelte";
	import Modal from "./Modal.svelte";
	import ItemCard from "./ItemCard.svelte";
	import ItemList from "./ItemList.svelte";
	import type { MinecraftAPI } from "./minecraft_app";
	import { get_concrete_beam } from "./minecraft_app";
	import { get_np_beam } from "./minecraft_app";
	import { preprocess_np_for_search } from "./minecraft_app";
	import { ui_solver_timeout_ms } from "./minecraft_app";
	import type { Goal } from "./minecraft_app";
	import type { CraftingWorld } from "./minecraft_app";
	import type { ProgramState } from "./minecraft_app";
	import type { Item } from "minecraft/items";
	import type { NaturalProgramSketch } from "natural-programs/types";
	import type { NaturalSignature } from "natural-programs/types";
	import type { Library } from "natural-programs/library/library";
	import { get_constraint_as_string } from "minecraft/constraints";
	import { post_condition_string_interpreter } from "minecraft/constraints";
	import { concrete_interpreter } from "./minecraft_app";
	import type { BeamSearchParameters } from "./session_snapshot";
	import { SessionType } from "./session_snapshot";
	import { MinecraftSimplePrimitive } from "minecraft/grammar";
	import type { MinecraftPrimitivePick } from "minecraft/grammar";
	import { solve_with_multisolver } from "./solver";
	import { exact_solver } from "natural-programs/library/exact_solver";
	import { write_execution_cancellation } from "./firebase";
	import type { ChainNode } from "./firebase";
	import type { ProposeParameters } from "natural-programs/library/proposers";
	import type { BeamParameters } from "./minecraft_app";
	import type { SourcedSolverResult } from "./solver";
	import type { SynthesisProblem } from "./minecraft_app";
	import { SourceSolver } from "./solver";
	import Timer from "easytimer.js";

	const solver_stopwatch = new Timer();

	export let state: ProgramState;
	export let library: Library;
	export let world: CraftingWorld<Item>;
	export let show_debug_menu: boolean;
	export let item_csv: string;
	export let mine_csv: string;
	export let session_type: SessionType;
	export let beam_search_parameters: BeamSearchParameters;
	export let propose_parameters: ProposeParameters;
	export let snapshot_id: string;
	export let snapshot_url: string;
	export let session_id: string;
	export let user_id: string;
	export let seed: string;
	export let goal: Goal|null;
	export let chain_node: ChainNode|null;
	export let tutorial_video_urls: {title: string, path: string}[];
	export let api: MinecraftAPI;
	export let beam_parameters: BeamParameters;
	export let session_timeout_ms: number|null;
	export let sat_item_bonus_usd: number|null;
	export let starting_sat_items_names: string[];
	export let max_queue_size: number;
	export let replay_id: string|null;
	export let show_library_stopwatch: Timer;
	
	const show_signature_execution_error_stopwatch = new Timer();

	// Execution modal configuration
	const execution_modal_timeout_ms = ui_solver_timeout_ms;
	const execution_modal_delay_ms = 500;
	const execution_modal_min_ms = 1000;
	let show_execution_modal = false;
	let execution_modal_message = "";
	let execution_modal_time_ms = 0;
	let execution_modal_interval: any = null;
	let cancel_execution: (null|(()=>Promise<void>)) = null;
	function show_execution_modal_(message: string) {
		const start = Date.now();
		execution_modal_time_ms = 0;
		show_execution_modal = true;
		execution_modal_message = message;
		if (execution_modal_interval !== null)
			clearInterval(execution_modal_interval);
		execution_modal_interval = setInterval(() => {
			execution_modal_time_ms = Date.now()-start;
		}, 100);
	}
	function close_execution_modal() {
		if (execution_modal_interval !== null)
			clearInterval(execution_modal_interval);
		show_execution_modal = false;
	}

	const items_in_scope = [...world.name_to_item.values()];
	const post_conditions: string[] = [];
	for(const item of items_in_scope) {
		const constraint = get_constraint_as_string({
			inventory: [item],
		});
		post_conditions.push(constraint);
	}
	post_conditions.sort();

	// Execution error modals
	let execution_error: (null|string) = null;
	let pick_execution_error: (null|MinecraftPrimitivePick) = null;
	let craft_execution_error: (null|Item[]) = null;
	let signature_execution_error: (null|NaturalSignature) = null;
	let sketch_execution_error: (null|NaturalSignature) = null;

	// Helper function to show signature error.
	function show_signature_execution_error(np: NaturalSignature) {
		signature_execution_error = np;
		show_signature_execution_error_stopwatch.start();
	}
	function close_signature_execution_error() {
		signature_execution_error = null;
		show_signature_execution_error_stopwatch.pause();
	}

	// Solvers
	// Enumerative solver facade
	async function enumerative_solver_facade(
			np: NaturalProgramSketch,
			state: ProgramState,
			library: Library,
			mode: SessionType,
			message: string,
			notify_if_error: boolean,
			force_cancel_ms: null|number,
			fringe_seed: NaturalProgramSketch[],
			): Promise<SourcedSolverResult<ProgramState>> {
		solver_stopwatch.start();

		// Only show modal after a few milliseconds, and if it gets shown, show for
		// at least a little while
		let should_show_modal = true;
		let showed_modal = false;
		const min_modal_timeout = new Promise(r => setTimeout(r, execution_modal_min_ms));
		const show_modal_timeout = setTimeout(() => {
			if (should_show_modal) {
				should_show_modal = false;
				showed_modal = true;
				show_execution_modal_(message);
			}
		}, execution_modal_delay_ms);
		const solver_start = Date.now();
		const cancel_promise = new Promise<void>((resolve, _) => {
			// Set automatic cancel if given
			if (force_cancel_ms !== null)
				new Promise(r=>setTimeout(r, force_cancel_ms)).then(()=>resolve());

			// Listen to user-initiated cancel event
			cancel_execution = async () => {
				clearTimeout(show_modal_timeout);
				close_execution_modal();
				await write_execution_cancellation(
					session_id,
					np,
					snapshot_id,
					"User cancelled",
					Date.now()-solver_start
				);
				resolve();
			};
		});

		// Pre-process the program sketch
		const np_for_search = await preprocess_np_for_search(np, library);

		// Construct the beam according to the session type
		const np_beam = await get_np_beam(
				np_for_search,
				library,
				beam_search_parameters.library_beam_width,
				beam_parameters,
			);
		const concrete_beam = await get_concrete_beam(
				np_for_search,
				library,
				beam_search_parameters.library_beam_width,
				beam_parameters,
			);

		// Assemble synthesis problems
		const np_synthesis_problem = {
			np: np,
			state: state,
			beam: np_beam,
			parameters: beam_search_parameters,
			world: world,
			api: api,
			library: library,
			timeout_ms: ui_solver_timeout_ms,
			propose_parameters: propose_parameters,
			max_queue_size: max_queue_size,
			fringe_seed: fringe_seed,
		};
		const ds_synthesis_problem = {
			np: np,
			state: state,
			beam: concrete_beam,
			parameters: beam_search_parameters,
			world: world,
			api: api,
			library: library,
			timeout_ms: ui_solver_timeout_ms,
			propose_parameters: propose_parameters,
			max_queue_size: max_queue_size,
			fringe_seed: fringe_seed,
		};
		const llmnp_synthesis_problem = {
			np: np,
			state: state,
			beam: np_beam,  // it's ignored
			parameters: beam_search_parameters,
			world: world,
			api: api,
			library: library,
			timeout_ms: ui_solver_timeout_ms,
			propose_parameters: propose_parameters,
			max_queue_size: max_queue_size,
			fringe_seed: fringe_seed,
		};
		const llmds_synthesis_problem: SynthesisProblem = {
			np: np,
			state: state,
			beam: concrete_beam,  // it's ignored
			parameters: beam_search_parameters,
			world: world,
			api: api,
			library: library,
			timeout_ms: ui_solver_timeout_ms,
			propose_parameters: propose_parameters,
			max_queue_size: max_queue_size,
			fringe_seed: fringe_seed,
		};

		try {
			// Execute enumerative solver
			const result = (mode === SessionType.NaturalProgramming) ?
				await solve_with_multisolver({
					llmnp_synthesis_problem: undefined,
					llmds_synthesis_problem: undefined,
					np_synthesis_problem: np_synthesis_problem,
					ds_synthesis_problem: undefined,
				}, cancel_promise)
				: (mode === SessionType.DirectSynthesis) ?
				await solve_with_multisolver({
					llmnp_synthesis_problem: undefined,
					llmds_synthesis_problem: undefined,
					np_synthesis_problem: undefined,
					ds_synthesis_problem: ds_synthesis_problem,
				}, cancel_promise)
				: // mode = SessionType.Multisolver
				await solve_with_multisolver({
					llmnp_synthesis_problem: llmnp_synthesis_problem,
					llmds_synthesis_problem: llmds_synthesis_problem,
					np_synthesis_problem: np_synthesis_problem,
					ds_synthesis_problem: ds_synthesis_problem,
				}, cancel_promise);

			if (!("error" in result)) {
				if (showed_modal) {
					await min_modal_timeout;
					close_execution_modal();
				}
				clearTimeout(show_modal_timeout);
				should_show_modal = false;
				solver_stopwatch.pause();
				console.log({
					event: "SUCCESS EXECUTION",
					result: result,
				});
				return result;
			}
			throw result;
		} catch (err) {
			console.log({
				event: "FAILED EXECUTION",
				err: err,
				np: np,
			});
			// Display an error message. Some forms of error are pretty-printed
			// to ease debugging by the user
			if (notify_if_error) {
				if (typeof np === "string") {
					const parsed_np = api.primitives_map.get(np);
					if (parsed_np === undefined) {
						execution_error = "An unknown primitive was executed!";
					} else if (typeof parsed_np !== "string" && "item" in parsed_np) {
							pick_execution_error = parsed_np;
					} else if (parsed_np === MinecraftSimplePrimitive.Craft) {
						craft_execution_error = state.crafting_table;
					} else {
						execution_error = "Something wrong happened!";
					}
				} else if (!("signature" in np)) {
					show_signature_execution_error(np);
				} else {
					sketch_execution_error = np.signature;
				}
			}
		}

		clearTimeout(show_modal_timeout);
		should_show_modal = false;
		close_execution_modal();
		solver_stopwatch.pause();
		throw "Neither base solver nor beam search worked!";
	}

	// Exact solver: expand the tree only with exact matches from library
	async function exact_solver_facade(
			np: NaturalProgramSketch,
			state: ProgramState,
			library: Library,
			notify_if_error: boolean,
			): Promise<SourcedSolverResult<ProgramState>> {
		const result = exact_solver(
			np,
			state,
			library,
			(p, s)=>concrete_interpreter(p, s, api, world),
			(p, o, n)=>post_condition_string_interpreter(p, o, n, world),
		);

		if (!("error" in result))
			return {...result, source_solver: SourceSolver.DP};

		// Display an error message. Some forms of error are pretty-printed
		// to ease debugging by the user
		if (notify_if_error) {
			if (result.first_execution_error === null) {
				execution_error = "Seems like an unknown function was called!";
			} else {
				if (typeof np === "string") {
					const parsed_np = api.primitives_map.get(np);
					if (parsed_np === undefined) {
						execution_error = "An unknown primitive was executed!";
					} else if (typeof parsed_np !== "string" && "item" in parsed_np) {
							pick_execution_error = parsed_np;
					} else if (parsed_np === MinecraftSimplePrimitive.Craft) {
						craft_execution_error = state.crafting_table;
					} else {
						execution_error = JSON.stringify(result.first_execution_error.error);
					}
				} else if (!("signature" in np)) {
					show_signature_execution_error(np);
				} else {
					sketch_execution_error = np.signature;
				}
			}
		}

		if (result.first_execution_error !== null)
			throw result.first_execution_error.error;
		throw "Seems like an unknown function was called!";
	}

	/**
	 * Solver function.
	 */
	async function solver(
			np: NaturalProgramSketch,
			state: ProgramState,
			library: Library,
			message: string,
			notify_if_error: boolean,
			force_cancel_ms: null|number,
			fringe_seed: NaturalProgramSketch[],
			): Promise<SourcedSolverResult<ProgramState>> {
		// In Direct Programming, interpret the program as-is with
		// the "exact solver"
		if (session_type === SessionType.DirectProgramming) {
			return exact_solver_facade(
				np,
				state,
				library,
				notify_if_error,
			);
		}

		// Call enumerative solver with the beam
		return enumerative_solver_facade(
			np,
			state,
			library,
			session_type,
			message,
			notify_if_error,
			force_cancel_ms,
			fringe_seed,
		);
	}
</script>

<LibrarySave
	user_id={user_id}
	snapshot_id={snapshot_id}
	state={state}
	library={library}
	solver={solver}
	world={world}
	show_debug_menu={show_debug_menu}
	mine_csv={mine_csv}
	item_csv={item_csv}
	session_type={session_type}
	beam_search_parameters={beam_search_parameters}
	propose_parameters={propose_parameters}
	snapshot_url={snapshot_url}
	session_id={session_id}
	seed={seed}
	goal={goal}
	chain_node={chain_node}
	tutorial_video_urls={tutorial_video_urls}
	api={api}
	beam_parameters={beam_parameters}
	sat_item_bonus_usd={sat_item_bonus_usd}
	session_timeout_ms={session_timeout_ms}
	starting_sat_items_names={starting_sat_items_names}
	solver_stopwatch={solver_stopwatch}
	max_queue_size={max_queue_size}
	replay_id={replay_id}
	show_library_stopwatch={show_library_stopwatch}
	show_signature_execution_error_stopwatch={show_signature_execution_error_stopwatch}
	on:task_completed
/>

{#if show_execution_modal}
	<Modal
			show_close_button={false}
			>
		<h1 class="title">Robot is "thinking"</h1>
		<h2 class="subtitle">Please standby</h2>
		<p><b>{execution_modal_message}</b></p>
		<p>You can cancel anytime by clicking "Stop" :)</p>
		<p>Running time: {Math.round(execution_modal_time_ms/1000)}s</p>
		<p>Timeout: {Math.round(execution_modal_timeout_ms)/1000}s</p>
		<progress class="progress is-medium is-primary" max="100">1%</progress>
		<button class="button" on:click={()=>{if (cancel_execution !== null) cancel_execution()}}>Stop</button>
	</Modal>
{/if}

{#if execution_error !== null}
	<Modal on:close="{() => {execution_error = null;}}">
		<h1 class="title">Whoops!</h1>
		<p>{execution_error}</p>
	</Modal>
{/if}

{#if pick_execution_error !== null}
	<Modal on:close="{() => {pick_execution_error = null;}}">
		<h1 class="title">Whoops!</h1>
		<p>Cannot execute input because the following item is not in the inventory!</p>
		<ItemCard item={pick_execution_error.item} clickable={false} />
	</Modal>
{/if}

{#if craft_execution_error !== null}
	<Modal on:close="{() => {craft_execution_error = null;}}">
		<h1 class="title">Whoops!</h1>
		<p>Cannot execute craft because the inputs do not match any recipe!</p>
		<ItemList
			items={craft_execution_error}
			clickable={false}
		/>
	</Modal>
{/if}

{#if signature_execution_error !== null}
	<Modal on:close="{() => {close_signature_execution_error()}}">
		<h1 class="title">The robot needs some help!</h1>
		{#if signature_execution_error.post_condition !== undefined}
			<!-- Signature had a post condition -->
			<div class="block">
				<p>Please take your time to read carefully. Your timer is paused.</p>
				<p>The robot cannot figure out how to '{signature_execution_error.name}'. This is perfectly normal, but you need to teach the robot. Here is how to teach the robot to '{signature_execution_error.name}':</p>
			</div>
			<div class="content">
				<ol>
					<li>Open the recipe book (button on top left).</li>
					<li>Find the item that corresponds to '{signature_execution_error.name}'.</li>
					<li>Notice how this item depends on some other items according to the recipe.</li>
					<li>Ask the robot to build the required items first.</li>
					<li>When all required items are in the robot's inventory, ask the robot to '{signature_execution_error.name}' again.</li>
				</ol>
			</div>
			<div class="block">
			<p>
				If you are finding it difficult to follow these instructions, please watch tutorial 3 (<i>Teach the robot how to make a new item using the recipe book</i>).
				Your timer will be paused while you're watching the tutorial.
			</p>
			</div>
		{:else}
			<!-- Signature did not have a post condition -->
			<div class="block">
				<p>Please take your time to read carefully. Your timer is paused.</p>
				<p>An error was encountered while executing {(signature_execution_error.name !== undefined) ? "'" + signature_execution_error.name +"'" : "your command"}.</p>
			</div>
			<div class="content">
				<p>You can either:</p>
				<ul>
					<li>Look for a similar skill in the library, or</li>
					<li>Teach a different way to {(signature_execution_error.name !== undefined) ? "'" + signature_execution_error.name + "'" : "do accomplish your intent"}.
				</ul>
			</div>
			<div class="block">
			<p>
				If you are finding it difficult to follow these instructions, please watch the tutorial.
				Your timer will be paused while you're watching the tutorial.
			</p>
			</div>
		{/if}
	</Modal>
{/if}

{#if sketch_execution_error !== null}
	<Modal on:close="{() => {sketch_execution_error = null;}}">
		<h1 class="title">Whoops!</h1>
		<div class="block">
			<p>Robot failed to learn {(sketch_execution_error.name !== undefined) ? "'" + sketch_execution_error.name +"'" : "your command"}.</p>
		</div>
		<div class="content">
			{#if sketch_execution_error.post_condition !== undefined}
				Please make sure the steps can be run in sequence to produce the goal.
			{:else}
				Please make sure the steps can be run in sequence.
			{/if}
		</div>
	</Modal>
{/if}
