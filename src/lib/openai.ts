import { Configuration, OpenAIApi } from "openai";
import { ChatCompletionResponseMessageRoleEnum } from "openai";
import { get_openai_api_key } from "./firebase";


const completion_cache = new Map<string, string[]>();


///**
// * Return the given number of prompt completions from a LLM.
// **/
//async function get_llm_completions(prompt: string, n: number): Promise<string[]> {
//	const key = JSON.stringify([prompt, n]);
//	const cached_result = completion_cache.get(key);
//	if (cached_result !== undefined && Math.random() < 0.2)
//		return cached_result;
//
//	const api_key = await get_openai_api_key();
//	const configuration = new Configuration({
//			apiKey: api_key,
//	});
//	const start_time = Date.now();
//	const model = "text-davinci-003";
//
//	const openai = new OpenAIApi(configuration);
//
//	try {
//		console.log({event: "Asking LLM completion", prompt: prompt});
//		const response = await openai.createCompletion({
//			model: model,
//			prompt: prompte
//			max_tokens: 512, // TODO: parametrize
//			n: n,
//			best_of: n+1,
//			temperature: 1.4,
//		});
//		const completions = [];
//		for (const choice of response.data.choices) {
//			const text = choice.text;
//			if (text !== undefined)
//				completions.push(text);
//		}
//		console.log({
//			event: "LLM query",
//			prompt: prompt,
//			completions: completions,
//			time_s: (Date.now()-start_time)/1000,
//		});
//
//		completion_cache.set(key, completions);
//		return completions;
//	} catch (err) {
//		console.error(err);
//		return [];
//	}
//}

/**
 * Return the given number of prompt completions from a LLM.
 **/
async function get_llm_completions(prompt: string, n: number): Promise<string[]> {
	const key = JSON.stringify([prompt, n]);
	const cached_result = completion_cache.get(key);
	//if (cached_result !== undefined)
	//	return cached_result;

	const api_key = await get_openai_api_key();
	const configuration = new Configuration({
			apiKey: api_key,
	});
	delete configuration.baseOptions.headers['User-Agent'];
	const start_time = Date.now();
	const model = "gpt-3.5-turbo";
	//const model = "text-davinci-003";

	const chat_prompt = [{
		role: ChatCompletionResponseMessageRoleEnum.User,
		content: prompt,
	}];

	try {
		const openai = new OpenAIApi(configuration);
		console.log("ASD");
		const response = await openai.createChatCompletion({
			model: model,
			messages: chat_prompt,
			//max_tokens: 120, // TODO: parametrize
			n: n,
			//best_of: n+1,
			//temperature: 1.4,
		});
		const completions = [];
		for (const choice of response.data.choices) {
			const message = choice.message;
			if (message === undefined)
				continue;
			const text = message.content;
			if (text !== undefined)
				completions.push(text);
		}
		console.log({
			event: "LLM query",
			prompt: prompt,
			completions: completions,
			time_s: (Date.now()-start_time)/1000,
		});

		//completion_cache.set(key, completions);
		return completions;
	} catch(e) {
		console.error(e);
		return [];
	}
}

export {
	get_llm_completions,
};
