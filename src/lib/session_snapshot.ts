/**
 * Defines session snapshots as a data type.
 */
import { as_json, from_json, values, type Library } from "natural-programs/library/library";
import type { BeamSearchParameters } from "natural-programs/library/fringe_solver";
import type { Item } from "minecraft/items";
import type { BeamParameters, Goal } from "./minecraft_app";
import { get_api, primitive_parser, string_sim_timeout } from "./minecraft_app";
import { parse_csvs } from "./world_csv";
import type { ProposeParameters } from "natural-programs/library/proposers";
import { get_fallback } from "./firebase";

enum SessionType {
	NaturalProgramming = "Natural Programming",
	DirectSynthesis = "Direct Synthesis",
	DirectProgramming = "Direct Programming",
	Multisolver = "Multisolver",
};

type SessionSnapshot = {
	item_csv: string,
	mine_csv: string,
	session_type: SessionType,
	inventory: Item[],
	beam_search_parameters: BeamSearchParameters,
	propose_parameters: ProposeParameters,
	beam_parameters: BeamParameters,
	library: Library,
	seed: string,
	goal: Goal|null,
	sat_items_names: string[]|null,
	total_ms: number,
	solver_ms: number,
	show_recipes_ms: number,
	show_tutorial_ms: number,
	show_library_ms: number,
	show_signature_execution_error_ms: number,
	max_queue_size: number,
};

/**
 * Return a JSON string that can be used to recreate the snapshot.
 */
function snapshot_as_json(snapshot: SessionSnapshot): string {
	return JSON.stringify({
		...snapshot,
		library: JSON.parse(as_json(snapshot.library)),
		inventory: snapshot.inventory.map(i=>i.name),
		goal: (snapshot.goal===null)?snapshot.goal:snapshot.goal.map(i=>i.name),
	});
}

/**
 * The snapshot data model has changed through development. This function
 * attempts to sanitize old data.
 */
async function maybe_fix_old_snapshot(snapshot: any): Promise<any> {
	if (!("propose_parameters" in snapshot))
		snapshot = {
			...snapshot,
			propose_parameters: await get_fallback("propose_parameters"),
		};
	if (!("beam_parameters" in snapshot)) {
		const beam_parameters = (snapshot.session_type === SessionType.NaturalProgramming) ?
			"np_beam_parameters" : "synthesis_beam_parameters";
		snapshot = {
			...snapshot,
			beam_parameters: await get_fallback(beam_parameters)
		};
	}
	if (!("sat_items_names" in snapshot)) {
		snapshot = {
			...snapshot,
			sat_items_names: null,
		};
	}
	if (!("total_ms" in snapshot)) {
		snapshot = {
			...snapshot,
			total_ms: 0,
		};
	}
	if (!("solver_ms" in snapshot)) {
		snapshot = {
			...snapshot,
			solver_ms: 0,
		};
	}
	if (!("show_recipes_ms" in snapshot)) {
		snapshot = {
			...snapshot,
			show_recipes_ms: 0,
		};
	}
	if (!("show_tutorial_ms" in snapshot)) {
		snapshot = {
			...snapshot,
			show_tutorial_ms: 0,
		};
	}
	if (!("show_library_ms" in snapshot)) {
		snapshot = {
			...snapshot,
			show_tutorial_ms: 0,
		};
	}
	if (!("show_signature_execution_error_ms" in snapshot)) {
		snapshot = {
			...snapshot,
			show_signature_execution_error_ms: 0,
		}
	}
	if (typeof snapshot.library === "string") {
		snapshot = {
			...snapshot,
			library: JSON.parse(snapshot.library),
		};
	}
	if (!("goal" in snapshot)) {
		snapshot = {
			...snapshot,
			goal: null,
		};
	}
	if (!("max_queue_size" in snapshot))
		snapshot = {
			...snapshot,
			max_queue_size: await get_fallback("max_queue_size"),
		};

	// Legacy support: Multisolver mode
	if (snapshot.session_type === "LLMNaturalProgramming + Natural Programming + Direct Synthesis")
		snapshot.session_type = SessionType.Multisolver;
	return snapshot;
}

/**
 * Returns the snapshot used to build the given JSON string.
 */
async function snapshot_from_json(json: string, icon_directory: string): Promise<SessionSnapshot> {
	const snapshot = await maybe_fix_old_snapshot(JSON.parse(json));
	const world = parse_csvs(snapshot.item_csv, snapshot.mine_csv, snapshot.seed, icon_directory);

	// Restore inventory
	const inventory = [];
	for (const item_name of snapshot.inventory) {
		const item = world.name_to_item.get(item_name);
		if (item === undefined)
			throw "Inventory in snapshot contains item " + item_name + " not in world!";
		inventory.push(item);
	}

	// Restore goal
	const maybe_goal = [];
	if (snapshot.goal !== null) {
		for (const item_name of snapshot.goal) {
			const item = world.name_to_item.get(item_name);
			if (item === undefined)
				throw "Goal in snapshot contains item " + item_name + " not in world!";
			maybe_goal.push(item);
		}
	}

	// Restore library
	const library = await from_json(JSON.stringify(snapshot.library), string_sim_timeout);
	const api = get_api(world);
	for (const np of values(library)) {
		if (typeof np === "string")
			try {
				primitive_parser(np, api);
			} catch (err) {
				throw "Seems like a primitive in the library is not compatible with the API of this world: "+err;
			}
	}

	return {
		...snapshot,
		library: library,
		inventory: inventory,
		goal: (snapshot.goal===null)?snapshot.goal:maybe_goal,
	};
}

export type {
	BeamSearchParameters,
	SessionSnapshot,
};

export {
	SessionType,
	snapshot_as_json,
	snapshot_from_json,
};
