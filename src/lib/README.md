# Application loading

Loading follows a sequence of components that define the logic of each
behavior (e.g. setting hyperparameters, solver modals, etc).

```mermaid
graph TD;
  Initial["Initial parameters (e.g. from a base snapshot)"] -- "Data required to fully load a session (e.g., CSVs, user ID, etc.)" --> CSVForm.svelte
  CSVForm.svelte -- "Parse CSVs (e.g. session recipes, optionally customize)" --> ParameterForm.svelte
  ParameterForm.svelte -- "Parse solver parameters (optionally customize)" --> Solver.svelte
  Solver.svelte  -- Solver modal logic --> LibrarySave.svelte
  LibrarySave.svelte -- Library saving logic --> MinecraftApp.svelte
```
